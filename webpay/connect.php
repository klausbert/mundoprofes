<?
	require_once('../environment.php');
	
	if (defined('ENVIRONMENT'))
	{
		if (! defined('APPPATH'))  define('APPPATH', 'application/');
		if (! defined('BASEPATH')) define('BASEPATH', '(flag for database.php)');
		
		require_once('../'. APPPATH .'config/'. ENVIRONMENT .'/database.php');
		
		$host = $db['default']['hostname'];
		$user = $db['default']['username'];
		$pass = $db['default']['password'];
		$base = $db['default']['database'];	// �ltimo
		$conn = mysqli_connect($host, $user, $pass, $base) or die(mysql_error());
	}
	else
	{	
		echo 'Connect error: ENVIRONMENT not defined ** ';
		echo ENVIRONMENT;
		die();
	}
