(function() { 
	angular.module('mp.news', ['ngSanitize'])
	.controller('NewsCtrl', NewsCtrl)
	.controller('PostCtrl', PostCtrl)
	.filter('toTrusted', ['$sce', function ($sce) {
		return function (value) {
			return $sce.trustAsHtml(value);
		};
	}])	// http://stackoverflow.com/questions/28051061/inserting-iframe-from-trusted-source-in-angularjs
	;
	function NewsCtrl($scope, $state, $http) {
		
		$http.get('wp', { params: { json: 'get_category_posts', slug: 'portada' }})
		.then( done => done.data ).then( function(response) {
			$scope.posts = response.posts || [];
		});
	};
	NewsCtrl.$inject = ['$scope','$state','$http'];
	
	function PostCtrl($scope, $stateParams, $http) {
		
		$http.get('wp', { params: { json: 'get_post', post_id: $stateParams.postId }})
		.then( done => done.data ).then( function(response) {
			$scope.post = response.post || [];
		});
	};
	PostCtrl.$inject = ['$scope','$stateParams','$http'];
})();
