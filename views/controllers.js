(function() { 
	angular.module('mp')
	.controller('ResetCtrl', ResetCtrl)
	.controller('ForgotCtrl', ForgotCtrl)
	;
	// included in both stated: confirm and request
	function ResetCtrl($scope, $state, $stateParams, $http) {
		
		$scope.data = { email: '', pass: '', dummy: '', reset: $stateParams.reset, status: 'pending' };
		$http.get('confirm', { params: $stateParams }).then( done => done.data ).then( function(response) {
			if (response) $scope.data.email = response.email;
		});
		$scope.put = function() {
			$http.put('reset', $scope.data).then( done => done.data ).then( function(response) {
				$scope.data.status = 'complete';
			});
		};
	};
	ResetCtrl.$inject = ['$scope', '$state', '$stateParams', '$http'];
	
	function ForgotCtrl($scope, $state, $http) {
		
		$scope.data = { email: '', status: 'pending' };
		$scope.put = function() {
			$http.put('forgot', $scope.data)
			.then( done => done.data ).then( function(response) {
				if (response.status) {
					$scope.data.status = response.status;
				}
			});
		}
	};
	ForgotCtrl.$inject = ['$scope', '$state', '$http'];
})();
