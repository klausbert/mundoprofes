(function() { 
	'use strict';
	angular.module('mp.signin', ['http-auth-interceptor'])
	.run(SetupAuthEvents)
	.controller('SignInCtrl', SignInCtrl)	
	;
	function SetupAuthEvents ($rootScope, $http, $modal, $auth) {
		
		function setupSigninStatus (response) {
			$rootScope.isLoggedIn = response.signin || false;
			$rootScope.mainRole = response.tipo || 'public';
			$rootScope.roles = response.roles || [];
			$rootScope.user = response.email || '';
			
			$rootScope.session = response || [];
		};
		
		var currentState = undefined;
		
		$rootScope.$on('event:auth-loginRequired', function(event, response) {
			console.log('event:auth-loginRequired');
			setupSigninStatus({});
        });
		$rootScope.$on('event:auth-loginConfirmed', function(event, response) {
			console.log('event:auth-loginConfirmed', response);
			// setupSigninStatus(response);
			// $rootScope.$state.go(currentState || response.tipo);
			// currentState = undefined;
			$rootScope.getSession( function(responseFromSession) {
				$rootScope.$state.go(currentState || responseFromSession.tipo);
				currentState = undefined;
			})
        });
		$rootScope.$on('event:auth-forbidden', function() {
			console.log('event:auth-forbidden');
        });
		
		$rootScope.signout = function() {
			
			$auth.logout();
			$http.delete('session', { data:{ destroy: true }}).then( done => done.data ).then( function(response) {
				console.log(response);
				setupSigninStatus(false);
				$rootScope.$state.go('v.home');
			});
		};
		
		$rootScope.terminos = function() {
			$modal.open({
				animation: true,
				templateUrl: 'views/terminos.html'
			});
		}
		
		$rootScope.getSession = function(next) {
			$http.get('session').then( done => done.data ).then( response => {
				setupSigninStatus(response);
				console.log('$rootScope.getSession', next, response);
				next && next(response);
			});
		}
		$rootScope.getSession();	//	run once
	};
	SetupAuthEvents.$inject = ['$rootScope','$http','$uibModal','$auth'];
	
	function SignInCtrl ($scope, $http, authService, $auth) {
	
		$scope.data = { email: "", password: "" };
		$scope.error = { notFound: false };
		
		$scope.post = function() {
			$http.post('/session', $scope.data)
			.then( done => done.data ).then( function(response) {
				if (response && response.signin) {
					if (response.roles.length) 
						response.roles[0].isOpen = true
					;
					authService.loginConfirmed(response);
				} else {
					$scope.error.notFound = true;
				}
			});
		};
		/*	https://github.com/sahat/satellizer
		*/
		$scope.auth = function(provider) {
			
			$auth.link(provider)
			.then( function(response) {
				
				console.log('Auth w/ %s, %O', provider, response);
				
				if (response.status==200) response = response.data;
				
				if (response && response.signin) {
					if (response.roles.length) 
						response.roles[0].isOpen = true
					;
					authService.loginConfirmed(response);
				} else {
					$scope.error.notFound = true;
				}
			})
			.catch( function(response) {
				console.log('Auth w/ %s FAILED, %O', provider, response);
			});
		}
	}
	SignInCtrl.$inject = ['$scope','$http','authService','$auth'];
})();
