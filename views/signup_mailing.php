<h1>Registración en MundoProfes</h1>

<h2>Se ha iniciado una registración para el email <?= $email; ?></h2>

<h2>Para completarla <a href="<?= base_url(), "#/confirm/$password"; ?>">haz click en este enlace</a>.</h2>

<h6>Si crees que has recibido este mensaje por error o no deseas continuar, simplemente ignora este correo - tu dirección será borrada de nuestra base en 24 hs.</h6>
