(function() {
	'use strict';
	angular.module('mp', [
		'mp.signin', 'ngAnimate',
		'ui.router', 'ui.bootstrap', 'ui.mask', 'ngMask', 'ui.validate', 
		'ngTagsInput', 'ngFileUpload',
		'satellizer',
		'oc.lazyLoad'
	])
	.config(['$httpProvider', function($httpProvider) {
		$httpProvider.defaults.headers.patch = {'Content-Type': 'application/json;charset=utf-8'};
	}])
	.config(['$authProvider', function($authProvider) {
		$authProvider.facebook({ clientId: '329674110489709' });
		$authProvider.google({ clientId: '273817871343-41eihdbe6uasep1t6qr54j495icpdm65.apps.googleusercontent.com' });
		// $authProvider.linkedin({ clientId: '77f8cpaemvqhsy' });
		$authProvider.linkedin({ clientId: '789ngcyw31ho5c' });
	}])
	.run(['$rootScope', function ($rootScope) {
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) { 
			if (toState.name=='profesor.alta-express')
				console.log('Entering state %s at %s', toState.name, new Date().toJSON());
		});
		$rootScope.$on("$stateChangeError", console.log.bind(console));
	}])
	.run(['$rootScope','$window', SetupWindowOrientation])
	;
	function SetupWindowOrientation ($rootScope, $window) {
		
		angular.element($window).bind('resize', function () {
			$rootScope.windowOrientation = ($window.innerWidth > $window.innerHeight ? 'landscape' : 'portrait');
		});
		$rootScope.windowOrientation = ($window.innerWidth > $window.innerHeight ? 'landscape' : 'portrait');
	};
})();
