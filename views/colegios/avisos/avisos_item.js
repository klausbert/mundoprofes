(function() {
	'use strict';
	angular.module('mp')
	.component('aviso', {
		bindings: { onUpdate: '&' },
		templateUrl: 'views/colegios/avisos/avisos_item.html',
		controller: ['$scope','$http', function($scope, $http) {
			var $ctrl = this;
			
			var id_aviso = $scope.$parent.$stateParams.id;
			var id_colegio = $scope.$parent.$stateParams.id_colegio;
			
			$scope.updateComuna = function(id, id_region) {
				$scope.item.id_comuna = id;
				$scope.item.id_region = id_region;
			}
			
			$scope.regiones = [];
			$http.get('regiones').then( done => done.data ).then( data => ($scope.regiones = data) );
			
			$scope.item = [];
			$scope.options = {};
			$http.get('colegios/avisos', { params:{ 
				id: id_aviso, 
				id_colegio: id_colegio	//	vuelvo a agregar para alta Sin Compra
			}})
			.then( done => {
				$scope.item = angular.copy(done.data);	// m�s completo
				$scope.options = $scope.item.options;
				delete $scope.item.options;
				$scope.item.fecha    = new Date($scope.item.fecha +'T12:00:00');
				$scope.item.vigencia = new Date($scope.item.vigencia +'T12:00:00');
				if ($scope.item.contrato_desde)
					$scope.item.contrato_desde = new Date($scope.item.contrato_desde +'T12:00:00');
				if ($scope.item.contrato_hasta)
					$scope.item.contrato_hasta = new Date($scope.item.contrato_hasta +'T12:00:00');
			});
			
			$scope.save = function(data) {
				var item = angular.copy(data);	// aisla de las transformaciones por si permanece
				
				if (item.descripcion && item.descripcion.match(/[A-Za-z0-9._%+-]+@[A-Za-z]+\.[A-Za-z.]+/g)) {
					item.descripcion = item.descripcion.replace(/[A-Za-z0-9._%+-]+@[A-Za-z]+\.[A-Za-z.]+/g, "**email**");
				}
				// if (item.comunas   && item.comunas.length)   item.id_comuna  = item.comunas[0].id;
				if (item.funciones && item.funciones.length) item.id_funcion = item.funciones[0].id;
				// arreglar fecha!
				item.fecha    = item.fecha.toJSON().slice(0, 10);
				item.vigencia = item.vigencia.toJSON().slice(0, 10);
				if (item.contrato_desde)	// y si es inmediata forzar NULL
					item.contrato_desde = item.contrato_desde.toJSON().slice(0, 10);
				if (item.contrato_hasta)
					item.contrato_hasta = item.contrato_hasta.toJSON().slice(0, 10);
				
				$http.put('colegios/avisos', item)
				.then( function(done) {
					$ctrl.onUpdate({ response: { id_colegio: id_colegio }});
				});
			}
		}],
	})
})();
