(function() {
	'use strict';
	angular.module('mp')
	.component('postulantes', {
		bindings: { id: '=', }, 
		templateUrl: 'views/colegios/avisos/avisos_postulantes.html',
		controller: ['$scope','$http', function($scope, $http) {
			$scope.list = [];
			$http.get(`colegios/avisos/postulantes/${this.id}`).then( done => done.data )
			.then( list => {
				$scope.list = list;
				$scope.list.map( function(m) { m.titulos = m.titulos.map( function(t) { return t.titulo; }).join(', '); });
				$scope.today = new Date();
			});
		}]
	})
})();
