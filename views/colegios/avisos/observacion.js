(() => {
	angular.module('mp')
	.component('avisoProfesorObservacionForm', {
		bindings: {
			resolve: '<',
			close: '&',
			dismiss: '&',
		},
		controller: [function() {
			this.$onInit = () => {
				this.observacion = this.resolve.observacion
			}
		}],
		template: `
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Agregar observaci&oacute;n del candidato</h3>
				</div>
				
				<div class="panel-body">
					<input class="form-control" type="text" ng-model="$ctrl.observacion" />
				</div>
				
				<div class="panel-footer">
					<button class="btn btn-danger"  ng-click="$ctrl.close({ $value: $ctrl.observacion })">Aceptar</button>
					
					<button class="btn btn-default float-right" ng-click="$ctrl.dismiss()">Cancelar</button>
				</div>
			</div>
		`,
	})
})()
