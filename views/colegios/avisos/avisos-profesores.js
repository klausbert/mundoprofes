(function () {
	angular.module('mp')
	.component('avisosProfesores', {
		bindings: { 
			id: '=',
			onSelect: '&' 
		},
		controller: ['$http','$uibModal', function($http, $modal) {
			let masterList = []	//	$http
			
			this.list = [];
			this.caption = '';
			this.filters = {
				active: {
					/* list of current filters */
				},
				apply: (oldList) => {
					let newList = angular.copy(oldList);
					Object.keys(this.filters.active).forEach( k => {
						newList = newList.filter( f => f[k]==this.filters.active[k] )
					})
					newList = newList.map( m => {
						m.changes = [/* prop1, prop2, prop3 */]
						return m
					})
					return newList
				},
				isActive: () => {
					return Object.keys(this.filters.active).filter( k => k!=='situacion' ).length > 0
				},
				options: {	/* for dropdowns */
					comuna     : [], profesion  : [], experiencia: [], puntaje    : [], 
				},
				reload: () => {
					this.list = this.filters.apply(masterList)
					
					this.grid.filter( f => f.situacion!=='Todos')
					.forEach( i => i.cantidad = masterList.filter( f => f.situacion==i.situacion ).length )
					
					// limitar las opciones de los otros filtros
					Object.keys(this.filters.options).forEach( k => {
						var filteredOptions = new Set(
							this.list.map( m => m[k] ).sort()	//	MISSING: apply() here!!	//	what?
						);
						this.filters.options[k] = [...filteredOptions ].map( m => { return { value: m, label: m }})
					})
					//	caption
					var cantidad = this.list.length;
					
					if (this.filters.active.situacion) {
						this.caption  = 'CANDIDATOS '+ this.filters.active.situacion.toUpperCase()
						this.caption += this.filters.active.situacion.substr(-1, 1)=='o' ? 'S' : '';
					} else {
						this.caption  = 'TODOS';
					}
					this.caption +=' ('+ cantidad +')'
				},
				reset: () => {
					this.filters.show = false
					
					Object.keys(this.select).filter( k => k!=='situacion' ).forEach( k => {
						this.select[k] = '';
						this.filters.set(k, '');
					})
				},
				set: (filter, value) => {
					if (filter=='situacion' && value=='Todos')
						value = '';
					
					/* add or remove to active set */
					if (value)
						this.filters.active[filter] = value;
					else
						delete this.filters.active[filter];
					
					this.filters.reload();
				},
				show: false,
			}
			this.order = {
				by: '',
				reverse: false,
				set: (column) => {
					if (this.order.by===column)
						this.order.reverse = ! this.order.reverse;
					else
						this.order.by = column;
				}
			}
			this.select = {};
			Object.keys(this.filters.options).forEach( k => this.select[k] = '' )	// ng-models for <select>
			
			this.head = {};
			this.grid = [];
			
			this.$onInit = () => {
				$http.get(`colegios/avisos?id=${this.id}&fields=busqueda`).then( done => done.data )
				.then( data => this.head = data )
				
				$http.get(`colegios/avisos_profesores?id_aviso=${this.id}`).then( done => done.data )
				.then( data => {
					masterList = data.list;
					// this.head = data.head;
					
					//	botones de estado
					this.grid = [{ situacion: 'Todos', cantidad: data.list.length }];	// no deberia cambiar durante la sesión
					data.enum.situacion.forEach( m => this.grid.push({ situacion: m, cantidad: 0 }) );
					
					//	lista filtrada inicial
					this.filters.set('situacion', '');	
				})
			}
			this.observacion = (item) => {
				$modal.open({ 
					component: 'aviso-profesor-observacion-form',
					resolve: {
						observacion: () => (item.observacion),
					},
					size: 'sm', 
				}).result.then( 
					done => {
						if (done) {
							item.observacion = done
							this.changes.push(item, 'observacion')
						}
					},
					fail => null
				)
			}
			this.changes = {
				list: () => this.list.filter( f => f.changes.length ),
				push: (item, prop) => {
					item.changes = item.changes.filter( f => f!==prop )
					
					if (item[prop]!==masterList.find( s => s.id==item.id )[prop])
						item.changes.push(prop)
				},
				save: () => {
					const list = this.changes.list()
					
					this.changes.saving = list.length
					
					list.forEach( item => {
						masterList.filter( s => s.id==item.id )	// one
						.map( orig => {
							let change = { id: item.id }
							
							item.changes.forEach( k => {
								change[k] = item[k]
								orig[k] = item[k]	//	optimistic local
							})
							$http.put(`colegios/avisos_profesores`, change)	//	optimistic remote
							.then(() => --this.changes.saving )
							
							item.changes = []	//	reset
							
							this.filters.reload();	//	repeating
						})
					})
				},
				saving: 0,
			}
		}],
		templateUrl: 'views/colegios/avisos/avisos-profesores.html',
	})
})();
