(function() {
	'use strict';
	angular.module('mp')
	.component('avisos', {
		bindings: { id: '=', }, 
		templateUrl: 'views/colegios/avisos/avisos_list.html',
		controller: ['$scope','$http', function($scope, $http) {
			
			$scope.list = [];
			
			this.$onChanges = () => {
				if (this.id)
				$http.get('colegios/avisos', { params:{ id_colegio: this.id }})
				.then( done => { $scope.list = done.data || [] });
			}
		}]
	})
})();
