(function() {
	angular.module('mp')
	.controller('ColegioDatosColegioCtrl', ColegioDatosColegioCtrl)
	;
	function ColegioDatosColegioCtrl($scope, $state, $stateParams, $http) {
			
		$scope.comunas = [];
		$http.get('comunas').then( done => done.data ).then( function(response) {
			$scope.comunas = response;
		});
		
		$scope.items = { put: {} };
		
		$http.get('colegios/datos', { params:{ id: $stateParams.id_colegio }})
		.then( done => done.data ).then( function(response) {
			$scope.items.put = response;
		});
		$scope.put = function() {
			$http.put('colegios/datos', $scope.items.put).then( done => done.data ).then( function(response) {
				$scope.items.put = response;
				alert('Sus datos han sido actualizados');
			});
		}
	};
	ColegioDatosColegioCtrl.$inject = ['$scope','$state','$stateParams','$http'];
})();
