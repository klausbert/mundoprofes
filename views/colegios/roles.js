(function () {
	angular.module('mp')
	.component('roles', {
		templateUrl: 'views/colegios/roles.html',
		bindings: { id: '<' }, 
		controller: ['$scope','$http', function($scope, $http) {
			this.$onChanges = () => {
				if (! this.id) return;
				refresh($scope, $http, this.id);
				$scope.add = { email: '', RUT: '', role: '', id: this.id };
			}
			$scope.save = (item) => {
				$http.post('colegios/roles', item)
				.catch( err => (err && err.data && err.data.error && alert(err.data.error)))
				.then( done => console.log(done, refresh($scope, $http, this.id)));
			}
			$scope.kill = (item) => {
				if (! confirm('Quitar '+ item.id)) return;
				
				$http.delete('colegios/roles', { data:{ id: item.id }})
				.then( done => console.log(done, refresh($scope, $http, item.id_colegio)));
			}
		}]
	})
	
	function refresh($scope, $http, id) {
		if (! id) return console.table({ error: 'No id'})

		return $http.get('colegios/roles', { params:{ id_colegio: id }})
		.then( done => done.data )
		.then( data => {
			var any = data ? data.filter( f => f.propio ) : null;
			var role = any && any.length ? any.pop().role : '';
			var roles = ['supervisor', 'vendedor', 'sostenedor', 'director', 'gestor'];
			$scope.roles = roles.slice(1 + roles.indexOf(role));
			$scope.list = data ? data.filter( f => ! f.propio ) : [];
			return data;
		});
	}
})();
