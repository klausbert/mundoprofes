(function() {
	angular.module('mp')
	.config(['$stateProvider', function ($stateProvider) { $stateProvider
		.state('colegios.busquedas.list', {
			url: '/list/:id', 
			templateUrl: 'views/colegios/busquedas_list.html', 
			controller: function($scope, $state, $stateParams, $http) 
			{		
				$scope.list = [];
				$http.get('colegios/busquedas', { params:{ id_colegio: $stateParams.id }})
				.then( done => done.data ).then( function(response) {
					$scope.list = response.list;
				});
				
				$scope.form = function(id) {
					$state.go('colegios.busquedas.form', { id: id, id_colegio: $stateParams.id });
				};
				
				$scope.delete = function(id) {
					$http.delete('colegios/busquedas', { params:{ id: id }})
					.then( done => done.data ).then( function(response) {
						console.log('deleted? ', response);
						$scope.list = $scope.list.filter( function(f) { return f.id != id; });
					});
				};
			}
		})
		.state('colegios.busquedas.form', { 
			url: '/form/:id/:id_colegio', 
			templateUrl: 'views/colegios/busquedas_form.html', 
			controller: function($scope, $state, $stateParams, $http) 
			{
				$scope.item = {};
				$scope.options = {};
				$http.get('colegios/busquedas', { params:{ id: $stateParams.id, id_colegio: $stateParams.id_colegio }})
				.then( done => done.data ).then( function(response) {
					$scope.item = response.item;
					$scope.options = response.options;
				});
				
				$scope.titMgr = {
					id: 0,
					add: function() {
						$scope.item.titulos.some( function(s) { return s.id==$scope.titMgr.id; })
						|| $scope.item.titulos.push($scope.titulos.filter( function(f) { return f.id==$scope.titMgr.id; })[0]);
					},
					remove: function(id) {
						// $scope.titMgr.id = id;
						$scope.item.titulos = $scope.item.titulos.filter( function(f) { return f.id!==id; });
					}
				}
				$scope.titulos = [];
				$http.get('titulos').then( done => done.data ).then( function(response) {
					$scope.titulos = response;
				});
				$scope.cargos = [];
				$http.get('cargos', { params: { tipo: 'Colegio' }}).then( done => done.data ).then( function(response) {
					$scope.cargos = response;
				});
				$scope.asignaturas = [];
				$http.get('asignaturas').then( done => done.data ).then( function(response) {
					$scope.asignaturas = response;
				});
				$scope.niveles = [];
				$http.get('niveles').then( done => done.data ).then( function(response) {
					$scope.niveles = response;
				});
				
				$scope.save = function() {
					if ($scope.item.id) {
						$http.put('colegios/busquedas', $scope.item).then( done => done.data ).then( function(response) {
							$scope.item = response.item;
							$state.go('^.list', { id: $scope.item.id_colegio });
						});
					} else {
						$http.post('colegios/busquedas', $scope.item).then( done => done.data ).then( function(response) {
							$scope.item = response.item;
							$state.go('^.list', { id: $scope.item.id_colegio });
						});
					}
				}
			}
		})
	}])
})();
