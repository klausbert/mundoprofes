(function() {
	angular.module('mp')
	.controller('ColegioDatosAdminCtrl', ['$scope', '$state', '$http', function($scope, $state, $http) {
			
		$scope.items = { put: {} };
		
		$http.get('colegios/datos_admin').then( done => done.data ).then( function(response) {
			$scope.items.put = response;
		});
		$scope.put = function() {
			$http.put('colegios/datos_admin', $scope.items.put).then( done => done.data ).then( function(response) {
				$scope.items.put = response;
				alert('Sus datos han sido actualizados');
			});
		};
		$scope.validaRut = function(rut) {
			
			var error = validaRut(rut);

			$scope.form.rut.$valid = (rut!==undefined && error=='');
			
			return $scope.form.rut.$valid;
		};
	}])
})();
