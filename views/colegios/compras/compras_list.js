(function() {
	'use strict';
	angular.module('mp')
	.component('compras', {
		bindings: { 
			id: '=', 
			onSelect: '&',
		},
		templateUrl: 'views/colegios/compras/compras_list.html',
		controller: ['$http', function($http) {
			
			this.list = [];
			
			this.$onChanges = () => {
				if (this.id)
				$http.get('colegios/compras', { params:{ id_colegio: this.id }})
				.then( done => done.data )
				.then( response => {
					if (response instanceof Array) this.list = response;
				});
			}
		}]
	})
})();
