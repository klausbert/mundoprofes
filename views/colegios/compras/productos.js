(function() {
	'use strict';
	angular.module('mp')
	.component('productos', {
		bindings: {
			idColegio: '<',
			onSelect: '&',
		},
		templateUrl: 'views/colegios/compras/productos.html',
		controller: ['$http','$uibModal', function($http, $modal) {
			var $ctrl = this;
			
			$ctrl.list = [];
			$http.get('general/productos')
			.then( function(done) { $ctrl.list = done.data; });
			
			$ctrl.text = function(id) {
				if (id)
				$modal.open({
					template: '<producto id="'+ id +'" />',
					size: 'lg',
				});
			}
			//	selección
			$ctrl.id_producto = 0;
			
			$ctrl.inSet = function(set) {
				// console.log('check if id %s is in set %O', id, set);
				// return false;
				return set.some( function(s) { return s.id==$ctrl.id_producto; });
			};
			
			$ctrl.checkout = function(id) {
				 $ctrl.onSelect({ response:{ id_colegio: $ctrl.idColegio, id_producto: $ctrl.id_producto }})
			}
		}]
	})
})();
