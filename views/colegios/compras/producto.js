(function() {
	angular.module('mp')
	.component('producto', {
		bindings: { id: '@' }, 
		templateUrl: 'views/colegios/compras/producto.html',
		controller: ['$http','$sce', function($http, $sce) {
			var self = this;
			
			this.title = '';
			this.content = '';
			
			$http.get('wp', { params:{ json: 'get_post', post_id: this.id }})
			.then( function(done) {
				console.log(done);
				self.title = $sce.trustAsHtml(done.data.post.title);
				self.content = $sce.trustAsHtml(done.data.post.content);
			});
		}]
	})
})();
