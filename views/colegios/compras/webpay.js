(function() {
	'use strict';
	angular.module('mp')
	.component('webpay', {
		bindings: { 
			colegio: '=', 
			result: '=',
			session: '=',
		},
		controller: ['$http', function($http) {
			var $ctrl = this;
			
			$http.get('colegios/webpay/data/'+ $ctrl.session)
			.then( function(done) {
				console.log(done);
				$ctrl.item = done.data;
				
				var md = '0'+ $ctrl.item.TBK_FECHA_TRANSACCION; 
				$ctrl.item.TBK_FECHA_TRANSACCION = 
					md.substr(-2, 2) +'/'+
					md.substr(-4, 2) +'/'+
					md.substr(-8, 4);
					
				var hms = '0'+ $ctrl.item.TBK_HORA_TRANSACCION; 
				$ctrl.item.TBK_HORA_TRANSACCION = 
					hms.substr(-6, 2) +':'+
					hms.substr(-4, 2) +':'+
					hms.substr(-2, 2);
			})
			.catch( function(fail) {
				// alert('fail');
				console.log(fail);
			});
			
			$ctrl.tipoDePago  = {'VD': 'D�bito', 'VN': 'Cr�dito', 'VC': 'Cr�dito', 'SI': 'Cr�dito', 'S2': 'Cr�dito', 'NC': 'Cr�dito'};
			$ctrl.tipoDeCuota = {'VD': 'Venta d�bito', 'VN': 'Sin cuotas', 'VC': 'Cuotas normales', 'SI': 'Sin inter�s', 'S2': 'Sin inter�s', 'NC': 'Sin inter�s'};
		}],
		templateUrl: 'views/colegios/compras/webpay.html',
	})
})();
