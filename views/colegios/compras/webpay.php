<?
function force_base_url($relative)
{
	// return base_url($relative);
	return str_ireplace('https:', 'http:', base_url($relative));
}
?>
<html>
	<body>
		<h1>Un momento, por favor:</h1>
		<h2>Reenviando datos a WebPay...</h2>
		
		<form action="<?= force_base_url('cgi-bin/tbk_bp_pago.cgi'); ?>" name="frm" method="post" id="form">
			<input type="hidden" name="TBK_TIPO_TRANSACCION" value="TR_NORMAL" />
			<input type="hidden" name="TBK_MONTO" value="<?= $precio; ?>00" />
			<input type="hidden" name="TBK_ORDEN_COMPRA" value="<?= $TBK_ORDEN_COMPRA; ?>"/>
			<input type="hidden" name="TBK_ID_SESION" value="<?= $TBK_ID_SESION; ?>"/>
			<input type="hidden" name="TBK_URL_EXITO" value="<?= base_url('colegios/webpay/done'); ?>"/>
			<input type="hidden" name="TBK_URL_FRACASO" value="<?= base_url('colegios/webpay/fail'); ?>"/>
			<input type="hidden" value="submit">
		</form>
	</body>
	
	<script>
		(function() {
			document.getElementById('form').submit();
		})();
	</script>
</html>
