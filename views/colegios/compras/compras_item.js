((app) => {
	app.component('compra', {
		bindings: { colegio: '<', producto: '<' },
		templateUrl: 'views/colegios/compras/compras_item.html',
		controller: ['$http', function($http) {
			this.$onChanges = (obj) => {
				this.producto &&
				$http.get('general/productos', { params:{ id: this.producto }}).then( done => done.data )
				.then( data => {
					if (data && data instanceof Object) {
						this.item = data;
						this.item.id_colegio  = this.colegio;
						this.item.id_producto = this.producto;
						this.item.pago_fecha  = new Date();
						this.item.pago_forma = 'transferencia';	// default
						this.status = 'submit';
					}
				});
			}
			this.save = (item) => {
				this.status = 'processing';
				item.pago_fecha  = item.pago_fecha.toJSON().slice(0, 10);
				$http.post('colegios/compras', item).then( done => done.data )
				.then( data => {
					console.log('Nueva compra creada: %O', data);
					this.status = 'created';
				});
			};
			
			this.status = 'waiting';
		}]
	})
})(angular.module('mp'));
