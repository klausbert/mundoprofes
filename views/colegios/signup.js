(function() { 
	angular.module('mp')
	.component('signupColegio', {
		bindings: { onUpdate: '&' },
		templateUrl: 'views/colegios/signup.html',
		controller: ['$scope','$http','$q', function($scope, $http, $q) {
			var $ctrl = this;
			
			$scope.item = { rbd: "", colegio: "", rut: "", profesor: "", error: "", status: "" };
			$scope.valid = { rbd: false, rut: false };
			$scope.buscar_por = 'R';	// input radio
			$scope.estoy_inscripto = 'S';
			$scope.configModel = {
				updateOn: 'default blur',
				debounce: { default: 500, blur: 0 }
			};

			$scope.validaRbd = function(rbd) {
				return $q( function(resolve, reject) {
					if (rbd) {
						$scope.item.colegio = '(Buscando...)';
						$http.get('colegios', { params:{ id: rbd }})
						.then( function(done) {
							$scope.valid.rbd = done.data.length;
							if ($scope.valid.rbd) {
								$scope.item.colegio = done.data[0].nombre;
								$scope.updateColegio(done.data[0]);
								resolve(true);
							} else {
								$scope.item.colegio = '';
								reject(false);
							}
						});
					} else
						resolve(true);
				});
			};
			$scope.validaRut = function(rut) {
				if (rut==undefined || rut=='')
					return;
				
				$scope.item.error = validaRut(rut);	// validaRut.js

				if (rut!==undefined && $scope.item.error=='') {
					
					$http.get('profesor', { params:{ RUT: rut.replace(/\.+/g, '').replace(/-+/g, '') }})
					.then( done => done.data ).then( function(response) {
						$scope.valid.rut = response.length;
						if ($scope.valid.rut) {
							var p = response[0];
							$scope.item.profesor = [p.nombres, p.apellidos, p.apellido_m].join(' ');
						}
					});
					return true;
				}
				return false;
			};
			$scope.updateColegio = function(item) {
				$scope.sostenedor = (item.sostenedor > 0);
				if ($scope.sostenedor)
					alert('Este Colegio ya tiene un Sostenedor asignado');
			};
			$scope.updateProfesor = function(params) {
				$scope.item.id_profesor = params.id;
				$scope.item.email = params.email;
				//
				console.log('Send registration form with data %O', $scope.item);
				$scope.post();
			};
			$scope.post = function() {
				$http.post('colegios/signup', $scope.item)
				.then( done => done.data ).then( function(response) {
					console.log(response);
					
					if (response.error) {
						$scope.item.error = response.error;
					} else 
					if (response.email) {
						$scope.item.error = '';
						$scope.item.status = response.status;
						$scope.item.email = response.email;
						alert('Le hemos enviado un email a '+ $scope.item.email);
						$ctrl.onUpdate(response);	// call parent to change state
					} else {
						alert(response);
					}
				})
				.error( function(data, status, header, config) {
					console.log('Error status ', status);
					console.log('Error header ', header);
				});
			};
		}]
	})
})();
