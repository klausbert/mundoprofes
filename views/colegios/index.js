(function () {
	'use strict';
	angular.module('mp')
	.config(['$stateProvider', function ($sp) { $sp
		//	PUBLICO 
		.state('v.signup2', {
			url: '/colegios_signup', 
			template: '<signup-colegio on-update="$state.go(\'v.home\')" />',
		})
		//	PRIVADO
		.state('colegios', { 
			resolve: { ocLazyLoad: ['$ocLazyLoad', function($ocLazyLoad) {
				var modules = [
					'datos_colegio', 'datos_admin', 'roles',
					'compras/compras_list', 'compras/compras_item', 'compras/productos', 'compras/producto', 'compras/webpay',
					'../general/datepicker', 
					'avisos/avisos_list', 'avisos/avisos_item', 'avisos/avisos-profesores', 'avisos/observacion'
				];
				return $ocLazyLoad.load( modules.map( function(m) {
					return 'scripts/colegios/'+ m +'.js';
				}));
			}]},
			url: '/colegios', 
			templateUrl: 'views/colegios/index.html', 
			controller: ['$scope','$http','$sce', function($scope, $http, $sce) {
				var $ctrl = this;
				
				$http.get('general/wp?name=portada-colegios').then(
					function(done) {
						$ctrl.item = done.data || {};
						$ctrl.item.content = $sce.trustAsHtml($ctrl.item.content);
					}
				);
			}],
			controllerAs: '$ctrl'
		})		
		.state('colegios.roles', { 
			url: '/:id_colegio/roles', 
			template: '<roles id="$stateParams.id_colegio" />'
		})
		.state('colegios.datos', { 
			url: '/:id_colegio/datos', 
			templateUrl: 'views/colegios/datos_colegio.html', 
			controller: 'ColegioDatosColegioCtrl' 
		})
		.state('colegios.admin', { 
			url: '/:id_colegio/admin', 
			templateUrl: 'views/colegios/datos_admin.html', 
			controller: 'ColegioDatosAdminCtrl' 
		})
		//	COMPRAS
		.state('colegios.compras', { 
			url: '/:id_colegio/compras', 
			template: '<compras id="$stateParams.id_colegio" on-select="$state.current.action($state)" />',
			action: function($state) {
				console.log($state.current.name, $state.params);
				$state.go('colegios.productos', $state.params);
			}
		})
		.state('colegios.productos', { 
			url: '/:id_colegio/productos', 
			template: '<productos id-colegio="$stateParams.id_colegio" on-select="$state.current.action($state, response)" />', 
			action: function($state, response) {
				$state.go('colegios.compra', response);
			}
		})
		.state('colegios.compra', { 
			url: '/:id_colegio/productos/:id_producto', 
			template: '<compra colegio="$stateParams.id_colegio" producto="$stateParams.id_producto" ui-view />'
		})
		.state('colegios.compra.webpay', { 
			url: '/webpay/:result/:session',
			template: '<webpay colegio="$stateParams.id_colegio" result="$stateParams.result" session="$stateParams.session" />',
		})
		//	AVISOS
		.state('colegios.avisos', { 
			url: '/:id_colegio/avisos', 
			template: '<avisos id="$stateParams.id_colegio" />'
		})
		.state('colegios.aviso', { 
			url: '/:id_colegio/avisos/:id', 
			template: '<aviso on-update="$state.current.action($state, response)" />', 
			action: function($state, response) {
				$state.go('colegios.avisos', response, { reload: true });
			}
		})
		.state('colegios.postulantes', { 
			url: '/:id_colegio/avisos/:id/postulantes', 
			template: '<avisos-profesores id="$stateParams.id" />', 
		})
	}])
})();
