(function() { 
	angular.module('mp')
	.component('first', {
		bindings: { hash: '<' },
		templateUrl: 'views/first.html',
		controller: ['$http', function($http) {
			var $ctrl = this;
			
			$ctrl.data = { email: '', pass: '', reset: $ctrl.hash, status: 'pending' };
			
			$http.get('confirm', { params:{ reset: $ctrl.hash }}).then(
				function(done) {
					if (done) $ctrl.data.email = done.data.email;
				}
			);
			$ctrl.save = function() {
				$http.put('reset', $ctrl.data).then(
					function(done) {
						$ctrl.data.status = 'complete';
					}
				);
			};
		}]
	})
})();
