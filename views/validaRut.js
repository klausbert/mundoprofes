var validaRut = function(rut) {
	if (rut==undefined)
		return '';
		
	rut = rut.replace(/\.+/g, '').replace(/-+/g, '');
	
	var rexp = new RegExp(/^([0-9]){8}([kK0-9]){1}$/);
	if (rut.match(rexp)) {
		var valor = 0;
		var dv = '';
		
		valor += parseInt( rut.substr(7, 1)) * 2;
		valor += parseInt( rut.substr(6, 1)) * 3;
		valor += parseInt( rut.substr(5, 1)) * 4;
		valor += parseInt( rut.substr(4, 1)) * 5;
		valor += parseInt( rut.substr(3, 1)) * 6;
		valor += parseInt( rut.substr(2, 1)) * 7;
		valor += parseInt( rut.substr(1, 1)) * 2;
		valor += parseInt( rut.substr(0, 1)) * 3;
		
		valor = valor % 11;
		if (valor==0) dv = '0';
		if (valor==1) dv = 'k';
		if (valor>=2) dv = (11 - valor).toString().substr(0, 1);
		
		if (dv!=rut.substr(-1, 1).toLowerCase())
			return 'El RUT es inv�lido';
		else
			return '';
	} else {
		return 'Formato de RUT incorrecto';
	}
}
