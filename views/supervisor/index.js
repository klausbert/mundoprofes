(function () {
	'use strict';
	angular.module('mp')
	.config(['$stateProvider', function ($sp) { $sp
		.state('supervisor', { 
			resolve: { ocLazyLoad: function($ocLazyLoad) {
				var modules = [
					'vendedores', 'colegios', 
				];
				return $ocLazyLoad.load( modules.map( function(m) {
					return 'scripts/supervisor/'+ m +'.js';
				}));
			}},
			url: '/supervisor',
			templateUrl: 'views/_index_member.html',
		})
		.state('supervisor.vendedores', { 
			url: '/vendedores', 
			template: '<supervisor-vendedores on-select="$state.current.action($state, response)" ui-view />', 
			action: function($state, response) {
				$state.go('supervisor.vendedores.colegios', response);
			}
		})
		.state('supervisor.vendedores.colegios', { 
			url: '/{id_role:int}', 
			controller: ['$uibModal', '$state', function($modal, $state) {
				$modal.open({
					animation: true,
					template: '<supervisor-colegios id-role="$stateParams.id_role" />',
				}).result
				.then({}, 
				function(fail) {
					console.log('Fail %O', fail);
					$state.go('supervisor.vendedores', {}, { reload: true })
				});
			}],
		})
	}])
})();
