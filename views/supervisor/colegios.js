(function () {
	'use strict';
	angular.module('mp')
	.component('supervisorColegios', {
		bindings: { idRole: '<', onUpdate: '&' }, 
		templateUrl: 'views/supervisor/colegios.html',
		controller: ['$http', function($http) {
			var $ctrl = this;
			
			$ctrl.list = [];
			$http.get('supervisor/colegios')	// los colegios previamente asignados al supervisor
			.then( function(done) {
				$ctrl.list = done.data;	// ver acá cuáles fueron asignados a este id_role
			});

			$ctrl.swap = function(item) {
				if (item.id_role==undefined) {
					item.id_role = $ctrl.idRole;
					
					$http.post('supervisor/colegios', { id_role: $ctrl.idRole, id_colegio: item.id })
					.then( function(done) {
						console.log('colegios [POST] response %O', done);
						$ctrl.onUpdate({ response:{ id_role: item.id_role, id_colegio: item.id }});
					});
				} else {
					item.id_role = undefined;
					
					$http.delete('supervisor/colegios', { data:{ id_role: $ctrl.idRole, id_colegio: item.id }})
					.then( function(done) {
						console.log('colegios [DELETE] response %O', done);
						$ctrl.onUpdate({ response:{ id_role: item.id_role, id_colegio: item.id }});
					});
				}
			}
		}]
	})
})();
