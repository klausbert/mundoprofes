(function () {
	angular.module('mp')
	.component('supervisorVendedores', {
		bindings: { onSelect: '&' },
		templateUrl: 'views/supervisor/vendedores.html',
		controller: ['$http', function($http) {
			var $ctrl = this;
			
			$ctrl.list = [];
			$http.get('supervisor/vendedores').then( function(done) {
				$ctrl.list = done.data;
			});
			
			$ctrl.user = { email: '' };
			
			$ctrl.select = function(item) {
				$ctrl.user = item;
				$ctrl.user.role = 'vendedor';
			}
			$ctrl.open = function(item) {
				$ctrl.onSelect({ response:{ id_role: item.id }});
			}
			$ctrl.save = function() {
				if ($ctrl.user.id && $ctrl.user.role)
				$http.post('supervisor/vendedores', $ctrl.user)
				.then( function(done) {
					if (done.status==201) {
						$ctrl.list.push( angular.copy($ctrl.user));
						$ctrl.user = { email: '' };
					}
				}, function(fail) {
					if (fail.status==400 && fail.data.error) {
						alert('ERROR: '+ fail.data.error);
					} else {
						console.log('failed with %O', fail);						
					}
				});
			}
			$ctrl.kill = function(item) {
				if (! confirm('Quitar '+ item.role +' '+ item.apellidos)) return;
				
				$http.delete('supervisor/vendedores', { data:{ id: item.id }})
				.then( function(done) {
					if (done.status==200) {
						$ctrl.list = $ctrl.list.filter(function(f) { return f.id!=item.id; }) ;
					}
				});
			}
		}]
	})
})();
