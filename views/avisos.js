(function () {
	'use strict';
	angular.module('mp')
	.config(['$stateProvider', function ($stateProvider) { $stateProvider
		.state('v.avisos',      { 
			url: '/avisos', 
			templateUrl: 'views/profesor/avisos_list.html?v=20190311', 
			data: { listUrl: 'general/avisos' },
			controller: 'AvisosListCtrl'
		})
		.state('v.aviso', { 
			url: '/aviso/:id', 
			templateUrl: 'views/profesor/avisos_form.html', 
			data: { listUrl: 'general/avisos' },
			controller: 'AvisosFormCtrl'
		})
	}])
})();
