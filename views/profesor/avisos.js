(function () {
	'use strict';
	angular.module('mp')
	.controller('AvisosListCtrl', AvisosListCtrl)
	.controller('AvisosFormCtrl', AvisosFormCtrl)
	;
	function AvisosListCtrl($scope, $state, $http) 
	{
		$scope.list = [];
		$http.get($state.current.data.listUrl)
		.then( done => done.data ).then( response => {
			$scope.list = response.list;
		});
	};
	AvisosListCtrl.$inject = ['$scope','$state','$http'];
		
	function AvisosFormCtrl($rootScope, $scope, $state, $stateParams, $http) 
	{
		$scope.item = {};
		$scope.options = {};
		
		$http.get($state.current.data.listUrl, { params:{ id: $stateParams.id }})
		.then( done => done.data ).then( response => {
			$scope.item = response.item;
			$scope.options = response.options;
			
			if ($scope.item.id_region)
			$http.get('regiones', { params:{ id: $scope.item.id_region }})
			.then( done => done.data ).then( response => {
				$scope.item.region = response[0].region;
			});
			if ($scope.item.id_comuna)
			$http.get('comunas' , { params:{ id: $scope.item.id_comuna }})
			.then( done => done.data ).then( response => {
				$scope.item.comuna = response[0].comuna;
			});
		});
		
		$scope.save = function() {
			if (! $rootScope.isLoggedIn) {
				$state.go('v.signup');
			} else
			if ($scope.item.profesor.postulado) {
				$http.delete($state.current.data.listUrl, {data:{ id_aviso: $stateParams.id }})
				.then( done => done.data ).then( response => {
					if (response && response.success) {
						delete $scope.item.profesor.postulado;
						$state.go('^.avisos');
					}	
				});
			} else {
				$http.post($state.current.data.listUrl, { id_aviso: $stateParams.id })
				.then( done => done.data ).then( response => {
					if (response.id) {
						$scope.item.profesor.postulado = response.id;
						$state.go('^.avisos');
					}
				});
			}
		};
	};
	AvisosFormCtrl.$inject = ['$rootScope','$scope','$state','$stateParams','$http'];
})();
