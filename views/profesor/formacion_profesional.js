(function() {
    'use strict';
	angular.module('mp')
	.directive('formacionProfesional', function() { return {
		restrict: 'E',
		scope:{ id: '=' },
		templateUrl: 'views/profesor/formacion_profesional.html',
		controller: ['$scope','$http', function($scope, $http) {

			// $scope.readonly = $scope.$state.current.data && $scope.$state.current.data.readonly;
			
			$scope.instituciones = [];
			$scope.instituciones_x_tipo = function() {
				$http.get('instituciones', { params: { tipo: $scope.post.tipo }})
				.then( done => done.data ).then( function(response) {
					$scope.instituciones = response;
				});
			};
			
			$scope.titulos_profesionales = [];
			$http.get('titulos_profesionales')
			.then( done => done.data ).then( function(response) {
				$scope.titulos_profesionales = response;
			});
			
			$scope.list = [];
			$scope.post = { id_profesor: $scope.id };
			//	admin could set id parameter here!
			$http.get('profesor/formacion_profesional', { params:{ id_profesor: $scope.id }})
			.then( done => done.data ).then( function(response) 
			{
				$scope.list = (typeof(response)=='object') ? response : [];
				
				if (response.error) alert(response.error);
			});
			$scope.save = function() {
				$http.post('profesor/formacion_profesional', $scope.post)
				.then( done => done.data ).then( function(response) 
				{
					if (typeof(response)=='object') {
						$scope.list.push( response );	
						$scope.post = { id_profesor: $scope.id };
					}
				});
			};
			$scope.delete = function(id_registro) {
				$http.delete('profesor/formacion_profesional', { data: { id: id_registro }})
				.then( done => done.data ).then( function(response) {
					
					angular.forEach($scope.list, function(item, index) {
						if (item.id==id_registro) {
							$scope.post = $scope.list[index];
							delete $scope.post.id;
							
							$scope.list.splice(index, 1);
						}
					});
				});
			};
		}]
	}})
})();
