(function() { 
	angular.module('mp')
	.component('signupProfesor', {
		bindings: { onUpdate: '&' },
		templateUrl: 'views/profesor/signup.html',
		controller: ['$http','authService','$auth', function($http, authService, $auth) {
			var $ctrl = this;
			
			$ctrl.signup = {email: "", password: ""};
			$ctrl.signupButtonVisible = true;
			
			$ctrl.post = function() {
				// $ctrl.signupButtonVisible = false;
				
				$http.post('profesor/signup', $ctrl.signup).then( done => done.data )
				.then( data => {
					$ctrl.signup = data;
					$ctrl.signup.provider = 'email';
					$ctrl.onUpdate({ response: $ctrl.signup });
					
					authService.loginConfirmed(data);
					
					$http.put('forgot', $ctrl.signup).then( done => done.data ).then( data => console.log(data) );
				})
				.catch( fail => {
					$ctrl.signup.error = fail.error;	// ?
					$ctrl.signup.password = "";
					$ctrl.signupButtonVisible = true;
				});
			};
			/*	https://github.com/sahat/satellizer
			*/
			$ctrl.auth = function(provider) {
				
				$auth.link(provider)
				.then( function(done) {
					
					console.log('Auth w/ %s, %O', provider, done);
					
					if (done && done.data && done.data.signin) {
						if (done.data.roles.length) 
							done.data.roles[0].isOpen = true
						;
						authService.loginConfirmed(done.data);
						
						$ctrl.signup = done.data;
						$ctrl.signup.provider = provider;
						$ctrl.onUpdate($ctrl.signup);
					} else {
						$ctrl.error.notFound = true;
					}
				})
				.catch( function(fail) {
					console.log('Auth w/ %s FAILED, %O', provider, fail);
				});
			}
		}]
	})
})();
