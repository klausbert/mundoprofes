(function () {
	'use strict';
	angular.module('mp')
	.component('altaExpress', {
		bindings: { response: '&' },
		templateUrl: 'views/profesor/alta_express.html',
		controller: ['$scope','$http','Upload', function($scope, $http, $upload) {
			var $ctrl = this;
			
			$scope.cv = {};
			$http.get('profesor/alta_express').then( done => done.data )
			.then( response => {
				console.log('leyendo alta express - %s', new Date().toJSON());
				$scope.cv = response;
				if (response.RUT)
					$scope.cv.RUT = ('000000000'+ response.RUT).slice(-9);	// hotfix auto conversion
			});
			
			$scope.validaRut = function(rut) {
				
				$scope.form.rut.errorMsg = validaRut(rut);	// validación interna
				
				if (rut===undefined || $scope.form.rut.errorMsg=='') {
					$scope.form.rut.$setValidity('unique', true);	// temp turn off 
					return true;
				}
				return false;
			};
			$scope.status = '';
			$scope.put = function() {
				if ($scope.cv.RUT!=undefined && $scope.cv.RUT=='')
					delete $scope.cv.RUT;
				
				$scope.status = 'saving';
				$http.put('profesor/alta_express', $scope.cv).then( done => done.data )
				.then( response => {
					$scope.status = '';
					if (response.error) {
						alert(response.error.map( m => m.message).join('\n'));
					} else {
						$ctrl.response({ request: response.request });
					}
				});
			};
			
			$scope.progressPercentage = 0;
			$scope.upload = function (files) {	// vile copy ** $http POST
				if (files && files.length) {
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						$upload.upload({
							url: 'profesor/alta_express',
							fields: {id: 0 },
							file: file
						}).progress( function(evt) {
							$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
							console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.file.name);
						}).then( done => done.data ).then( function(data, status, headers, config) {
							console.log('file %s uploaded. Response: %O', config.file.name, data);
							if (data.error) {
								alert(data.error);
							} else {
								$scope.cv.cv = data.name;
								$scope.progressPercentage = 0;
							}
						});
					}
				}
			};
		}]
	})
})();
