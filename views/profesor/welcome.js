(function () {
	angular.module('mp')
	.component('welcome', {
		bindings: { request: '<' },
		templateUrl: 'views/profesor/welcome.html',
		controller: ['$http','$sce', function($http, $sce) {
			var $ctrl = this;
			
			$ctrl.$onInit = function() {				
				$http.get('profesor/welcome', { params:{ request: $ctrl.request }})
				.then( done => {
					// if (done.data.status=='ok')
					$ctrl.post = done.data.post;
					$ctrl.post.content = $sce.trustAsHtml($ctrl.post.content);
				})
			}
		}]
	})
})();
