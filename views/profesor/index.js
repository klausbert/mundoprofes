(function () {
	'use strict';
	angular.module('mp')
	.config(['$stateProvider', function ($sp) { $sp
		.state('profesor', { 
			url: '/profesor', 
			templateUrl: 'views/profesor/index.html', 
			controller: ['$scope','$http','$sce', function($scope, $http, $sce) {
				var $ctrl = this;
				
				$http.get('general/wp?name=sample-page').then(
					function(done) {
						$ctrl.item = done.data || {};
						$ctrl.item.content = $sce.trustAsHtml($ctrl.item.content);
					}
				);
			}],
			controllerAs: '$ctrl'
		})
		.state('profesor.alta-express', { 
			url: '/alta-express',
			template: '<alta-express response="$state.current.action($state, request)" />',
			action: function($state, request) {
				if (request) {
					$state.go('profesor.welcome', { request: request });
				} else {
					$state.go('profesor.avisos');
				}
			},
		})
		.state('profesor.welcome', { 
			url: '/welcome/{request}',
			template: '<welcome request="$stateParams.request" />',
		})
		.state('profesor.avisos',      { 
			url: '/avisos', 
			templateUrl: 'views/profesor/avisos_list.html', 
			data: { listUrl: 'profesor/avisos' },
			controller: 'AvisosListCtrl'
		})
		.state('profesor.aviso', { 
			url: '/aviso/:id', 
			templateUrl: 'views/profesor/avisos_form.html', 
			data: { listUrl: 'profesor/avisos' },
			controller: 'AvisosFormCtrl'
		})
	}])
})();
