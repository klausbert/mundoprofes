(() => {
	var app = angular.module('mp')
	
	app.path = { template: 'views/', endpoint: './' }
	
	app.component('typeaheadWrapper', {
		bindings: { 
			ngModel: '=', 
			ngRequired: '<', 
			ngFilter: '<',
			placeholder: '@', text: '@', uri: '@', 
			onUpdate: '&',
		},
		templateUrl: app.path.template +'general/typeahead-wrapper.html',
		controller: ['$http', function($http) {
			var $ctrl = this;
			
			$ctrl.$onInit = () => {
				$ctrl.value = '';
				$ctrl.placeholder = $ctrl.placeholder || '';
				$ctrl.ngRequired = $ctrl.ngRequired || true;
			}
			$ctrl.$onChanges = () => {
				if ($ctrl.ngModel)
				$http.get(app.path.endpoint + $ctrl.uri, { params:{ id: $ctrl.ngModel }}).then( 
					(done) => { $ctrl.value = done.data[0][$ctrl.text] },
					(fail) => {}
				)
			}
			
			$ctrl.typeahead = ($query) => {
				var params = angular.copy($ctrl.ngFilter || {});
				params.like = $ctrl.text;
				params[$ctrl.text] = $query;
				return $http.get(app.path.endpoint + $ctrl.uri, { params: params }).then( 
					(done) => { return done.data }
				);
			}
			
			$ctrl.save = (item) => {
				if (item.id) {
					$ctrl.ngModel = item.id;
					$ctrl.onUpdate({ item: item });
				}
			}
			
		}]
	})
})();
