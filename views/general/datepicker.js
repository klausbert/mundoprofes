(function () {
	angular.module('mp')
	.component('datepicker', {
		bindings: { model: '=' },
		templateUrl: 'views/general/datepicker.html',
		controller: [ function() {
			var self = this;
			
			this.options = {
				showWeeks: false,
				showButtonBar: false
			};
			
			this.opened = false;
			
			this.open = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				self.opened = true;
			};
		}]
	})
})();
