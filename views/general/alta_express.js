(function () {
	'use strict';
	angular.module('mp')
	.component('altaExPress', {
		bindings: { 
			response: '&' 
		},
		controller: ['$scope','$http','authService','Upload', function($scope, $http, authService, $upload) {
			
			$scope.cv = {};
			$http.get('general/form_express').then( done => done.data )	// sin permisos
			.then( response => {
				console.log('leyendo alta express - %s', new Date().toJSON());
				$scope.cv = response;
				if (response.RUT)
					$scope.cv.RUT = ('000000000'+ response.RUT).slice(-9);	// hotfix auto conversion
			});
			
			$scope.validaRut = function(rut) {
				
				$scope.form.rut.errorMsg = validaRut(rut);	// validaci�n interna
				
				if (rut===undefined || $scope.form.rut.errorMsg=='') {
					$scope.form.rut.$setValidity('unique', true);	// temp turn off 
					return true;
				}
				return false;
			};
			// $scope.maskOptions = { maskDefinitions: {'A': /[A-Za-z ������������]/} };
			
			$scope.email = {
				available: true,
				check: (email) => 
					$http.get(`profesor?email=${encodeURIComponent(email)}`).then( done => done.data )
					.then( data => {
						$scope.email.available =( data.id==undefined );
						$scope.form.Email.$setValidity('unique', $scope.email.available);
					})
			};
			$scope.rut = {
				available: true,
				check: (rut) => 
					$http.get(`profesor?rut=${rut}`).then( done => done.data )
					.then( data => ($scope.rut.available =( data.id==undefined )) )
			};
			
			$scope.status = '';
			$scope.displayErrorsBox = () => ($scope.status=='errors' && Object.keys($scope.form.$error).length);
			$scope.post = () => {
				if ($scope.cv.RUT!=undefined && $scope.cv.RUT=='')
					delete $scope.cv.RUT;
				
				$scope.status = ($scope.form.$invalid) ? 'errors' : 'saving';
				
				if ($scope.status=='saving')
				$http.post('general/form_express', $scope.cv).then( done => done.data )
				.then( data => {
					if ($scope.files.length && data.id)
						onUpload($scope.files, data.id);
					
					$http.post('/session', data).then( done => done.data )
					.then( data => {
						authService.loginConfirmed(data) 
						this.response({ request: data.request });
					})
				})
			};
			$scope.files = [];
			$scope.upload = (files) => {
				$scope.files = files || [];
			}
			$scope.progressPercentage = 0;
			//	solo si ya tiene id
			var onUpload = function (files, id) {
				files.forEach( file => {
					$upload.upload({
						url: 'general/form_express/cv',
						fields: {id: id },
						file: file
					}).progress( function(evt) {
						$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.file.name);
					}).then( done => done.data ).then( function(data, status, headers, config) {
						console.log('file %s uploaded. Response: %O', config.file.name, data);
						if (data.error) {
							alert(data.error);
						} else {
							$scope.cv.cv = data.name;
							$scope.progressPercentage = 0;
						}
					});
				})
			};
		}],
		templateUrl: 'views/general/alta_express.html',
	})
})();
