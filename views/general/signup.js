(function() { 
	angular.module('mp')
	.component('signupGeneral', {
		bindings: { 
			onUpdate: '&' 
		},
		controller: ['$state', function($state) {
			this.signupSuccessful = function(response) {
				console.log('Signed up as', response);
				console.log('Get rid of $state here... go to alta-express');
				$state.go('profesor.alta-express');
			}
		}],
		templateUrl: 'views/general/signup.html',
	})
})();
