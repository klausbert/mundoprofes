(app = angular.module('mp') => { app
	.component('forgot', {
		bindings: { onUpdate: '&' },		
		controller: ['$scope','$state','$http', function($scope, $state, $http) {
			$scope.data = { email: '', status: 'pending' };
			$scope.put = () => {
				$http.put('forgot', $scope.data).then( done => done.data )
				.then( response => {
					if (response.status) {
						$scope.data.status = response.status;
					}
				});
			}
		}],
		template: `
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h3 class="panel-title">Olvidó su contraseña?</h3>
					</div>

					<form class="form-horizontal panel-body" name="form" role="form" ng-submit="form.$valid && put()" novalidate>
					
						<p>Ingrese el email con el que está registrado y le enviaremos instrucciones para reiniciarla.</p>
						<p class="danger" ng-if="data.status=='not found'">Su dirección de email debe existir previamente en nuestra base de datos.</p>
						
						<div ng-if="data.status=='pending'">
							<label class="control-label">Email</label>
							<input class="form-control" type="email" ng-model="data.email" ng-pattern="/^[A-Za-z0-9._%+-]+@[A-Za-z]+\.[A-Za-z.]+$/" required>
							<br>
							<input class="form-control btn" ng-class="{'btn-success': form.$valid }" ng-disabled="form.$invalid" type="submit" value="Enviar" />
						</div>
						
						<div ng-if="data.status=='complete'">
							<p class="lead">Hemos enviado un email a {{ data.email }}</p>
						</div>
					</form>
				</div>
			</div>
		`,
}
