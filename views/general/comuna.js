(function () {
	angular.module('mp')
	.component('comuna', {
		bindings: { id: '<', onUpdate: '&' },
		templateUrl: '/views/general/comuna.html',
		controller: ['$http', function($http) {
			this.value = '';
			this.$onChanges = bindings => {
				if (bindings.id && bindings.id.currentValue)
				$http.get(`comunas?id=${bindings.id.currentValue}`).then( done => done.data )
				.then( data => (this.value = data[0].comuna) );
			}
			
			this.comunas = query => $http.get(`comunas?comuna=${query}`).then( done => done.data );
		}]
	})
})();
