(function () {
	angular.module('mp')
	.component('tagsInputWrapper', {
		bindings: { 
			class: '@', 
			addFromAutocompleteOnly: '@', minTags: '@', maxTags: '@', name: '@', placeholder: '@', text: '@', uri: '@', 
			model: '=', onUpdate: '&',
		},
		templateUrl: '/views/general/tags-input-wrapper.html',
		controller: ['$http', function($http) {
			var $ctrl = this;
			
			$ctrl.addFromAutocompleteOnly = $ctrl.addFromAutocompleteOnly || true;
			$ctrl.minTags = $ctrl.minTags || 0;
			$ctrl.maxTags = $ctrl.maxTags || 3;
			
			$ctrl.tagsInput = ($query) => {
				var params = {};
				params[$ctrl.text] = $query;
				return $http.get($ctrl.uri, { params: params }).then( 
					(done) => { return done.data }
				);
			}
			$ctrl.showInvalid = ($tag) => console.log('Invalid tag %O', $tag);
		}]
	})
})();
