(function () {
	'use strict';
	angular.module('mp')
	.directive('profesiones', function() { return {
		restrict: 'E',
		scope: { class: '@', model: '=' },
		templateUrl: '/views/general/profesiones.html',
		controller: ['$scope','$http', function($scope, $http) {
			
			$scope.profesiones = function($query) {
				return $http.get("general/profesiones", { params:{ profesion: $query }});
			}
		}]
	}})
})();
