(function() { 
	angular.module('mp')
	.factory('RESTFactory', ['$http', function($http) {	// admin/cvs, profesor/intereses
		var factory = {};
		var prefix = '';
		
		factory.get = function(uri, data) {
			uri = prefix + uri;
			return $http.get(uri, { params: data });
		};
		factory.post = function(uri, data) {
			uri = prefix + uri;
			return $http.post(uri, data);
		};
		factory.put = function(uri, data) {
			uri = prefix + uri;
			return $http.put(uri, data);
		};
		factory.delete = function(uri) {
			uri = prefix + uri;
			return $http.delete(uri);
		};
		return factory;
	}])
	
	.factory('ATEsFactory', ['$http', function($http) {	// profesor/intereses
		var factory = {};
		
		factory.find = function(tipo, param) {
			return $http.get('especificaciones_ATE', { params: { especificacion: param }});
		};
		factory.get = function(param) {
			return $http.get('especificaciones_ATE');
		};
		return factory;
	}])
	
	.factory('cargosFactory', ['$http', function($http) {	// profesor/experiencias...
		var factory = {};
		
		factory.find = function(tipo, param) {
			return $http.get('cargos', { params: { tipo: tipo, cargo: param }});
		};
		factory.get = function(param) {
			return $http.get('cargos', { params: { tipo: param }});
		};
		return factory;
	}])
	
	.factory('colegiosFactory', ['$http', function($http) {	// profesor/experiencia_colegios, formacion_escolar
		var factory = {};
		
		factory.find = function(param) {
			return $http.get('colegios', { params: { id_comuna: param }});
		};
		factory.get = function() {
			return $http.get('colegios');
		};
		return factory;
	}])
	
	.factory('comunasFactory', ['$http', function($http) {	// profesor/*
		var factory = {};
		
		factory.find = function(param) {
			return $http.get('comunas', { params: { comuna: param }});
		};
		factory.get = function() {
			return $http.get('comunas');
		};
		return factory;
	}])
	
	.factory('mesesFactory', [ function() {	// profesor/*
		var factory = {};
		
		factory.get = function() {
			return [
				{id: 1, mes: "Ene"}, {id: 02, mes: "Feb"}, {id: 03, mes: "Mar"}, {id: 04, mes: "Abr"}, 
				{id: 5, mes: "May"}, {id: 06, mes: "Jun"}, {id: 07, mes: "Jul"}, {id: 08, mes: "Ago"}, 
				{id: 9, mes: "Sep"}, {id: 10, mes: "Oct"}, {id: 11, mes: "Nov"}, {id: 12, mes: "Dic"}
			];
		};
		return factory;
	}])
	
	.factory('nacionalidadesFactory', ['$http', function($http) {	// profesor/datos_personales
		var factory = {};
		
		factory.get = function() {
			return $http.get('nacionalidades');
		};
		return factory;
	}])
	
	.factory('regionesFactory', ['$http', function($http) {	// profesor/intereses
		var factory = {};
		
		factory.find = function(region) {
			return $http.get('regiones', { params: { region: region }});
		};
		factory.get = function() {
			return $http.get('regiones');
		};
		return factory;
	}])
	
	.factory('profesorFactory', ['$http', function($http) {	// profesor/datos_personales
		var factory = {};
		
		factory.get = function() {
			return $http.get('profesor/datos_personales');
		};
		factory.put = function(data) {
			return $http.put('profesor/datos_personales', data);
		};
		return factory;
	}])
})();
