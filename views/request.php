<h1><?= $email; ?></h1>

<h2>Un pedido de cambio de contrase�a se inici� en tu nombre</h2>

<p>Para reemplazar tu contrase�a actual por una nueva, <a href="<?= base_url(), "#/request/$hash"; ?>">haz click en este enlace</a>, que te llevar� a una p�gina donde realizar el cambio.</p>

<p>Si quieres conservar tu contrase�a actual, simplemente ignora este email. Gracias!</p>
