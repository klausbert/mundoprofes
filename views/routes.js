(function () {
	'use strict';
	angular.module('mp')
	.config(['$locationProvider', function($locationProvider) {
		$locationProvider.hashPrefix('');	// http://stackoverflow.com/questions/41272314/angular-all-slashes-in-url-changed-to-2f
	}])
	.config(['$urlRouterProvider', function ($urlRouterProvider) {
		$urlRouterProvider.when('/form', '/inscribete');
		// For any unmatched url, redirect to /state1
		$urlRouterProvider.otherwise("/");
	}])
	.config(['$stateProvider', function ($stateProvider) {
		//
		// Now set up the states
		$stateProvider
		.state('v',          { abstract: true,		 templateUrl: "views/_index_public.html"   })
		.state('v.home',     { url: '/',             templateUrl: "views/home.portada.html"    })
		.state('v.propuesta',{ url: '/propuesta',    templateUrl: "views/home.propuesta.html"  })
		.state('v.servicios',{ url: '/servicios',    templateUrl: "views/home.servicios.html"  })
		// .state('v.avisos', ... )
		.state('v.signin',   { url: '/signin',       templateUrl: "views/signin.html" })	// H menu only
		.state('v.signup',   { url: '/signup',       template: '<signup-general />' })
		.state('v.forgot',   { url: '/forgot',       templateUrl: "views/forgot.html"          })
		.state('v.reset',    { url: '/reset/:reset', templateUrl: "views/reset.v2.html", controller: 'ResetCtrl' })
		.state('v.first',    { url: '/first/:reset', template: '<first hash="$stateParams.reset" />' })
		// .state('v.news',     { url: '/news/:postId', templateUrl: "views/news.post.html", controller: 'PostCtrl' })
		.state('x',          { abstract: true,		 template: '<div class="col-xs-12" ui-view />' })
		.state('x.form',     { url: '/inscribete',	 template: '<alta-ex-press response="$state.current.action($state, request)" />',
														action: function($state, request) {
															if (request) {
																$state.go('profesor.welcome', { request: request });
															} else {
																$state.go('profesor.avisos');
															}
														},
		})
	}])	
	.run(['$rootScope','$state','$stateParams', function ($root, $state, $stateParams) {
		$root.$state = $state;
		$root.$stateParams = $stateParams;
	}])
})();
