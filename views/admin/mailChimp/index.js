(function() {
	angular.module('mp')
	.component('mailChimp',  {
		templateUrl: 'views/admin/mailChimp/index.html',
		controller: ['$http', function($http) {
			
			var self = this;
			
			this.lists = [];
			$http.get('mailing/lists')
			.then( function(done) {
				self.lists = done.data;
			}, function(fail) {
				console.log(fail);
			});
			
			this.delete = function(listId, memberId) {
				$http.delete('mailing/lists/members/', { data: { listId: listId.substr(1), memberId: memberId }})
				.then( function(done) {
					console.log('Done: %O, status %s', done.data, done.status);
					return (done.status==204);
				}, function(fail) {
					console.log(fail);
				});
			}
		}]
	})
})();


/*
$config['mailChimp'] = [
	'apiKey' => 'ab132bc3a167ed65c2cf9448cb64bf35-us12',
	'listas' => ['V' => '5497eecc13', 'A' => '864efb21b9', 'R' => '332369e022'],
	'url'    => 'https://us12.api.mailchimp.com/3.0/'
];
*/
