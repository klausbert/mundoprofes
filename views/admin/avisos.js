(function () {
	'use strict';
	angular.module('mp')
	.component('adminAviso', {
		templateUrl: 'views/admin/avisos_form.html', 
		controller: ['$scope','$state','$http', function ($scope, $state, $http) {
			
			$scope.item = {};
			$scope.options = {};
			
			$http.get($state.current.data.listUrl, { params:{ id: $state.params.id }})
			.then( done => done.data ).then( function(response) {
				$scope.item = response.item;
				$scope.item.vigencia = new Date(response.item.vigencia +'T12:00');
				
				$scope.options = response.options;
				
				$http.get('regiones', { params:{ id: $scope.item.id_region }})
				.then( done => done.data ).then( function(response) {
					$scope.item.region = response[0].region;
				});
				$http.get('comunas' , { params:{ id: $scope.item.id_comuna }})
				.then( done => done.data ).then( function(response) {
					$scope.item.comuna = response[0].comuna;
				});
			});
			
			$scope.save = function(estado) {
				$scope.status = 'saving';
				$http.put($state.current.data.listUrl, { 
					id: $state.params.id, estado: estado, vigencia: $scope.item.vigencia.toJSON().slice(0, 10)
				})
				.then( done => done.data ).then( function(response) {
					$scope.item = response.item;
					$state.go('^.avisos');
				});
			}
		}]
	})
})();
