(function () {
	angular.module('mp')
	.component('avisosProfesoresSituacion', {
		bindings: { 
			id: '<', situacion: '<', 
			onUpdate: '&'
		},
		controller: ['$http', function($http) {
			this.grid = [];
			this.head = {};
			this.list = [];
			$http.get('admin/avisos_profesores', { params:{ id_aviso: this.id, situacion: this.situacion }})
			.then( done => done.data )
			.then( data => {
				this.grid = data.enum.situacion
				this.head = data.head 
				this.list = data.list 
			})
			this.change = (item) => {
				if (! item._original)
					item._original = item.situacion
				else if (item._original==item.situacion)
					delete item._original
			}
			this.update = () => {
				var updates = this.list.filter( f => f._original );
				var count = updates.length;
				
				updates.map( m => {
					$http.put('admin/avisos_profesores', { id: m.id, situacion: m.situacion })
					.then( done => done.data )
					.then( data => {
						if (--count==0)
							this.onUpdate()
					})
				})
			}
		}],
		templateUrl: 'views/admin/avisos-profesores-situacion.html',
	})
})();
