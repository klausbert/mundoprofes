(function () {
	angular.module('mp')
	.component('avisosProfesores', {
		bindings: { 
			onSelect: '&' 
		},
		controller: ['$http', function($http) {
			this.grid = [];
			this.list = [];
			$http.get('admin/avisos_profesores').then( done => done.data )
			.then( data => {
				this.grid = data.enum.situacion;
				this.list = data.list;
				
				this.list.map( a => {
					a.situaciones = {};
					this.grid.map( s => {
						a.situaciones[s] = 0
						a.postulantes.map( p => {
							a.situaciones[p.situacion] = p.cantidad
						})
					})
				});
			})
		}],
		templateUrl: 'views/admin/avisos-profesores.html',
	})
})();
