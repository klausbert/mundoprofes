(function() {
	angular.module('mp')
	.config(['$stateProvider', function ($sp) { $sp
		.state('admin.busquedas', { 
			url: '/busquedas', template: '<ui-view>Admin Busquedas</ui-view>', abstract: true 
		})
		.state('admin.busquedas.list', {
			url: '/list', 
			templateUrl: 'views/admin/busquedas_list.html', 
			controller: ['$scope', '$http', function($scope, $http) 
			{
				$scope.list = [];
				$http.get('admin/busquedas')
				.then( done => done.data ).then( function(response) {
					$scope.list = response.list;
				});
				
				$scope.form = function(id) {
					$scope.$state.go('admin.busquedas.form', { id: id });
				};
			}]
		})
		.state('admin.busquedas.form', { 
			url: '/form/:id', 
			data: { check: {}, results: [] },
			templateUrl: 'views/admin/busquedas_form.html', 
			controller: ['$scope', '$http', function($scope, $http) 
			{
				$scope.item = {};
				$scope.options = {};
				$scope.prospects = {};
				$scope.sets = {};	// found items
				$scope.titulos = [];
				$scope.cargos = [];
				$scope.asignaturas = [];
				$scope.niveles = [];
				
				$http.get('admin/busquedas', { params:{ id: $scope.$stateParams.id }})
				.then( done => done.data ).then( function(response) {
					$scope.item = response.item;
					$scope.options = response.options;
					$scope.prospects = response.prospects;
					$scope.sets = response.sets;
				})
				.error( function(response) {
					console.log(response);
				});
				$http.get('titulos').then( done => done.data ).then( function(response) {
					$scope.titulos = response;
				});
				$http.get('cargos', { params: { tipo: 'Colegio' }}).then( done => done.data ).then( function(response) {
					$scope.cargos = response;
				});
				$http.get('asignaturas').then( done => done.data ).then( function(response) {
					$scope.asignaturas = response;
				});
				$http.get('niveles').then( done => done.data ).then( function(response) {
					$scope.niveles = response;
				});
				
				$scope.saveStatus = function() {
					$http.put('admin/busquedas', { id: $scope.$stateParams.id, estado: $scope.item.estado }).then( 
						function(success) {
							console.log('Success ', success.data);
						},
						function(error) {
							console.log('Error ', error);
						}
					);
				};
				/*	buscador
				*/
				$scope.check   = $scope.$state.current.data.check   || {};	// checkboxes for intersect()
				$scope.results = $scope.$state.current.data.results || [];
				$scope.intersect = function()
				{
					var arrays = [];
					Object.keys($scope.check).map( function(m) { 
						if (typeof($scope.check[m])=='boolean' && $scope.check[m]) { 
							arrays.push($scope.sets[m]); 
						} else
						if (typeof($scope.check[m])=='object') { 
							Object.keys($scope.check[m]).map( function(k) { 
								if ($scope.check[m][k])
									arrays.push($scope.sets[m][k]);
							});
						}
					})
					// intersection
					$scope.results = arrays.pop().filter( function(f) { // uso el Ult array contra todos los demás
						return arrays.every( function(e) { 
							return e.some( function(s) { return s.id==f.id; }); 
						}); 
					});
					$scope.results.forEach( function(e) { 
						e.prospecto = $scope.prospects.some( function(s) { return s.id==e.id; }); 
					});
				};
				$scope.addToProspects = function() {
					var ids_prospectos = $scope.results.filter( function(f) { return f.check==true; }).map( function(m) { return m.id; }).join(',');
					$http.post('admin/busquedas', { id: $scope.$stateParams.id, ids_prospectos: ids_prospectos })
					.then( done => done.data ).then( function(response) {
						$scope.prospects = response.prospects;
					});
				}
				$scope.removeFromProspects = function() {
					var ids_prospectos = $scope.prospects.filter( function(f) { return f.check==true; }).map( function(m) { return m.id; }).join(',');
					$http.delete('admin/busquedas', { data:{ id: $scope.$stateParams.id, ids_prospectos: ids_prospectos }})
					.then( done => done.data ).then( function(response) {
						$scope.prospects = response.prospects;
					});
				}
				$scope.profesor = function(id) {
					$scope.$state.current.data.check = $scope.check;
					$scope.$state.current.data.results = $scope.results;
					$scope.$state.go('admin.busquedas.profesor', { id: id });
				}
			}]
		})
		.state('admin.busquedas.profesor', { 
			url: '/profesor/:id', 
			templateUrl: 'views/admin/profesor/profesor.html', 
			controller: ['$scope','$http', function($scope, $http) 
			{
				$scope.profesor = {};
				$http.get('admin/profesor', { params:{ id: $scope.$stateParams.id }}).then( done => done.data ).then( function(response) {
					console.log('Profesor %O', response);
					$scope.profesor = response;
				});
			}]
		})
	}])
})();
