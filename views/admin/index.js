(function () {
	'use strict';
	angular.module('mp')
	.config(['$stateProvider', function ($sp) { $sp
		.state('admin', { 
			url: '/admin', 
			templateUrl: 'views/_index_member.html',
			resolve: { ocLazyLoad: function($ocLazyLoad) {
				var modules = [
					'avisos', 'avisos-profesores', 'avisos-profesores-situacion', 
					'roles-list', 'roles-item',
					'cvs/cvs-list', 'cvs/cvs-find', 'cvs/cvs-alta', 'cvs/cvs-item', 'cvs/mch-post',
					'compras/compras_list', 'compras/compras_item',
					'mailChimp/index',
				];
				return $ocLazyLoad.load( modules.map( function(m) {
					return 'scripts/admin/'+ m +'.js';
				}));
			}},
		})
		.state('admin.roles', { 
			url: '/roles', 
			template: '<ui-view></ui-view><admin-roles on-update="$state.current.action($state, response)" />', 
			action: function($state, id_role) {
				$state.go('admin.roles.user', { id_role: id_role });
			}
		})
		.state('admin.roles.user', { 
			url: '/{id_role:int}', 
			controller: ['$uibModal', '$state', function($modal, $state) {
				$modal.open({
					animation: true,
					template: '<admin-role id="$stateParams.id_role" on-update="$state.current.action($state, response)" />',
				}).result.then(
					function(done) {
						console.log('Done %O', done);
						$state.go('admin.roles', {}, { reload: true })
					}, 
					function(fail) {
						console.log('Fail %O', fail);
						$state.go('admin.roles', {}, { reload: true })
					}
				);
			}],
			action: function($state, response) {
				console.log('Action: %s, %O', $state.current.name, response);
				$state.go('admin.roles', {}, { reload: true })
			}
		})
		//
		//	Compras
		//
		.state('admin.compras', { 
			url: '/compras', 
			template: '<admin-compras response="$state.go(\'^.compra\', { id: id })" />', 
		})
		.state('admin.compra', { 
			url: '/compras/{id:int}', 
			template: '<admin-compra id="$stateParams.id" response="$state.go(\'^.compras\')" />'
		})
		//
		//	Avisos
		//
		.state('admin.avisos',      { 
			url: '/avisos', 
			templateUrl: 'views/admin/avisos_list.html', 
			data: { listUrl: 'admin/avisos' },
			controller: 'AvisosListCtrl'	// debe ser el de Profesor...
		})
		.state('admin.aviso', { 
			url: '/avisos/:id', 
			template: '<admin-aviso />', 
			data: { listUrl: 'admin/avisos' },
		})
		.state('admin.postulantes', { 
			url: '/postulantes', 
			template: '<avisos-profesores on-select="$state.current.action($state, id_aviso, situacion)" />', 
			action: function($state, id_aviso, situacion) {
				$state.go('admin.situaciones', { id: id_aviso, situacion: situacion });
			},
		})
		.state('admin.situaciones', { 
			url: '/postulantes/{id:int}/:situacion', 
			template: `<avisos-profesores-situacion id="$stateParams.id" situacion="$stateParams.situacion" on-update="$state.go('admin.postulantes')" />`, 
		})
		//
		//	CVs
		//
		.state('admin.cvs', { 
			url: '/cvs',
			template: '<cvs-list response="$state.current.action($state, id)" ng-show="$state.current.name==\'admin.cvs\'"></cvs-list>'
					+ '<ui-view></ui-view>', 
			action: function($state, id) {
				if (id) $state.go('admin.cvs.edit', { id: id });
				else	$state.go('admin.cvs.alta');
			},
		})
		.state('admin.cvs.alta', { 
			url: '/alta', 
			template: '<cvs-alta response="$state.current.action($state, id)" />',
			action: function($state, id) {
				$state.go('admin.cvs.edit', { id: id });
			},
		})
		.state('admin.cvs.edit', { 
			url: '/{id:int}', 
			template: '<cvs-item id="$stateParams.id" />'
		})
		//	MailChimp
		.state('admin.mailChimp', { 
			url: '/mailChimp', 
			template: '<mail-chimp />',
		})
	}])
})();
