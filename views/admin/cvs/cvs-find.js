(function() {
	angular.module('mp')
	.component('cvsFind', {
		bindings: { 
			found: '=',
			response: '&'
		},
		templateUrl: 'views/admin/cvs/cvs-find.html',
		controller: ['$http', function($http) {
			var $ctrl = this;		
			
			$ctrl.isCollapsed = true;
			
			$ctrl.filter = what => {
				$http.get('admin/cvs/filter/'+ what, { headers:{ 'Range': 'cvs=0-20' }})	// 1st pass
				.then( done => {
					$ctrl.found = done.data;
					
					if (done.status==206)
					$http.get('admin/cvs/filter/'+ what, { headers:{ 'Range': 'cvs=21-1000' }})	// 2nd pass
					.then( done => { $ctrl.found = $ctrl.found.concat(done.data) });
				});
			}
			
			var resources = ['comunas', 'etiquetas', 'profesiones'   , 'experiencia']
			resources.forEach( e => { $ctrl[e] = { tags: [], list: [], check: [] }})
			$http.get('profesor/experiencia').then( done => {
				$ctrl.experiencia.opts = done.data.map( m => { return { id: m, value: m }; }) || [];
			})
			
			$ctrl.updated = resource => {
				var params
				if (resource=='experiencia')
					params = { ids: $ctrl[resource].tags }
				else
					params = { ids: $ctrl[resource].tags.map( m => { return m.id; }).join(',') }
				
				$http.get('admin/cvs/filter/'+ resource, { params: params }).then( 
					done => { $ctrl[resource].list = done.data }
				)
			}
			// on click, let's intersect all checked subsets
			$ctrl.intersect = () => {
				
				var checkedLists = [];
				resources.map( m => { 
					if (typeof($ctrl[m].check)=='boolean' && $ctrl[m].check) { 
						checkedLists.push($ctrl[m].list); 
					} else
					if (typeof($ctrl[m].check)=='object') { 
						Object.keys($ctrl[m].check).map( function(k) { 
							if ($ctrl[m][k].check)
								checkedLists.push($ctrl[m][k].list);
						});
					}
				})
				// intersection
				$ctrl.found = checkedLists.pop().filter( f => { // uso el Ult array contra todos los demás
					return checkedLists.every( e => { 
						return e.some( s => { return s.id==f.id; }); 
					});
				});
			}
			$ctrl.checked = function() {
				return $ctrl.found.filter( function(f) {
					return f.checked;
				});
			}
		}]
	})
})();
