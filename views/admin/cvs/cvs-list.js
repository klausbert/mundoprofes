(function() {
	angular.module('mp')
	.component('cvsList', {
		bindings: { response: '&' },
		templateUrl: 'views/admin/cvs/cvs-list.html',
		controller: ['$scope','$http', function($scope, $http) {
			
			$scope.cvs = [];
			$scope.$watch('cvs', function() {
				console.log($scope.cvs);
			})
			$scope.checkAll = false;
			$scope.checkAllNone = function() {
				$scope.cvs.forEach( function(e) {
					e.checked = $scope.checkAll;	// toggles on input
				});
			}
			$scope.checked = function() {
				return $scope.cvs.filter( function(f) {
					return f.checked;
				});
			}
		}]
	})
})();
