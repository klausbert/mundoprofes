(function() {
	angular.module('mp')
	.component('cvsItem', {
		bindings: { id: '<' },
		templateUrl: 'views/admin/cvs/cvs-item.html',
		controller: ['$scope','$http','Upload', function($scope, $http, $upload) {

			$scope.cv = {};
			$http.get('admin/cvs', { params:{ id: this.id }})
			.then( done => done.data ).then( function(response) {
				
				$scope.cv = response;
				$scope.cv.RUT = ('000000000'+ response.RUT).slice(-9);	// hotfix auto conversion
			});
			
			$http.get('profesor/experiencia').then( done => {
				$scope.anios_experiencia = done.data || [];
			});
			
			$scope.put = function(item) {
				if ($scope.cv.RUT!=undefined && $scope.cv.RUT=='undefined')	// !
					delete $scope.cv.RUT;
				
				$http.put('admin/cvs', item)
				.then( done => done.data ).then( function(response) {
					alert('CV actualizado: '+ item.email);
					$scope.$parent.$state.go('admin.cvs', null, { reload: true });	// **NO** es el lugar
				});
			};
			
			$scope.progressPercentage = 0;
			$scope.upload = function (files) {	// vile copy ** $http POST
				if (files==undefined || files.length==0)
					return;
				for (var i = 0; i < files.length; i++) {
					var file = files[i];
					$upload.upload({
						url: 'admin/cvs/upload',
						fields: {id: $scope.cv.id },
						file: file
					}).progress( function(evt) {
						$scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.file.name);
					}).then( done => done.data ).then( function(data, status, headers, config) {
						console.log('file %s uploaded. Response: %O', config.file.name, data);
						if (data.error) {
							alert(data.error);
						} else {
							$scope.cv.cv = data.name;
							$scope.progressPercentage = 0;
						}
					});
				}
			};
		}]
	})
})();
