(function() {
	angular.module('mp')
	.component('mchPost', {
		bindings: { members: '=' },
		templateUrl: 'views/admin/cvs/mch-post.html',
		controller: ['$http', function($http) {
			var self = this;
			
			this.list = '';
			this.lists = [];
			this.getLists = function() {				
				$http.get('mailing/lists').then( 
				function(done) {
					console.log(done);
					self.list = '';
					self.lists = [];
					if (done.data)
						self.lists = done.data.map( function(m) {
							return { id: m.id, name: m.name, value: m.name +' ['+ m.stats.member_count +']' };
						});
				}, 
				function(fail) { console.log(fail); });
			};
			this.getLists();
			
			this.count = 0;
			this.total = 0;
			this.send = function() {
				self.total = self.members.filter( function(f) {
					return f.checked;
				}).length;
				
				if (! self.total) return;
				
				self.count = 0;
				self.members.forEach( function(e) {
					if (e.checked) {
						$http.post('mailing/lists/members', {
							listId: self.list,
							email: e.email,
							fName: e.nombres,
							lName: e.apellidos
						}).then(
						function(done) {
							e.checked = false;
							delete e.mchResult;
							if (++self.count==self.total) {
								self.total = 0;
								self.getLists();
							}
						}, 
						function(fail) {
							e.mchResult = fail.data.title;
							if (++self.count==self.total) {
								self.total = 0;
								self.getLists();
							}
						});
					}
				});
			}
		}]
	})
})();
