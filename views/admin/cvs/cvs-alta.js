(function() {
	angular.module('mp')
	.component('cvsAlta', {
		bindings: { response: '&' },
		templateUrl: 'views/admin/cvs/cvs-alta.html',
		controller: ['$scope','$http', function($scope, $http) {
			var $ctrl = this;
			
			$scope.cv = { email: '', RUT: 0 };
			
			$scope.validaRut = function(rut) {
				
				$scope.form.rut.errorMsg = validaRut(rut);	// validación interna
				
				if (rut===undefined || $scope.form.rut.errorMsg=='') {
					$scope.form.rut.$setValidity('unique', true);	// temp turn off 
					return true;
				}
				return false;
			};
			
			$scope.save = function(item) {
				$http.post('/admin/cvs', item)
				.then( done => done.data ).then( function(response) {
					$scope.item = response;
					if (! response.error) {
						// $scope.$state.go('admin.cvs.edit', { id: $scope.item.id });
						$ctrl.response({ id: $scope.item.id });
					}
				});
				return true;
			}
		}]
	})
})();
