(function() {
	'use strict';
	angular.module('mp')
	.component('adminCompras', {
		bindings: { response: '&' },
		templateUrl: 'views/admin/compras/compras_list.html',
		controller: ['$scope','$http', function($scope, $http) {
			var $ctrl = this;
			
			$scope.list = [];
			$http.get('admin/compras')
			.then( done => done.data ).then( function(response) {
				if (response instanceof Array) $scope.list = response;
			});
		}]
	})
})();
