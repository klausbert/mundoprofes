(function() {
	'use strict';
	angular.module('mp')
	.component('adminCompra', {
		bindings: { 
			id: '=', 
			response: '&' 
		},
		templateUrl: 'views/admin/compras/compras_item.html',
		controller: ['$scope','$http', function($scope, $http) {
			$scope.item = {};
			this.$onChanges = () => {
				$http.get('admin/compras', { params:{ id: this.id }})
				.then( done => done.data ).then( data => {
					if (data instanceof Object) $scope.item = data;
				});
			}
			$scope.status = 'submit';
			
			$scope.save = (item) => {
				$scope.status = 'waiting';
				
				$http.put('admin/compras', item)
				.then( done => done.data ).then( data => {
					console.log('Vieja compra %O', data);
					$scope.status = 'created';
					this.response({ status: $scope.status });
				});
			}
		}]
	})
})();
