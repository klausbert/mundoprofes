(function () {
	angular.module('mp')
	.component('adminRole', {
		bindings: { id: '<', onUpdate: '&' }, 
		templateUrl: 'views/admin/roles-item.html',
		controller: ['$http','$scope', function($http, $scope) {
			var $ctrl = this;
			
			this.$onChanges = (obj) => {
				this.id &&
				// $http.get('admin/roles?id_role='+ this.id).then( done => done.data )
				$http.get('admin/roles',{ params:{ id_role: this.id }}).then( done => done.data )
				.then( data => {
					console.log('Datos del vendedor %s: %O', this.id, data);
					this.user = data;
					if (! this.user.colegios) this.user.colegios = [];
				});
			}
			$ctrl.list = [];
			$ctrl.colegios = function(id_comuna) {
				console.log('Trae colegios de la comuna %s', id_comuna);
				$http.get('colegios?id_comuna='+ id_comuna)
				.then( function(done) {
					$ctrl.list = done.data.filter( function(f) {
						return ! $ctrl.user.colegios.some( function(s) {
							return s.id==f.id;
						});
					});
				});
			}
			$ctrl.dependencia = '';
			$ctrl.dependencias = ['Municipal', 'Particular Subvencionado', 'Particular Pagado'];
			
			$ctrl.save = function(item) {
				$http.post('admin/roles', { id_role: $ctrl.user.id, id_colegio: item.id })
				.then( function(done) {
					if (done.status==201) {
						$ctrl.user.colegios.push(item);
						$ctrl.list = $ctrl.list.filter( function(f) { return f.id!==item.id; });
					}
				});
				console.log('agregar vendedor %O a usuarios_roles por colegio %s', $ctrl.user, item.id);
			}
			$ctrl.kill = function(item) {
				if (item.email) {
					if (! confirm('Quitar '+ item.role +' '+ item.apellidos)) return;
					
					$http.delete('admin/roles', { data:{ id: item.id }})
					.then( function(done) {
						if (done.status==200) {
							$ctrl.onUpdate({ response: done.data });
							$scope.$parent.$close(done.data);
						}
					});
				} else {
					if (! confirm('Quitar RBD '+ item.id)) return;
					
					$http.delete('admin/roles', { data:{ id_colegio: item.id, id_role: $ctrl.user.id }})
					.then( function(done) {
						if (done.status==200) {
							$ctrl.user.colegios = $ctrl.user.colegios.filter( function(f) { return f.id!==item.id; });
						}
					});
				}
			}
		}]
	})
})();
