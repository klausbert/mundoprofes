(function () {
	angular.module('mp')
	.component('adminRoles', {
		bindings: { onUpdate: '&' },
		templateUrl: 'views/admin/roles-list.html',
		controller: ['$http', function($http) {
			var $ctrl = this;
			
			$ctrl.list = [];
			$http.get('admin/roles').then( function(done) {
				$ctrl.list = done.data;
			});
			
			$ctrl.user = { id: 0, email: '', RUT: ''};
			
			$ctrl.select = function(item) {
				$ctrl.user = item;
			}
			$ctrl.open = function(item) {
				if (item.directo)
					$ctrl.onUpdate({ response: item.id });
			}
			$ctrl.save = function() {
				if ($ctrl.user.id && $ctrl.user.role)
				$http.post('admin/roles', $ctrl.user)
				.then( function(done) {
					if (done.status==201) {
						$ctrl.list.push(done.data);
						$ctrl.user = { id: 0, email: '', RUT: ''};
					}
				}, function(fail) {
					if (fail.status==400 && fail.data.error) {
						alert('ERROR: '+ fail.data.error);
					} else {
						console.log('failed with %O', fail);						
					}
				});
				console.log('agregar vendedor %O a usuarios_roles SIN colegio', $ctrl.user);
			}
		}]
	})
})();
