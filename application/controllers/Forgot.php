<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot extends REST_Controller_v3
{
	protected function index_get()
	{
		$this->status(405);
	}
	protected function index_post()
	{
		$this->status(405);
	}
	protected function index_put()
	{
		if ($this->put()) 
		{
			$status = 403;
			$data = $this->model->update($this->put());
			
			if ($data['status']=='mailing') {
			/*
			  **	Send email, requesting confirmation (passwords)
				*/
				$this->load->module('mailing');			// MX_Controller
				
				$data['view'] = 'mailing/forgot';
				$data['result'] = $this->mailing->post($data);
				$data['status'] = 'complete';
				
				$status = 200;
			}
			$this->response($data, $status);
		}
	}
	protected function index_delete()
	{
		$this->status(405);
	}
}
