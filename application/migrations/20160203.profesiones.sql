ALTER TABLE profesor_intereses
  CHANGE referencia referencia ENUM('regiones','comunas','asignaturas','asig_clases_part','niveles','cargos','especificaciones_ate','profesiones') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

  
  
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE profesiones (
  id smallint(5) UNSIGNED NOT NULL,
  profesion varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO profesiones (id, profesion) VALUES
(1, 'Profesor de Artes Visuales'),
(2, 'Profesor Educaci�n General B�sica'),
(3, 'Profesor de Lenguaje y Comunicaci�n'),
(4, 'Profesor de Matem�tica'),
(5, 'Profesor de Historia, Geograf�a y Ciencias Sociales'),
(6, 'Profesor de F�sica'),
(7, 'Profesor de Ciencias Naturales'),
(8, 'Profesor de Biolog�a'),
(9, 'Profesor de Qu�mica'),
(10, 'Profesor de Educaci�n F�sica y Salud'),
(11, 'Profesor de Religi�n'),
(12, 'Profesor de Filosof�a'),
(13, 'Profesor de M�sica'),
(14, 'Profesor de Idioma Extranjero'),
(15, 'Profesor de Idioma Extranjero Franc�s'),
(16, 'Profesor de Idioma Extranjero Ingl�s'),
(17, 'Profesor de Idioma Extranjero Alem�n'),
(18, 'Profesor Educaci�n Diferencial'),
(19, 'Estudiante en Pedagog�a'),
(20, 'Profesor de Orientaci�n'),
(21, 'Profesor de Tecnolog�a'),
(22, 'Profesor Administraci�n y Comercio'),
(23, 'Profesor Agropecuario'),
(24, 'Profesor Alimentaci�n'),
(25, 'Profesor Confecci�n'),
(26, 'Profesor Construcci�n'),
(27, 'Profesor Electricidad'),
(28, 'Profesor Gr�fico'),
(29, 'Profesor Maderero'),
(30, 'Profesor Maritimo'),
(31, 'Profesor Metalmecanico'),
(32, 'Profesor Minero'),
(33, 'Profesor Programas y Proyectos Sociales'),
(34, 'Profesor Qu�mica'),
(35, 'Profesor Turismo y Hoteler�a'),
(36, 'Educadora de Parvulos'),
(37, 'Educadora de Parvulos Biling�e'),
(38, 'Psicopedagoga'),
(39, 'T�cnico en P�rvulos'),
(40, 'Psicopedag�go'),
(41, 'Psic�logo'),
(42, 'Fonoaudi�logo'),
(43, 'Asistente Social'),
(44, 'Antrop�logo'),
(45, 'Soci�logo'),
(46, 'Kinesi�logo'),
(47, 'Bibliotec�logo'),
(48, 'T�cnico Inform�tico');


ALTER TABLE profesiones
  ADD PRIMARY KEY (id);


ALTER TABLE profesiones
  MODIFY id smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;