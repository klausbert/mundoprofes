INSERT INTO comunas (comuna)
SELECT DISTINCT T.comuna 
FROM `colegios_v2` T
LEFT JOIN comunas M ON T.comuna = M.comuna
WHERE M.comuna IS NULL
;
UPDATE colegios_v2 T
JOIN comunas M ON T.comuna = M.comuna
SET T.id_comuna = M.id
WHERE M.comuna = T.comuna
AND T.id_comuna IS NULL
;
