SELECT COUNT(*) FROM `profesor`                     WHERE profesor.id NOT IN (SELECT id FROM `usuarios`) UNION ALL
SELECT COUNT(*) FROM `profesor_intereses`           WHERE id_profesor NOT IN (SELECT id FROM `profesor`) UNION ALL
SELECT COUNT(*) FROM `profes_experiencia_colegios`  WHERE id_profesor NOT IN (SELECT id FROM `profesor`) UNION ALL
SELECT COUNT(*) FROM `profes_experiencia_otras`     WHERE id_profesor NOT IN (SELECT id FROM `profesor`) UNION ALL
SELECT COUNT(*) FROM `profes_formacion_escolar`     WHERE id_profesor NOT IN (SELECT id FROM `profesor`) UNION ALL
SELECT COUNT(*) FROM `profes_formacion_postgrado`   WHERE id_profesor NOT IN (SELECT id FROM `profesor`) UNION ALL
SELECT COUNT(*) FROM `profes_formacion_profesional` WHERE id_profesor NOT IN (SELECT id FROM `profesor`) ;


DELETE FROM `profesor`                     WHERE profesor.id NOT IN (SELECT id FROM `usuarios`) ;
DELETE FROM `profesor_intereses`           WHERE id_profesor NOT IN (SELECT id FROM `profesor`) ;
DELETE FROM `profes_experiencia_colegios`  WHERE id_profesor NOT IN (SELECT id FROM `profesor`) ;
DELETE FROM `profes_experiencia_otras`     WHERE id_profesor NOT IN (SELECT id FROM `profesor`) ;
DELETE FROM `profes_formacion_escolar`     WHERE id_profesor NOT IN (SELECT id FROM `profesor`) ;
DELETE FROM `profes_formacion_postgrado`   WHERE id_profesor NOT IN (SELECT id FROM `profesor`) ;
DELETE FROM `profes_formacion_profesional` WHERE id_profesor NOT IN (SELECT id FROM `profesor`) ;


ALTER TABLE `profesor`                     ADD FOREIGN KEY (`id`)          REFERENCES `usuarios`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `profesor_intereses`           ADD FOREIGN KEY (`id_profesor`) REFERENCES `profesor`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `profes_experiencia_colegios`  ADD FOREIGN KEY (`id_profesor`) REFERENCES `profesor`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `profes_experiencia_otras`     ADD FOREIGN KEY (`id_profesor`) REFERENCES `profesor`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `profes_formacion_escolar`     ADD FOREIGN KEY (`id_profesor`) REFERENCES `profesor`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `profes_formacion_postgrado`   ADD FOREIGN KEY (`id_profesor`) REFERENCES `profesor`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `profes_formacion_profesional` ADD FOREIGN KEY (`id_profesor`) REFERENCES `profesor`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

//

SELECT COUNT(*) FROM `usuarios_a_confirmar`    WHERE id         NOT IN (SELECT id FROM `usuarios`) UNION ALL
SELECT COUNT(*) FROM `usuarios_roles`          WHERE id_usuario NOT IN (SELECT id FROM `usuarios`) UNION ALL
SELECT COUNT(*) FROM `usuarios_roles_colegios` WHERE id_role    NOT IN (SELECT id FROM `usuarios_roles`) ;


DELETE FROM `usuarios_roles`          WHERE id_usuario NOT IN (SELECT id FROM `usuarios`)       ;
DELETE FROM `usuarios_roles_colegios` WHERE id_role    NOT IN (SELECT id FROM `usuarios_roles`) ;


ALTER TABLE `usuarios_a_confirmar`    ADD FOREIGN KEY (id)         REFERENCES `usuarios`      (id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `usuarios_roles`          ADD FOREIGN KEY (id_usuario) REFERENCES `usuarios`      (id) ON DELETE CASCADE ON UPDATE RESTRICT;
ALTER TABLE `usuarios_roles_colegios` ADD FOREIGN KEY (id_role)    REFERENCES `usuarios_roles`(id) ON DELETE CASCADE ON UPDATE RESTRICT;

