ALTER TABLE usuarios
  CHANGE tipo tipo ENUM('profesor','colegios','admin','padre','colegio') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'profesor'
;



ALTER TABLE usuarios
  ADD linkedin VARCHAR(32) NULL DEFAULT NULL AFTER facebook
;