ALTER TABLE profes_formacion_profesional 
  CHANGE id_tipo_institucion id_tipo_institucion INT(10) UNSIGNED NULL DEFAULT NULL
, CHANGE anno anno SMALLINT(5) UNSIGNED NULL DEFAULT NULL
, CHANGE situacion_final situacion_final ENUM('estudiando','titulado','egresado','sin finalizar') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
;
ALTER TABLE profes_formacion_profesional 
  ADD UNIQUE( id_profesor, id_titulo_profesional)
;
