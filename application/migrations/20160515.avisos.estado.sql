ALTER TABLE avisos 
  CHANGE estado  estado ENUM('disponible', 'preparado', 'verificado', 'rechazado', 'vencido') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'disponible'
;
