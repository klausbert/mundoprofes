ALTER TABLE compras 
  CHANGE estado estado ENUM('pendiente','verificado','cancelado') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'pendiente'
;
ALTER TABLE compras  
  ADD pago_fecha DATE NOT NULL  AFTER estado
, ADD pago_forma ENUM('transferencia','deposito','webpay') NOT NULL  AFTER pago_fecha
, ADD pago_id INT UNSIGNED NOT NULL  AFTER pago_forma
;

