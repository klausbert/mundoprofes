ALTER TABLE profesor 
  ADD updated_at TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP 
, ADD updated_by INT UNSIGNED NULL DEFAULT NULL 
;
/*	log
*/
ALTER TABLE profesor /* change PRIMARY and UNIQUE to INDEX */;
ALTER TABLE profesor DROP INDEX id_comuna;
ALTER TABLE profesor DROP INDEX id_nacion;
ALTER TABLE profesor DROP INDEX cv_experiencia;

/*	triggers
*/
INSERT INTO mundoprofes_log.profesor
SELECT * FROM profesor
WHERE id = NEW.id
;
