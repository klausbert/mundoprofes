--
-- Table structure for table 'regiones'
--

CREATE TABLE IF NOT EXISTS regiones (
id int(10) unsigned NOT NULL,
  region varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table 'regiones'
--

INSERT INTO regiones (id, region) VALUES
(1, 'De Arica y Parinacota'),
(2, 'De Tarapac�'),
(3, 'De Antofagasta'),
(4, 'De Atacama'),
(5, 'De Coquimbo'),
(6, 'De Valpara�so'),
(7, 'Del Libertador Bernardo Ohiggins'),
(8, 'Del Maule'),
(9, 'Del Bio B�o'),
(10, 'De la Araucan�a'),
(11, 'De Los R�os'),
(12, 'De Los Lagos'),
(13, 'De Ays�n Del General Carlos Iba�ez del Campo'),
(14, 'De Magallanes y Ant�rtica Chilena'),
(15, 'Metropolitana de Santiago'),
(16, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table regiones
--
ALTER TABLE regiones
 ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table regiones
--
ALTER TABLE regiones
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;