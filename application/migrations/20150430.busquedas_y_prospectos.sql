--
-- Table structure for table 'busquedas'
--

DROP TABLE busquedas;
CREATE TABLE IF NOT EXISTS busquedas (
id int(10) unsigned NOT NULL,
  id_colegio int(10) unsigned NOT NULL,
  busqueda varchar(99) NOT NULL,
  estado enum('solicitada','en proceso','cerrada','anulada') NOT NULL,
  fecha date NOT NULL,
  id_titulos int(10) unsigned NOT NULL COMMENT 'M�s de uno',
  metodologias text NOT NULL,
  certificaciones text NOT NULL,
  cv_experiencia enum('0 a 1','2 a 5','6 a 10','11 a 15','16 a 20','m�s de 20') DEFAULT NULL COMMENT 'a�os',
  tamanio enum('Grande','Mediano','Peque�o') DEFAULT NULL,
  dependencia enum('Municipal','Particular Pagado','Particular Subvencionado') NOT NULL,
  laico_religioso enum('Congregaci�n','Iglesia Diocesana','Laico') NOT NULL,
  tipo enum('Colegio','Escuela lenguaje','Jardin') NOT NULL,
  id_proyecto int(10) unsigned NOT NULL,
  id_tipo_experiencia int(10) unsigned NOT NULL,
  contrato enum('Reemplazo','Plazo Fijo','Indefinido') NOT NULL,
  sueldo_mensual_hasta int(10) unsigned DEFAULT NULL,
  id_funciones_a_cumplir int(10) unsigned NOT NULL COMMENT 'M�s de una',
  disponibilidad enum('inmediata','periodo','fecha') DEFAULT NULL,
  descripcion varchar(300) NOT NULL,
  horario varchar(99) NOT NULL,
  cantidad_horas int(10) unsigned NOT NULL,
  id_asignaturas int(10) unsigned NOT NULL COMMENT 'M�s de una',
  id_niveles int(10) unsigned NOT NULL COMMENT 'M�s de uno',
  otros_beneficios text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Indexes for table busquedas
--
ALTER TABLE busquedas
 ADD PRIMARY KEY (id), ADD KEY id_colegio (id_colegio), ADD KEY estado (estado), ADD KEY fecha (fecha);

--
-- AUTO_INCREMENT for table busquedas
--
ALTER TABLE busquedas
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

--
-- Dumping data for table 'busquedas'
--

INSERT INTO busquedas (id, id_colegio, busqueda, estado, fecha, id_titulos, metodologias, certificaciones, cv_experiencia, tamanio, dependencia, laico_religioso, tipo, id_proyecto, id_tipo_experiencia, contrato, sueldo_mensual_hasta, id_funciones_a_cumplir, disponibilidad, descripcion, horario, cantidad_horas, id_asignaturas, id_niveles, otros_beneficios) VALUES
(5, 1, 'Profesor Chiflado', 'solicitada', '2015-04-14', 0, 'Hipnosis', 'MCZPQ', '2 a 5', 'Grande', 'Particular Pagado', 'Iglesia Diocesana', 'Colegio', 0, 0, 'Reemplazo', 1234566, 13, 'inmediata', 'Deber� ense�ar a unos alumnos que son unos diablos!', 'Lunes a Viernes de 9 a 15', 0, 8, 6, 'Plan Dental!');

--
-- Table structure for table 'busquedas_prospectos'
--

CREATE TABLE IF NOT EXISTS busquedas_prospectos (
id int(10) unsigned NOT NULL,
  id_busqueda int(10) unsigned NOT NULL,
  id_profesor int(10) unsigned NOT NULL,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  estado enum('seleccionado','contratado','rechazado') NOT NULL DEFAULT 'seleccionado'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Indexes for table busquedas_prospectos
--
ALTER TABLE busquedas_prospectos
 ADD PRIMARY KEY (id), ADD KEY id_busqueda (id_busqueda,id_profesor);

--
-- AUTO_INCREMENT for table busquedas_prospectos
--
ALTER TABLE busquedas_prospectos
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
