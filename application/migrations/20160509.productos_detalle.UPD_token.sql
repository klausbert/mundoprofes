CREATE TEMPORARY TABLE my_tokens (
id INT PRIMARY KEY AUTO_INCREMENT,
token CHAR(7) NULL
);
INSERT INTO my_tokens (token) VALUES 
  ('0g3roej')
, ('q6zzftj')
, ('ch9ougz')
, ('1nxfvta')
, ('doxy6gd')
, ('1gjgheo')
, ('ff1ottl')
, ('1wjce54')
, ('0atoxh4')
;
#SELECT * FROM my_tokens
;
UPDATE productos_detalle D
JOIN my_tokens Y ON D.id = Y.id
SET D.token = Y.token
;
