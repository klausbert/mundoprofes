ALTER TABLE usuarios_roles 
  CHANGE role role ENUM('sostenedor','director','gestor','vendedor','supervisor') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'gestor';
