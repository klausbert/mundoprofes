ALTER TABLE avisos
  ADD id_compra INT UNSIGNED NULL DEFAULT NULL  AFTER id_colegio
, ADD INDEX  FK_compras (id_compra)
;
ALTER TABLE avisos
  CHANGE estado estado ENUM('disponible','preparado','verificado','rechazado') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'disponible'
;
