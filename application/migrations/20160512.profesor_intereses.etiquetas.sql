ALTER TABLE profesor_intereses 
  CHANGE categoria categoria ENUM('regiones','comunas','asignaturas','asig_clases_part','niveles','cargos','especificaciones_ate','profesiones','etiquetas') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
;
INSERT INTO profesor_intereses
SELECT NULL, id_profesor, id_perfil AS id_referencia, 'etiquetas' AS categoria 
FROM profesor_cv_perfiles
;
DROP TABLE profesor_cv_perfiles
;
