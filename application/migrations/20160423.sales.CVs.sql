CREATE TABLE bogus_cvs (
  RUT varchar(12)
, email varchar(30)
, nombres varchar(99) 
, apellidos varchar(99)
, comuna varchar(30)
, telefono_movil int
, profesion varchar(99)
, tiene_empleo enum('NO', 'SI')
, cv_experiencia varchar(10)
, tipo varchar(20)
, institucion varchar(50)
, titulo varchar(99)
, termino smallint
, situacion varchar(10)
);
INSERT INTO bogus_cvs VALUES  
  ('191633598', 'profesor01@mundoprofes.com', 'Muriel Alejandra', 'lazo rossel', 'Colina', '5916353774', 'Profesor de Educación Física y Salud ', 'NO', '0 a 1', 'Universidad', 'Universidad Academia de Humanismo Cristiano', 'Educación Diferencial', '1989', 'Titulado')
, ('203091087', 'profesor02@mundoprofes.com', 'jessica mabel', 'Silva Vera', 'Lampa', '4740631850', 'Profesor de Religión', 'NO', '2 a 5', 'Universidad', 'Universidad Adolfo Ibañez', 'Educación Parvularia', '1990', 'Titulado')
, ('105777396', 'profesor03@mundoprofes.com', 'Keila', 'QUINTANA CANESSA,', 'TilTil', '5750269636', 'Profesor de Filosofía', 'NO', '6 a 10', 'Universidad', 'Universidad Adventista de Chile', 'Pedagogía en Artes Visuales', '1991', 'Titulado')
, ('199215825', 'profesor04@mundoprofes.com', 'Martyna del Pilar', 'Jessica Gemma', 'Santiago', '7384423704', 'Profesor de Lenguaje y Comunicación', 'NO', '7 a 10', 'Universidad', 'Universidad Adventista de Chile', 'Pedagogía en Artes Visuales', '1993', 'Titulado')
, ('222298148', 'profesor05@mundoprofes.com', 'Camila Ibis', 'Serrano Sandoval', 'Puente Alto', '6032215912', 'Profesor de Artes Visuales ', 'NO', '16 a 20', 'Universidad', 'Universidad Arturo Prat', 'Pedagogía en Filosofía', '1993', 'Egresado')
, ('071487911', 'profesor06@mundoprofes.com', ' FRANCISCO JAVIER', 'Palma Espinoza', 'San José de Maipo', '2086916479', 'Profesor Educación General Básica', 'NO', 'más de 20', 'Universidad', 'Universidad Austral de Chile', 'Pedagogía en Historia y Ciencias Sociales', '1994', 'Egresado')
, ('138242560', 'profesor07@mundoprofes.com', 'Diego Esteban', 'marin gomez', 'Buin', '6266306063', 'Profesor de Lenguaje y Comunicación', 'NO', '0 a 1', 'Universidad', 'Universidad Autónoma de Chile', 'Pedagogía en Inglés', '1995', 'Egresado')
, ('123239563', 'profesor08@mundoprofes.com', 'Martyna del Pilar', 'Rojas Amado', 'Calera de Tango', '7384423704', 'Profesor de Matemática', 'NO', '2 a 5', 'Universidad', 'Universidad Autónoma del Sur', 'Pedagogía en Lengua Castellana y Comunicación', '1996', 'Egresado')
, ('059175041', 'profesor09@mundoprofes.com', 'ana eugenia', 'Ulloa Muñoz', 'Paine', '8638343685', 'Profesor de Historia, Geografía y Ciencias Sociales', 'NO', '0 a 1', 'Universidad', 'Universidad Bernardo O´Higgins', 'Pedagogía en Matemáticas', '1997', 'Egresado')
, ('174142270', 'profesor10@mundoprofes.com', 'Constanza Pilar', 'Molina Cifuentes', 'San Bernardo', '4183130817', 'Profesor de Física', 'NO', '2 a 5', 'Universidad', 'Universidad Bolivariana', 'Pedagogía en Música', '1998', 'Egresado')
, ('084390976', 'profesor11@mundoprofes.com', 'Katherine del Carmen', 'Olea Yáñez', 'Alhué', '7097194610', 'Profesor de Ciencias Naturales ', 'NO', '0 a 1', 'Universidad', 'Universidad Católica Cardenal Raúl Silva Henríquez', 'Pedagogía en Historia y Ciencias Sociales', '1993', 'Egresado')
, ('219359594', 'profesor12@mundoprofes.com', 'Dora Elizabeth', 'Arancibia Cartagena', 'Curacaví', '7926531868', 'Profesor de Biología', 'NO', '2 a 5', 'Universidad', 'Universidad Católica de la Santísima Concepción', 'Pedagogía en Inglés', '1994', 'Egresado')
, ('229689290', 'profesor13@mundoprofes.com', 'Christian', 'Saavedra Bazaes', 'María Pinto', '8895419807', 'Profesor de Química', 'NO', '0 a 1', 'Universidad', 'Universidad Católica de Temuco', 'Pedagogía en Lengua Castellana y Comunicación', '1995', 'Egresado')
, ('192995698', 'profesor14@mundoprofes.com', 'Carolina Andrea', 'Retamal Miranda', 'Melipilla', '6129582800', 'Profesor de Educación Física y Salud ', 'NO', '2 a 5', 'Universidad', 'Universidad Católica del Maule', 'Pedagogía en Matemáticas', '1996', 'Egresado')
, ('055158177', 'profesor15@mundoprofes.com', 'Carlos Aquiles', 'vera martinez', 'San Pedro', '6923045464', 'Profesor de Religión', 'NO', '0 a 1', 'Universidad', 'Universidad Católica del Norte', 'Pedagogía en Historia y Ciencias Sociales', '1997', 'Egresado')
, ('096544332', 'profesor16@mundoprofes.com', 'Victoria Isabel', 'Olhaberry Mendéz', 'Cerrillos', '7218480541', 'Profesor de Filosofía', 'NO', '2 a 5', 'Universidad', 'Universidad Central de Chile', 'Pedagogía en Inglés', '1998', 'Egresado')
, ('187053463', 'profesor17@mundoprofes.com', 'Claudia Scarlette', 'Cuello LiraBarbara', 'Cerro Navia', '4619854736', 'Profesor de Música', 'NO', '0 a 1', 'Universidad', 'Universidad de Aconcagua', 'Pedagogía en Lengua Castellana y Comunicación', '1993', 'Egresado')
, ('065096773', 'profesor18@mundoprofes.com', 'Aníbal Matías', 'Manquian soto', 'Conchalí', '2953559066', 'Profesor de Artes Visuales ', 'NO', '2 a 5', 'Universidad', 'Universidad de Antofagasta', 'Pedagogía en Matemáticas', '1994', 'Egresado')
, ('202442412', 'profesor19@mundoprofes.com', 'María José', 'Arellano Durán', 'El Bosque', '8911524272', 'Profesor Educación General Básica', 'NO', '6 a 10', 'Universidad', 'Universidad de Arte y Ciencias Sociales Arcis', 'Pedagogía en Biología y Ciencias Naturales', '1995', 'Egresado')
, ('100669846', 'profesor20@mundoprofes.com', 'Nicole Constanza', 'Silva Herrada', 'Estación Central', '4357771035', 'Profesor de Lenguaje y Comunicación', 'NO', '11 a15', 'Universidad', 'Universidad de Artes, Ciencias y Comunicación - UNIACC', 'Pedagogía en Biología y Ciencias Naturales', '1996', 'Egresado')
, ('214972239', 'profesor21@mundoprofes.com', 'Guillermo', 'Hernández Bravo', 'Huechuraba', '7667936208', 'Profesor de Matemática', 'NO', '16 a 20', 'Universidad', 'Universidad de Atacama', 'Pedagogía en Biología y Ciencias Naturales', '1997', 'Titulado')
, ('184493292', 'profesor22@mundoprofes.com', 'Claudia Andrea Valentina', 'Miranda Briceño', 'Independencia', '8195637951', 'Profesor de Historia, Geografía y Ciencias Sociales', 'NO', '6 a 10', 'Universidad', 'Universidad de Chile', 'Pedagogía en Biología y Ciencias Naturales', '1998', 'Titulado')
, ('185431061', 'profesor23@mundoprofes.com', 'Jaime Antonio', 'Avalos Díaz', 'La Cisterna', '8824887397', 'Profesor de Física', 'NO', '11 a15', 'Universidad', 'Universidad de Ciencias de la Informática', 'Pedagogía en Biología y Ciencias Naturales', '1989', 'Egresado')
, ('146921744', 'profesor24@mundoprofes.com', 'Aníbal Matías', 'Trujillo Márquez', 'La Granja', '1489362115', 'Profesor de Ciencias Naturales ', 'NO', '16 a 20', 'Universidad', 'Universidad de Concepción', 'Pedagogía en Biología y Ciencias Naturales', '1989', 'Egresado')
, ('152781822', 'profesor25@mundoprofes.com', 'Carla Stephanie', 'Peñaloza Arce', 'La Florida', '2268272749', 'Profesor de Biología', 'NO', '6 a 10', 'Universidad', 'Universidad de La Frontera', 'Pedagogía en Biología y Ciencias Naturales', '1989', 'Egresado')
, ('151393683', 'profesor26@mundoprofes.com', 'Aillen Ester', 'Ossandon Monsalves', 'La Pintana', '1451134479', 'Profesor de Química', 'NO', '11 a15', 'Universidad', 'Universidad de La Serena', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('209446928', 'profesor27@mundoprofes.com', 'María José', 'salazar cabrera', 'La Reina', '4060916166', 'Profesor de Educación Física y Salud ', 'NO', '16 a 20', 'Universidad', 'Universidad de Las Américas', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Titulado')
, ('107315411', 'profesor28@mundoprofes.com', 'Katherine', 'Rios Moran', 'Las Condes', '6889465309', 'Profesor de Religión', 'NO', 'más de 20', 'Universidad', 'Universidad de Los Andes', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Titulado')
, ('070822660', 'profesor29@mundoprofes.com', 'Carla Andrea', 'Lopez Santibañez ', 'Lo Barnechea', '8267150980', 'Profesor de Filosofía', 'NO', 'más de 20', 'Universidad', 'Universidad de Los Lagos', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('118352963', 'profesor30@mundoprofes.com', 'DANIELA VICTORIA', 'Abarca Ramírez', 'Lo Espejo', '5297248306', 'Profesor de Música', 'NO', 'más de 20', 'Universidad', 'Universidad de los Leones', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('20835888k', 'profesor31@mundoprofes.com', 'Margarira Yessenia', 'Astudillo Lavanderos', 'Lo Prado', '5989967174', 'Profesor de Artes Visuales ', 'NO', 'más de 20', 'Universidad', 'Universidad de Magallanes', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('223627420', 'profesor32@mundoprofes.com', 'Nicole Constanza', 'Castro Castro', 'Macul', '8887055281', 'Profesor Educación General Básica', 'NO', 'más de 20', 'Universidad', 'Universidad Academia de Humanismo Cristiano', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('062033142', 'profesor33@mundoprofes.com', 'David Alberto', 'GARCIA ORELLANA', 'Maipú', '7501851170', 'Profesor de Lenguaje y Comunicación', 'SI', 'más de 20', 'Universidad', 'Universidad Adolfo Ibañez', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Titulado')
, ('147979509', 'profesor34@mundoprofes.com', 'Monica Jacqueline', 'Rojas Rojo', 'Ñuñoa', '3492647199', 'Profesor de Matemática', 'SI', 'más de 20', 'Universidad', 'Universidad Adventista de Chile', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Titulado')
, ('076335494', 'profesor35@mundoprofes.com', 'Francisca Belén', 'Godoy Soto', 'Pedro Aguirre Cerda', '9148585352', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', 'más de 20', 'Universidad', 'Universidad Alberto Hurtado', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('223350321', 'profesor36@mundoprofes.com', 'Alejandra', 'Araneda Pereira', 'Peñalolén', '3195167409', 'Profesor de Física', 'SI', 'más de 20', 'Universidad', 'Universidad Arturo Prat', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('093961323', 'profesor37@mundoprofes.com', 'Danny Javier', 'Vargas Ponce de león', 'Providencia', '2708570037', 'Profesor de Ciencias Naturales ', 'SI', 'más de 20', 'Universidad', 'Universidad Austral de Chile', 'Pedagogía en Lengua Castellana y Comunicación', '1989', 'Egresado')
, ('22289364k', 'profesor38@mundoprofes.com', 'Natalia Andrea', 'Sepúlveda Momberg ', 'Pudahuel', '2411711036', 'Profesor de Biología', 'SI', 'más de 20', 'Universidad', 'Universidad Autónoma de Chile', 'Pedagogía en Matemáticas', '1989', 'Egresado')
, ('246480613', 'profesor39@mundoprofes.com', 'Natalia Andrea', 'Cuevas Figueroa', 'Quilicura', '6997527210', 'Profesor de Química', 'SI', '2 a 5', 'Universidad', 'Universidad Autónoma del Sur', 'Pedagogía en Matemáticas', '1989', 'Titulado')
, ('206123079', 'profesor40@mundoprofes.com', 'Monica Jacqueline', 'Herrera Cruz', 'Quinta Normal', '6260500313', 'Profesor de Educación Física y Salud ', 'SI', '0 a 1', 'Universidad', 'Universidad Bernardo O´Higgins', 'Pedagogía en Matemáticas', '1989', 'Titulado')
, ('222116287', 'profesor41@mundoprofes.com', 'Danny Javier', 'Orellana Moyano', 'Recoleta', '6682693205', 'Profesor de Religión', 'SI', '2 a 5', 'Universidad', 'Universidad Bolivariana', 'Pedagogía en Matemáticas', '1989', 'Egresado')
, ('073992524', 'profesor42@mundoprofes.com', 'Sebastian Alejandro', 'Jara Espinoza', 'Renca', '6045267498', 'Profesor de Filosofía', 'SI', '0 a 1', 'Universidad', 'Universidad Católica Cardenal Raúl Silva Henríquez', 'Pedagogía en Matemáticas', '2002', 'Egresado')
, ('069367933', 'profesor43@mundoprofes.com', 'Francisca Belén', 'Godoy Escobar ', 'San Miguel', '4417231830', 'Profesor de Música', 'SI', '2 a 5', 'Universidad', 'Universidad Católica de la Santísima Concepción', 'Pedagogía en Matemáticas', '2007', 'Egresado')
, ('129607734', 'profesor44@mundoprofes.com', 'Ana Carina', 'Vega PalmaBilly', 'San Joaquín', '4767524353', 'Profesor de Artes Visuales ', 'SI', '6 a 10', 'Universidad', 'Universidad Católica de Temuco', 'Pedagogía en Matemáticas', '2002', 'Egresado')
, ('153117675', 'profesor45@mundoprofes.com', 'Alejandra', 'Abarzúa Carvacho', 'San Ramón', '3397137343', 'Profesor Educación General Básica', 'SI', '11 a15', 'Universidad', 'Universidad Católica del Maule', 'Pedagogía en Matemáticas', '2007', 'Titulado')
, ('19897577k', 'profesor46@mundoprofes.com', 'Nicolás Jaumett', 'Droguett Droguett', 'Santiago', '5328263481', 'Profesor de Lenguaje y Comunicación', 'SI', '16 a 20', 'Universidad', 'Universidad Católica del Norte', 'Pedagogía en Matemáticas', '2002', 'Titulado')
, ('126697090', 'profesor47@mundoprofes.com', 'Sofía Paz Lourdes Andrea', 'Marín Barrera', 'Vitacura', '1804985064', 'Profesor de Matemática', 'SI', '6 a 10', 'Universidad', 'Universidad Central de Chile', 'Pedagogía en Matemáticas', '2007', 'Egresado')
, ('110216114', 'profesor48@mundoprofes.com', 'Yennifer Tamara', 'Urrutia Gonzalez', 'El Monte', '5406140821', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '11 a15', 'Universidad', 'Universidad de Aconcagua', 'Pedagogía en Matemáticas', '2002', 'Egresado')
, ('200838351', 'profesor49@mundoprofes.com', 'Rocio Belen', 'Morales Alvarez', 'Isla de Maipo', '2173329649', 'Profesor de Física', 'SI', '2 a 5', 'Universidad', 'Universidad de Antofagasta', 'Pedagogía en Inglés', '2007', 'Egresado')
, ('240106876', 'profesor50@mundoprofes.com', 'Nelson Ulises', 'Vescovi Caceres', 'Padre Hurtado', '4576447744', 'Profesor de Ciencias Naturales ', 'SI', '0 a 1', 'Universidad', 'Universidad de Arte y Ciencias Sociales Arcis', 'Pedagogía en Inglés', '2002', 'Egresado')
, ('117137317', 'profesor51@mundoprofes.com', 'Sheilah Paulina', 'Silva Fuentes', 'Peñaflor', '3839515802', 'Profesor de Biología', 'SI', '2 a 5', 'Universidad', 'Universidad de Artes, Ciencias y Comunicación - UNIACC', 'Pedagogía en Inglés', '2007', 'Titulado')
, ('166911176', 'profesor52@mundoprofes.com', 'Lissette', 'Riderelli Mackenzie', 'Talagante', '5022768580', 'Profesor de Química', 'SI', '0 a 1', 'Universidad', 'Universidad de Atacama', 'Pedagogía en Inglés', '2002', 'Titulado')
, ('105380380', 'profesor53@mundoprofes.com', 'karinna Edirh', 'Claudia Barrios ', 'La Cisterna', '9020462659', 'Profesor de Lenguaje y Comunicación', 'SI', '2 a 5', 'Universidad', 'Universidad de Chile', 'Pedagogía en Inglés', '2007', 'Egresado')
, ('203419236', 'profesor54@mundoprofes.com', 'Paula Carolina', 'Quiñones Leiva', 'La Granja', '1907752836', 'Profesor de Matemática', 'SI', '6 a 10', 'Universidad', 'Universidad de Ciencias de la Informática', 'Pedagogía en Inglés', '2002', 'Titulado')
, ('112372776', 'profesor55@mundoprofes.com', 'Renato Antonio', 'Acuña Chacón', 'La Florida', '7447779772', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '11 a15', 'Universidad', 'Universidad de Concepción', 'Pedagogía en Inglés', '2007', 'Titulado')
, ('150176360', 'profesor56@mundoprofes.com', 'Ninoska Alejandra', 'Vega Acevedo', 'La Pintana', '8624136996', 'Profesor de Lenguaje y Comunicación', 'SI', '16 a 20', 'Universidad', 'Universidad de La Frontera', 'Pedagogía en Inglés', '2008', 'Titulado')
, ('129881283', 'profesor57@mundoprofes.com', 'Felipe Marcelo', 'Gómez Castillo', 'La Reina', '8381763160', 'Profesor de Matemática', 'SI', '6 a 10', 'Universidad', 'Universidad de La Serena', 'Pedagogía en Inglés', '2009', 'Titulado')
, ('088855515', 'profesor58@mundoprofes.com', 'Angela Andrea', 'Yáñez Farías', 'Las Condes', '7526379385', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '11 a15', 'Universidad', 'Universidad de Las Américas', 'Pedagogía en Inglés', '2010', 'Titulado')
, ('167773230', 'profesor59@mundoprofes.com', 'Paula Carolina', 'Castelblanco ', 'Lo Barnechea', '2490639616', 'Profesor de Lenguaje y Comunicación', 'SI', '0 a 1', 'Universidad', 'Universidad de Los Andes', 'Pedagogía en Inglés', '2011', 'Titulado')
, ('189018274', 'profesor60@mundoprofes.com', 'Felipe Marcelo', 'Ortiz Peña', 'Lo Espejo', '1914286992', 'Profesor de Matemática', 'SI', '2 a 5', 'Universidad', 'Universidad de Los Lagos', 'Pedagogía en Inglés', '2012', 'Titulado')
, ('159168395', 'profesor61@mundoprofes.com', 'Boris Alexis', 'Soto Sepúlveda', 'Lo Prado', '3932547085', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '0 a 1', 'Universidad', 'Universidad de los Leones', 'Pedagogía en Biología y Ciencias Naturales', '2008', 'Titulado')
, ('208197312', 'profesor62@mundoprofes.com', 'Renato Antonio', 'Puentes Olivares', 'La Cisterna', '3680641548', 'Profesor de Lenguaje y Comunicación', 'SI', '2 a 5', 'Universidad', 'Universidad de Magallanes', 'Pedagogía en Filosofía', '2009', 'Titulado')
, ('091811189', 'profesor63@mundoprofes.com', 'Juan Francisco', 'Araya Torres', 'La Granja', '2071971102', 'Profesor de Matemática', 'SI', '0 a 1', 'Universidad', 'Universidad de Atacama', 'Pedagogía en Filosofía', '2010', 'Titulado')
, ('244401635', 'profesor64@mundoprofes.com', 'María Piedad', 'Vergara Allendes', 'La Florida', '2700588206', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '2 a 5', 'Universidad', 'Universidad de Chile', 'Pedagogía en Filosofía', '2011', 'Titulado')
, ('205671145', 'profesor65@mundoprofes.com', 'Claudio Osmán', 'Gutiérrez Tanabe', 'La Pintana', '7942904045', 'Profesor de Biología', 'SI', '0 a 1', 'Universidad', 'Universidad de Ciencias de la Informática', 'Pedagogía en Filosofía', '2012', 'Titulado')
, ('159451755', 'profesor66@mundoprofes.com', 'Maria', 'valdivia castro', 'La Reina', '7133734780', 'Profesor de Biología', 'SI', '2 a 5', 'Universidad', 'Universidad de Concepción', 'Pedagogía en Filosofía', '2008', 'Titulado')
, ('144829158', 'profesor67@mundoprofes.com', 'karla beatriz', 'Rubio Díaz,', 'Las Condes', '9144579289', 'Profesor de Biología', 'SI', '0 a 1', 'Universidad', 'Universidad de La Frontera', 'Pedagogía en Filosofía', '2009', 'Titulado')
, ('175968296', 'profesor68@mundoprofes.com', 'Macarena Alejandra', 'Bermudes Andrade', 'Lo Barnechea', '5726246607', 'Profesor de Biología', 'SI', '2 a 5', 'Universidad', 'Universidad de Atacama', 'Pedagogía en Música', '2010', 'Titulado')
, ('096719329', 'profesor69@mundoprofes.com', 'Sebastián Andrés', 'Vilches Caballero,', 'Lo Espejo', '6119255340', 'Profesor de Biología', 'SI', '0 a 1', 'Universidad', 'Universidad de Chile', 'Pedagogía en Música', '2011', 'Titulado')
, ('163241730', 'profesor70@mundoprofes.com', 'Guillermo', 'Aburto Gajardo', 'Lo Prado', '1045251303', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '2 a 5', 'Universidad', 'Universidad de Ciencias de la Informática', 'Pedagogía en Música', '2012', 'Titulado')
, ('168562861', 'profesor71@mundoprofes.com', 'esteban gabriel', 'Rondon Bravo', 'La Cisterna', '7052532170', 'Profesor de Lenguaje y Comunicación', 'SI', '11 a15', 'Universidad', 'Universidad de Concepción', 'Pedagogía en Música', '2008', 'Titulado')
, ('12205702k', 'profesor72@mundoprofes.com', 'Angela Andrea', 'Fernández del Río', 'La Granja', '3248666551', 'Profesor de Matemática', 'SI', '16 a 20', 'Universidad', 'Universidad de La Frontera', 'Educación Diferencial', '2009', 'Titulado')
, ('179770202', 'profesor73@mundoprofes.com', 'Carolina de los angeles', 'Silva Barros', 'La Florida', '4925422249', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', 'más de 20', 'Universidad', 'Universidad de Atacama', 'Educación Parvularia', '2010', 'Titulado')
, ('225811970', 'profesor74@mundoprofes.com', 'Patricio Humberto', 'Gonzaléz Gonzaléz', 'La Pintana', '1290562609', 'Profesor de Lenguaje y Comunicación', 'SI', '11 a15', 'Universidad', 'Universidad de Chile', 'Pedagogía en Artes Visuales', '2011', 'Titulado')
, ('195077290', 'profesor75@mundoprofes.com', 'Angelica Maria', 'delgadillo marchant', 'La Reina', '9310977907', 'Profesor de Matemática', 'SI', '16 a 20', 'Universidad', 'Universidad de Ciencias de la Informática', 'Educación Diferencial', '2012', 'Titulado')
, ('135730319', 'profesor76@mundoprofes.com', 'María Fernanda', 'Contreras Soto', 'Las Condes', '8780122520', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', 'más de 20', 'Universidad', 'Universidad de Concepción', 'Educación Parvularia', '2008', 'Titulado')
, ('109522171', 'profesor77@mundoprofes.com', ' Pedro Pablo', 'Castillo Gallardo', 'Lo Barnechea', '3352092824', 'Profesor de Lenguaje y Comunicación', 'SI', '11 a15', 'Universidad', 'Universidad de La Frontera', 'Pedagogía en Artes Visuales', '2009', 'Titulado')
, ('061816046', 'profesor78@mundoprofes.com', 'esteban gabriel', 'Flores Cornejo', 'Lo Espejo', '5846101836', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '16 a 20', 'Universidad', 'Universidad de Atacama', 'Educación Diferencial', '2010', 'Titulado')
, ('11194301k', 'profesor79@mundoprofes.com', 'Alejandro Alexis', 'Maturana Naranjo', 'Lo Prado', '4932740661', 'Profesor de Lenguaje y Comunicación', 'SI', 'más de 20', 'Universidad', 'Universidad de Chile', 'Educación Parvularia', '2011', 'Titulado')
, ('244365973', 'profesor80@mundoprofes.com', 'Angelica Maria', 'Gomez Orellana', 'La Cisterna', '5474389401', 'Profesor de Matemática', 'SI', '0 a 1', 'Universidad', 'Universidad de Ciencias de la Informática', 'Pedagogía en Artes Visuales', '2012', 'Titulado')
, ('176466685', 'profesor81@mundoprofes.com', 'Daniel Ignacio', 'Restrepo Vergara', 'La Granja', '5260290158', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '2 a 5', 'Universidad', 'Universidad de Concepción', 'Educación Diferencial', '2008', 'Titulado')
, ('238773830', 'profesor82@mundoprofes.com', 'Amanda Javiera', 'Mansilla Chacón', 'La Florida', '4776988412', 'Profesor de Lenguaje y Comunicación', 'SI', '0 a 1', 'Universidad', 'Universidad de La Frontera', 'Educación Parvularia', '2009', 'Titulado')
, ('150527155', 'profesor83@mundoprofes.com', 'Romina Jacqueline', 'tapia cisternas', 'La Pintana', '4688264896', 'Profesor de Matemática', 'SI', '2 a 5', 'Universidad', 'Universidad de Atacama', 'Pedagogía en Artes Visuales', '2010', 'Titulado')
, ('113753579', 'profesor84@mundoprofes.com', 'Paulina De Los Angeles', 'Cáceres Lucero', 'La Reina', '8784771609', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '0 a 1', 'Universidad', 'Universidad de Chile', 'Pedagogía en Historia y Ciencias Sociales', '2011', 'Titulado')
, ('190989194', 'profesor85@mundoprofes.com', 'Alexis Bernardo', 'Gonzalez Aravena', 'Las Condes', '8567678917', 'Profesor de Lenguaje y Comunicación', 'SI', '2 a 5', 'Universidad', 'Universidad de Ciencias de la Informática', 'Pedagogía en Historia y Ciencias Sociales', '2012', 'Titulado')
, ('184989298', 'profesor86@mundoprofes.com', 'Felipe Tapia Cisternas', 'Fernandez Muñoz', 'Lo Barnechea', '3714566645', 'Profesor de Matemática', 'SI', '0 a 1', 'Universidad', 'Universidad de Concepción', 'Pedagogía en Historia y Ciencias Sociales', '2010', 'Titulado')
, ('072439619', 'profesor87@mundoprofes.com', 'Nayadeth Elena', 'mena orostica', 'Lo Espejo', '5763740190', 'Profesor de Historia, Geografía y Ciencias Sociales', 'SI', '2 a 5', 'Universidad', 'Universidad de La Frontera', 'Pedagogía en Historia y Ciencias Sociales', '2011', 'Titulado')
, ('169196141', 'profesor88@mundoprofes.com', 'aldo ignacio', 'Parra Andrade', 'Lo Prado', '7676688905', 'Profesor de Lenguaje y Comunicación', 'SI', '0 a 1', 'Universidad', 'Universidad de Atacama', 'Pedagogía en Historia y Ciencias Sociales', '2012', 'Titulado')
;
SELECT COUNT(*) FROM bogus_cvs
;
SELECT COUNT(*) FROM bogus_cvs T JOIN comunas_v2 S ON T.comuna = S.comuna
;
SELECT COUNT(*) FROM bogus_cvs T JOIN profesiones S ON T.profesion = S.profesion
;
INSERT INTO usuarios (email, tipo, precargado)
SELECT email, 'profesor', 1 
FROM bogus_cvs
;
INSERT INTO profesor (id, RUT, nombres, apellidos, id_comuna, telefono_movil, email, tiene_empleo, cv_experiencia)
SELECT U.id, RUT, nombres, apellidos, C.id AS id_comuna, telefono_movil, U.email, tiene_empleo, cv_experiencia
FROM usuarios U
JOIN bogus_cvs T ON U.email = T.email
JOIN comunas_v2 C ON T.comuna = C.comuna
;
INSERT INTO profesor_intereses (id_profesor, id_referencia, categoria)
SELECT U.id, P.id, 'profesiones'
FROM usuarios U
JOIN bogus_cvs T ON U.email = T.email
JOIN profesiones P ON T.profesion = P.profesion
;
INSERT INTO profes_formacion_profesional
SELECT NULL, U.id AS id_profesor, I.id AS id_tipo_institucion, T.id AS id_titulo_profesional, termino, situacion
FROM bogus_cvs Y
JOIN usuarios U ON Y.email = U.email
JOIN instituciones I ON Y.tipo = I.tipo AND Y.institucion = I.institucion
JOIN titulos T ON Y.titulo = T.titulo
;
/*	missing T's

Pedagogía en Historia y Ciencias Sociales
Pedagogía en Inglés
Pedagogía en Lengua Castellana y Comunicación
Pedagogía en Matemáticas
Pedagogía en Música
*/
/*	missing U's 

Universidad Adolfo Ibañez
Universidad Autónoma del Sur
Universidad Bernardo O´Higgins
Universidad Católica Cardenal Raúl Silva Henríquez
Universidad de Ciencias de la Informática
Universidad de los Leones
*/
