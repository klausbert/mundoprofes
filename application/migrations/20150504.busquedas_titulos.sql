--
-- Table structure for table 'busquedas_titulos'
--

CREATE TABLE IF NOT EXISTS busquedas_titulos (
  id int(10) unsigned NOT NULL,
  id_busqueda int(10) unsigned NOT NULL,
  id_titulo int(10) unsigned NOT NULL,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table busquedas_titulos
--
ALTER TABLE busquedas_titulos
 ADD PRIMARY KEY (id), ADD UNIQUE KEY id_busqueda (id_busqueda,id_titulo);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table busquedas_titulos
--
ALTER TABLE busquedas_titulos
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
