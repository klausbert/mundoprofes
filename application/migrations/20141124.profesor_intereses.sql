CREATE TABLE IF NOT EXISTS profesor_intereses (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_profesor int(10) unsigned NOT NULL,
  id_referencia int(10) unsigned NOT NULL,
  referencia varchar(20) NOT NULL,
  PRIMARY KEY (id),
  KEY referencia (id_referencia, referencia)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 
;
ALTER TABLE profesor_intereses
ADD UNIQUE id_profesor (id_profesor, id_referencia, referencia) COMMENT  ''
;

/*	migrate
*/
INSERT INTO profesor_intereses (id_profesor,id_referencia, referencia)
SELECT id_profesor, id_ATE, 'especificaciones_ate' 
FROM profesor_intereses_ATEs S
;
INSERT INTO profesor_intereses (id_profesor,id_referencia, referencia)
SELECT id_profesor, id_comuna, 'comunas' 
FROM profesor_intereses_comunas S
;
INSERT INTO profesor_intereses (id_profesor,id_referencia, referencia)
SELECT id_profesor, id_cargo, 'cargos' 
FROM profesor_intereses_empleos S
;
INSERT INTO profesor_intereses (id_profesor,id_referencia, referencia)
SELECT id_profesor, id_region, 'regiones' 
FROM profesor_intereses_regiones S
;
DROP TABLE profesor_intereses_ATEs, profesor_intereses_comunas, profesor_intereses_empleos, profesor_intereses_regiones
;
