CREATE TABLE usuarios_a_confirmar
SELECT id, request, creado 
FROM usuarios 
WHERE request IS NOT NULL
;
ALTER TABLE usuarios 
ADD facebook VARCHAR(32) NULL DEFAULT NULL AFTER password
;
UPDATE usuarios U 
JOIN profesor P ON U.id = P.id
SET U.facebook = P.facebook
WHERE P.facebook IS NOT NULL
;

