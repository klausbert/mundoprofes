RENAME TABLE usuarios_roles TO usuarios_roles_colegios;

CREATE TABLE usuarios_roles
SELECT MIN(id) AS id, id_usuario, role, created_at, created_by
FROM usuarios_roles_colegios
GROUP BY id_usuario, role
;
ALTER TABLE usuarios_roles CHANGE id id INT(10) UNSIGNED NOT NULL;
ALTER TABLE usuarios_roles ADD PRIMARY KEY(id);
ALTER TABLE usuarios_roles CHANGE id id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE usuarios_roles ADD UNIQUE(id_usuario, role);



-- ALTER TABLE usuarios_roles_colegios 
  -- CHANGE id_role id_usuario INT(10) UNSIGNED NOT NULL
-- ;
ALTER TABLE usuarios_roles_colegios 
  ADD id_role INT(10) UNSIGNED NOT NULL
;

UPDATE usuarios_roles_colegios v1 
JOIN usuarios_roles v2 ON v1.id_usuario = v2.id_usuario AND v1.role = v2.role
SET v1.id_role = v2.id
;
--ALTER TABLE usuarios_roles_colegios DROP FOREIGN KEY usuarios_roles_colegios_ibfk_1;
ALTER TABLE usuarios_roles_colegios DROP INDEX id_usuario;
ALTER TABLE usuarios_roles_colegios ADD UNIQUE id_role (id_role, id_colegio) USING BTREE;
ALTER TABLE usuarios_roles_colegios DROP id_usuario, DROP role;
