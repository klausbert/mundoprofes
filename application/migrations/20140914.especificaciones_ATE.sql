--
-- Table structure for table 'especificaciones_ATE'
--

CREATE TABLE IF NOT EXISTS especificaciones_ATE (
id int(10) unsigned NOT NULL,
  especificacion varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table especificaciones_ATE
--
ALTER TABLE especificaciones_ATE
 ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table especificaciones_ATE
--
ALTER TABLE especificaciones_ATE
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT;


INSERT INTO especificaciones_ATE (especificacion) VALUES
  ('Curriculum')
, ('Instrumento de Evaluación')
, ('Formación Docente')
, ('Formación directivas')
, ('Creación de PEI')
, ('Didáctica')
, ('TIC')
, ('Convivencia Escolar')
;

--
-- Table structure for table 'profesor_intereses_ATEs'
--

CREATE TABLE IF NOT EXISTS profesor_intereses_ATEs (
  id int(10) unsigned NOT NULL,
  id_profesor int(10) unsigned NOT NULL,
  id_ATE int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table profesor_intereses_ATEs
--
ALTER TABLE profesor_intereses_ATEs
 ADD PRIMARY KEY (id), 
 ADD UNIQUE KEY id_profesor (id_profesor,id_ATE), 
 ADD KEY id_ATE (id_ATE);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table profesor_intereses_ATEs
--
ALTER TABLE profesor_intereses_ATEs
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT;