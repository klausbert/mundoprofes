CREATE OR REPLACE VIEW colegios
AS
SELECT * 
FROM colegios_v2
JOIN colegios_v2b ON id = RBD;



CREATE TABLE productos (
  id int(10) unsigned NOT NULL,
  nombre varchar(100) NOT NULL,
  descripcion text NOT NULL,
  caracter_1 tinyint(1) NOT NULL COMMENT 'Caracter�stica uno',
  caracter_2 tinyint(1) NOT NULL COMMENT 'Texto de la caracteristica dos',
  caracter_3 tinyint(1) NOT NULL COMMENT 'Esta es la n�mero tres',
  precio int(10) unsigned NOT NULL,
  descuento int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO productos (id, nombre, descripcion, caracter_1, caracter_2, caracter_3, precio, descuento) VALUES
(1, 'MundoProfes Essentials', 'Descripci�n de lo que contepla este producto', 1, 0, 0, 50000, 35000),
(2, 'MundoProfes Advantage', 'Descripci�n de Advantage', 1, 1, 0, 70000, 55000),
(3, 'MundoProfes Premium', 'Me pregunto cu�ntos avisos compra cada opci�n o se diferencian en duraci�n, posici�n, etc?', 1, 1, 1, 90000, 75000);

ALTER TABLE productos
  ADD PRIMARY KEY (id);

ALTER TABLE productos
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;



CREATE TABLE compras (
  id int(10) unsigned NOT NULL,
  id_colegio int(10) unsigned NOT NULL,
  id_producto int(10) unsigned NOT NULL,
  creado timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  estado enum('pendiente','transferencia','deposito','webpay','cancelado') NOT NULL DEFAULT 'pendiente',
  pagado date DEFAULT NULL,
  verificado varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE compras
  ADD PRIMARY KEY (id),
  ADD KEY FK_productos (id_producto),
  ADD KEY id_colegio (id_colegio);

ALTER TABLE compras
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT
;


ALTER TABLE compras
  ADD FOREIGN KEY (id_colegio)  REFERENCES colegios_v2(id) ON DELETE CASCADE  ON UPDATE CASCADE
, ADD FOREIGN KEY (id_producto) REFERENCES productos(id)   ON DELETE RESTRICT ON UPDATE RESTRICT
;
