CREATE OR REPLACE VIEW asignaturas AS
SELECT id, referencia AS asignatura
FROM _referencias
WHERE categoria = 'asignaturas'
;
CREATE OR REPLACE VIEW cargos AS
SELECT id, referencia AS cargo
FROM _referencias
WHERE categoria = 'cargos'
;
CREATE OR REPLACE VIEW niveles AS
SELECT id, referencia AS nivel
FROM _referencias
WHERE categoria = 'niveles'
;
CREATE OR REPLACE VIEW regiones AS
SELECT IF(id_original = 0, id, id_original) AS id, referencia AS region 
FROM _referencias
WHERE categoria = 'regiones'
;
