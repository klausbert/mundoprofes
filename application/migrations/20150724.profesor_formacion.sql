CREATE TABLE IF NOT EXISTS profesor_formacion (
  id int(10) unsigned NOT NULL,
  id_profesor int(10) unsigned NOT NULL,
  id_institucion int(10) unsigned NOT NULL,
  id_titulo int(10) unsigned NOT NULL,
  anno smallint(5) unsigned NOT NULL,
  situacion_final enum('estudiando','titulado','egresado','sin finalizar') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
;
ALTER TABLE profesor_formacion
  ADD PRIMARY KEY (id)
, ADD KEY profesor (id_profesor)
, ADD KEY institucion (id_institucion)
, ADD KEY titulo (id_titulo)
;
ALTER TABLE profesor_formacion
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1
;
/*	moving data
*/
INSERT INTO profesor_formacion
SELECT NULL, id_profesor, id_tipo_institucion, id_titulo_profesional, anno, situacion_final 
FROM profes_formacion_profesional 
WHERE situacion_final IS NOT NULL
;
INSERT INTO profesor_formacion
SELECT NULL, id_profesor, id_tipo_institucion, id_titulo_postgrado, anno, situacion_final 
FROM profes_formacion_postgrado 
WHERE situacion_final IS NOT NULL
;
/*	swapping tables with views
*/
DROP TABLE profes_formacion_profesional, profes_formacion_postgrado
;
CREATE OR REPLACE VIEW profes_formacion_profesional AS
SELECT id, id_profesor, id_institucion AS id_tipo_institucion, id_titulo AS id_titulo_profesional, anno, situacion_final 
FROM profesor_formacion
WHERE id_titulo IN (
	SELECT id FROM titulos WHERE nivel = 'profesional'
);
CREATE OR REPLACE VIEW profes_formacion_postgrado AS
SELECT id, id_profesor, id_institucion AS id_tipo_institucion, id_titulo AS id_titulo_postgrado, anno, situacion_final 
FROM profesor_formacion
WHERE id_titulo IN (
	SELECT id FROM titulos WHERE nivel = 'postgrado'
);

