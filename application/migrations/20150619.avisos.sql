-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 19, 2015 at 11:36 AM
-- Server version: 5.5.44
-- PHP Version: 5.4.42

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: mundoprofes
--

-- --------------------------------------------------------

--
-- Table structure for table avisos
--

CREATE TABLE avisos (
  id int(10) unsigned NOT NULL,
  id_colegio int(10) unsigned NOT NULL,
  id_region int(10) unsigned NOT NULL,
  id_comuna int(10) unsigned NOT NULL,
  busqueda varchar(99) NOT NULL,
  estado enum('solicitada','en proceso','cerrada','anulada') NOT NULL,
  fecha date NOT NULL,
  cv_experiencia enum('0 a 1','2 a 5','6 a 10','11 a 15','16 a 20','más de 20') DEFAULT NULL COMMENT 'años',
  tamanio enum('Grande','Mediano','Pequeño') DEFAULT NULL,
  dependencia enum('Municipal','Particular Pagado','Particular Subvencionado') NOT NULL,
  laico_religioso enum('Congregación','Iglesia Diocesana','Laico') NOT NULL,
  tipo enum('Colegio','Escuela lenguaje','Jardin') NOT NULL,
  proyecto enum('Bachillerato Internacional','Montessori','Pie','Plan de mejoramiento','Colegio Bilingüe') NOT NULL,
  experiencia_docente enum('Didáctica','Biblioteca','Profesor jefe','UTP') NOT NULL,
  contrato enum('Reemplazo','Plazo Fijo','Indefinido') NOT NULL,
  sueldo_mensual_hasta int(10) unsigned DEFAULT NULL,
  id_funciones_a_cumplir int(10) unsigned NOT NULL COMMENT 'Más de una',
  disponibilidad enum('inmediata','periodo','fecha') DEFAULT NULL,
  contrato_desde date DEFAULT NULL,
  contrato_hasta date DEFAULT NULL,
  descripcion varchar(300) NOT NULL,
  horario varchar(99) NOT NULL,
  cantidad_horas int(10) unsigned NOT NULL,
  id_asignaturas int(10) unsigned NOT NULL COMMENT 'Más de una',
  id_niveles int(10) unsigned NOT NULL COMMENT 'Más de uno',
  otros_beneficios text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table avisos
--

INSERT INTO avisos (id, id_colegio, id_region, id_comuna, busqueda, estado, fecha, cv_experiencia, tamanio, dependencia, laico_religioso, tipo, proyecto, experiencia_docente, contrato, sueldo_mensual_hasta, id_funciones_a_cumplir, disponibilidad, contrato_desde, contrato_hasta, descripcion, horario, cantidad_horas, id_asignaturas, id_niveles, otros_beneficios) VALUES
(5, 1, 0, 0, 'Profesor Chiflado', 'solicitada', '2015-04-14', '6 a 10', 'Mediano', 'Particular Pagado', 'Laico', 'Colegio', '', '', 'Reemplazo', 1234566, 13, 'inmediata', NULL, NULL, 'Deberá enseñar a unos alumnos que son unos diablos!', 'Lunes a Viernes de 9 a 15', 0, 8, 6, 'Plan Dental!'),
(8, 25500, 0, 0, 'Nueva Búsqueda', 'solicitada', '2015-05-05', NULL, NULL, 'Municipal', 'Congregación', 'Colegio', 'Bachillerato Internacional', 'Didáctica', 'Reemplazo', NULL, 0, NULL, NULL, NULL, '', '', 20, 8, 4, ''),
(9, 25500, 0, 0, 'super profe', 'solicitada', '2015-05-09', '6 a 10', 'Mediano', 'Particular Subvencionado', 'Iglesia Diocesana', 'Colegio', 'Pie', 'Didáctica', 'Reemplazo', NULL, 0, NULL, NULL, NULL, '', '', 0, 0, 0, ''),
(12, 25500, 0, 0, 'otro profe', 'en proceso', '2015-05-10', '2 a 5', NULL, 'Municipal', 'Congregación', 'Colegio', 'Bachillerato Internacional', 'Profesor jefe', 'Indefinido', NULL, 4, NULL, NULL, NULL, '', '', 0, 2, 0, ''),
(13, 25500, 0, 0, 'Nueva Búsqueda', 'solicitada', '2015-05-12', NULL, NULL, 'Municipal', 'Congregación', 'Colegio', 'Bachillerato Internacional', 'Didáctica', 'Reemplazo', NULL, 0, NULL, NULL, NULL, '', '', 0, 0, 0, ''),
(14, 25500, 0, 0, 'soporte', 'solicitada', '2015-05-20', '6 a 10', 'Mediano', 'Particular Pagado', 'Laico', 'Colegio', 'Bachillerato Internacional', 'UTP', 'Indefinido', 800000, 15, 'inmediata', NULL, NULL, '', '', 0, 3, 5, 'otros'),
(15, 1, 2, 0, 'Nuevo Aviso A Publicrrrr', '', '2015-06-19', NULL, NULL, 'Municipal', 'Congregación', 'Colegio', 'Bachillerato Internacional', 'Didáctica', 'Reemplazo', NULL, 0, 'inmediata', NULL, NULL, 'super laburito', '', 0, 0, 0, ''),
(16, 1, 2, 0, 'Nuevo Aviso A Publicrrrr', '', '2015-06-19', NULL, NULL, 'Municipal', 'Congregación', 'Colegio', 'Bachillerato Internacional', 'Didáctica', 'Reemplazo', NULL, 0, 'inmediata', NULL, NULL, 'super laburito', '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table avisos_asignaturas
--

CREATE TABLE avisos_asignaturas (
  id int(10) unsigned NOT NULL,
  id_aviso int(10) unsigned NOT NULL,
  id_asignatura int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table avisos_asignaturas
--

INSERT INTO avisos_asignaturas (id, id_aviso, id_asignatura) VALUES
(4, 0, 6),
(1, 5, 9),
(5, 16, 11);

-- --------------------------------------------------------

--
-- Table structure for table avisos_experiencias
--

CREATE TABLE avisos_experiencias (
  id int(10) unsigned NOT NULL,
  id_aviso int(10) unsigned NOT NULL,
  id_cargo int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table avisos_experiencias
--

INSERT INTO avisos_experiencias (id, id_aviso, id_cargo) VALUES
(3, 0, 3),
(1, 5, 15),
(4, 16, 12);

-- --------------------------------------------------------

--
-- Table structure for table avisos_niveles
--

CREATE TABLE avisos_niveles (
  id int(10) unsigned NOT NULL,
  id_aviso int(10) unsigned NOT NULL,
  id_nivel int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table avisos_niveles
--

INSERT INTO avisos_niveles (id, id_aviso, id_nivel) VALUES
(5, 0, 1),
(1, 5, 4),
(2, 5, 12);

-- --------------------------------------------------------

--
-- Table structure for table avisos_profesores
--

CREATE TABLE avisos_profesores (
  id int(10) unsigned NOT NULL,
  id_aviso int(10) unsigned NOT NULL,
  id_profesor int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table avisos_titulos
--

CREATE TABLE avisos_titulos (
  id int(10) unsigned NOT NULL,
  id_aviso int(10) unsigned NOT NULL,
  id_titulo int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table avisos_titulos
--

INSERT INTO avisos_titulos (id, id_aviso, id_titulo) VALUES
(6, 0, 1),
(1, 5, 1),
(7, 16, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table avisos
--
ALTER TABLE avisos
  ADD PRIMARY KEY (id),
  ADD KEY id_colegio (id_colegio),
  ADD KEY estado (estado),
  ADD KEY fecha (fecha),
  ADD KEY id_region (id_region,id_comuna);

--
-- Indexes for table avisos_asignaturas
--
ALTER TABLE avisos_asignaturas
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY uq_aviso_asignatura (id_aviso,id_asignatura);

--
-- Indexes for table avisos_experiencias
--
ALTER TABLE avisos_experiencias
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY uq_aviso_asignatura (id_aviso,id_cargo);

--
-- Indexes for table avisos_niveles
--
ALTER TABLE avisos_niveles
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY uq_aviso_asignatura (id_aviso,id_nivel);

--
-- Indexes for table avisos_profesores
--
ALTER TABLE avisos_profesores
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY uq_aviso_asignatura (id_aviso,id_profesor);

--
-- Indexes for table avisos_titulos
--
ALTER TABLE avisos_titulos
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY uq_aviso_asignatura (id_aviso,id_titulo);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table avisos
--
ALTER TABLE avisos
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table avisos_asignaturas
--
ALTER TABLE avisos_asignaturas
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table avisos_experiencias
--
ALTER TABLE avisos_experiencias
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table avisos_niveles
--
ALTER TABLE avisos_niveles
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table avisos_profesores
--
ALTER TABLE avisos_profesores
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table avisos_titulos
--
ALTER TABLE avisos_titulos
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
