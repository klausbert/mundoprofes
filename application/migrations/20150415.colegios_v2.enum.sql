ALTER TABLE colegios_v2
  CHANGE dependencia dependencia ENUM('Municipal','Particular Pagado','Particular Subvencionado') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;


ALTER TABLE colegios_busquedas
  CHANGE id_dependencia dependencia ENUM('Municipal','Particular Pagado','Particular Subvencionado') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
;
