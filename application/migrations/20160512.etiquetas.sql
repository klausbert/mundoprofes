INSERT INTO _referencias (categoria, referencia, id_original)
SELECT 'etiquetas', perfil, id
FROM perfiles
;
DROP TABLE perfiles
;
CREATE OR REPLACE VIEW perfiles
AS
SELECT IF(id_original = 0, id, id_original) AS id, referencia AS perfil
FROM _referencias
WHERE categoria = 'etiquetas'
;
CREATE OR REPLACE VIEW etiquetas
AS
SELECT IF(id_original = 0, id, id_original) AS id, referencia AS etiqueta
FROM _referencias
WHERE categoria = 'etiquetas'
;
