INSERT INTO _referencias (categoria, referencia)
SELECT 'profesiones', profesion
FROM profesiones
;
DROP TABLE profesiones 
;
CREATE OR REPLACE VIEW profesiones
AS
SELECT id, referencia AS profesion
FROM _referencias
WHERE categoria = 'profesiones'
;

