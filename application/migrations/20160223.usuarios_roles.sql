CREATE TABLE usuarios_roles (
  id int(10) UNSIGNED NOT NULL,
  id_usuario int(10) UNSIGNED NOT NULL,
  id_colegio int(10) UNSIGNED DEFAULT NULL,
  role enum('sostenedor','director','gestor','vendedor') NOT NULL DEFAULT 'gestor',
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE usuarios_roles
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY id_usuario (id_usuario,id_colegio) USING BTREE,
  ADD KEY fk_colegios (id_colegio),
  ADD KEY fk_creador (created_by);

ALTER TABLE usuarios_roles
  MODIFY id int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE usuarios_roles
  ADD CONSTRAINT usuarios_roles_ibfk_1 FOREIGN KEY (id_usuario) REFERENCES usuarios (id),
  ADD CONSTRAINT usuarios_roles_ibfk_2 FOREIGN KEY (id_colegio) REFERENCES colegios_v2 (id)
;

 
 
INSERT INTO usuarios_roles (id_usuario, id_colegio, role, created_by)
SELECT id_usuario, id, 'sostenedor', id_usuario 
FROM colegios 
WHERE id_usuario IS NOT NULL
;
