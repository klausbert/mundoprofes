--
-- Table structure for table 'usuarios'
--

CREATE TABLE IF NOT EXISTS usuarios (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  email varchar(99) NOT NULL,
  `password` varchar(40) DEFAULT NULL COMMENT 'SHA1',
  request varchar(40) DEFAULT NULL COMMENT 'SHA1',
  tipo enum('profesor','padre','colegio','admin') NOT NULL DEFAULT 'profesor',
  creado timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
