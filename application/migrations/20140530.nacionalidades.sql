CREATE TABLE IF NOT EXISTS nacionalidades (
	id int(10) unsigned NOT NULL AUTO_INCREMENT,
	nacionalidad varchar(20) NOT NULL,
	destacada tinyint(1) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
;
INSERT INTO nacionalidades (nacionalidad, destacada) VALUES
  ('Chilena', 1)
, ('Argentina', 1)
, ('Boliviana', 1)
, ('Brasileña', 1)
, ('Colombiana', 1)
, ('Cubana', 1)
, ('Ecuatoriana', 1)
, ('Española', 1)
, ('Mexicana', 1)
, ('Paraguaya', 1)
, ('Peruana', 1)
, ('Uruguaya', 1)
, ('Venezolana', 1)
, ('Afgana', 0)
, ('Albanesa', 0)
, ('Alemana', 0)
, ('Alto volteña', 0)
, ('Andorrana', 0)
, ('Angoleña', 0)
, ('Argelina', 0)
, ('Australiana', 0)
, ('Austriaca', 0)
, ('Bahamesa', 0)
, ('Bahreina', 0)
, ('Bangladesha', 0)
, ('Barbadesa', 0)
, ('Belga', 0)
, ('Beliceña', 0)
, ('Bermudesa', 0)
, ('Birmana', 0)
, ('Botswanesa', 0)
, ('Bulgara', 0)
, ('Burundesa', 0)
, ('Butana', 0)
, ('Camboyana', 0)
, ('Camerunesa', 0)
, ('Canadiense', 0)
, ('Centroafricana', 0)
, ('Chadeña', 0)
, ('Checoslovaca', 0)
, ('China', 0)
, ('China', 0)
, ('Chipriota', 0)
, ('Congoleña', 0)
, ('Costarricense', 0)
, ('Dahoneya', 0)
, ('Danes', 0)
, ('Dominicana', 0)
, ('Egipcia', 0)
, ('Emirata', 0)
, ('Escosesa', 0)
, ('Eslovaca', 0)
, ('Estona', 0)
, ('Etiope', 0)
, ('Fijena', 0)
, ('Filipina', 0)
, ('Finlandesa', 0)
, ('Francesa', 0)
, ('Gabiana', 0)
, ('Gabona', 0)
, ('Galesa', 0)
, ('Ghanesa', 0)
, ('Granadeña', 0)
, ('Griega', 0)
, ('Guatemalteca', 0)
, ('Guinesa Ecuatoriana', 0)
, ('Guinesa', 0)
, ('Guyanesa', 0)
, ('Haitiana', 0)
, ('Holandesa', 0)
, ('Hondureña', 0)
, ('Hungara', 0)
, ('India', 0)
, ('Indonesa', 0)
, ('Inglesa', 0)
, ('Iraki', 0)
, ('Irani', 0)
, ('Irlandesa', 0)
, ('Islandesa', 0)
, ('Israeli', 0)
, ('Italiana', 0)
, ('Jamaiquina', 0)
, ('Japonesa', 0)
, ('Jordana', 0)
, ('Katensa', 0)
, ('Keniana', 0)
, ('Kuwaiti', 0)
, ('Laosiana', 0)
, ('Leonesa', 0)
, ('Lesothensa', 0)
, ('Letonesa', 0)
, ('Libanesa', 0)
, ('Liberiana', 0)
, ('Libeña', 0)
, ('Liechtenstein', 0)
, ('Lituana', 0)
, ('Luxemburgo', 0)
, ('Madagascar', 0)
, ('Malaca', 0)
, ('Malawi', 0)
, ('Maldivas', 0)
, ('Mali', 0)
, ('Maltesa', 0)
, ('Marfilesa', 0)
, ('Marroqui', 0)
, ('Mauricio', 0)
, ('Mauritana', 0)
, ('Monaco', 0)
, ('Mongolesa', 0)
, ('Nauru', 0)
, ('Neozelandesa', 0)
, ('Nepalesa', 0)
, ('Nicaraguense', 0)
, ('Nigerana', 0)
, ('Nigeriana', 0)
, ('Norcoreana', 0)
, ('Norirlandesa', 0)
, ('Norteamericana', 0)
, ('Noruega', 0)
, ('Omana', 0)
, ('Pakistani', 0)
, ('Panameña', 0)
, ('Polaca', 0)
, ('Portoriqueña', 0)
, ('Portuguesa', 0)
, ('Rhodesiana', 0)
, ('Ruanda', 0)
, ('Rumana', 0)
, ('Rusa', 0)
, ('Salvadoreña', 0)
, ('Samoa Occidental', 0)
, ('San marino', 0)
, ('Saudi', 0)
, ('Senegalesa', 0)
, ('Sikkim', 0)
, ('Singapur', 0)
, ('Siria', 0)
, ('Somalia', 0)
, ('Sovietica', 0)
, ('Sri Lanka', 0)
, ('Suazilandesa', 0)
, ('Sudafricana', 0)
, ('Sudanesa', 0)
, ('Sueca', 0)
, ('Suiza', 0)
, ('Surcoreana', 0)
, ('Tailandesa', 0)
, ('Tanzana', 0)
, ('Tonga', 0)
, ('Tongo', 0)
, ('Trinidad y Tobago', 0)
, ('Tunecina', 0)
, ('Turca', 0)
, ('Ugandesa', 0)
, ('Vaticano', 0)
, ('Vietnamita', 0)
, ('Yemen Rep Arabe', 0)
, ('Yemen Rep Dem', 0)
, ('Yugoslava', 0)
, ('Zaire', 0)
;