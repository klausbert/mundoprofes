ALTER TABLE compras
  CHANGE estado estado ENUM('pendiente','verificado','rechazado') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'pendiente'
;



ALTER TABLE compras 
  ADD pago_monto INT UNSIGNED NOT NULL AFTER pago_forma
;