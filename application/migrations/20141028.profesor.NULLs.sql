ALTER TABLE profesor
  CHANGE RUT RUT VARCHAR(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
, CHANGE nombres nombres VARCHAR(99) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
, CHANGE apellidos apellidos VARCHAR(99) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
, CHANGE sexo sexo ENUM('F','M') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
, CHANGE estado_civil estado_civil ENUM('casado','soltero','separado','viudo') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
, CHANGE fecha_nacimiento fecha_nacimiento DATE NULL DEFAULT NULL
, CHANGE domicilio domicilio VARCHAR(99) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
, CHANGE tiene_empleo tiene_empleo ENUM('NO','SI') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
, CHANGE cantidad_horas cantidad_horas INT(10) UNSIGNED NULL DEFAULT NULL;
