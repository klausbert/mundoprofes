ALTER TABLE colegios_v2
  ADD domicilio VARCHAR(99)  NULL DEFAULT NULL 
, ADD telefono  INT UNSIGNED NULL DEFAULT NULL 
, ADD email     VARCHAR(99)  NULL DEFAULT NULL 
, ADD website   VARCHAR(99)  NULL DEFAULT NULL 
, ADD facebook  VARCHAR(99)  NULL DEFAULT NULL
, ADD linkedIn  VARCHAR(99)  NULL DEFAULT NULL
, ADD id_usuario INT UNSIGNED NULL DEFAULT NULL 
, ADD id_cargo   INT UNSIGNED NULL DEFAULT NULL 
, ADD INDEX (id_usuario) 
, ADD INDEX (id_cargo) 
;
ALTER TABLE colegios_v2
  CHANGE id id INT(10) UNSIGNED NOT NULL COMMENT 'RBD'
;
ALTER TABLE colegios_v2
  ADD RUT varchar(12)            NULL DEFAULT NULL
, ADD razon_social varchar(99)   NULL DEFAULT NULL
, ADD giro_comercial varchar(50) NULL DEFAULT NULL
, ADD domicilio_fc varchar(99)   NULL DEFAULT NULL
, ADD telefono_fc int(10) unsigned NULL DEFAULT NULL
, ADD email_fc varchar(99)       NULL DEFAULT NULL
;

