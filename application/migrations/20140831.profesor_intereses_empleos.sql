CREATE TABLE IF NOT EXISTS profesor_intereses_empleos (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_profesor int(10) unsigned NOT NULL,
  id_cargo int(10) unsigned NOT NULL,
  PRIMARY KEY (id),
  KEY id_profesor (id_profesor),
  KEY id_cargo (id_cargo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 
;
ALTER TABLE profesor_intereses_empleos 
DROP INDEX  id_profesor ,
ADD UNIQUE  id_profesor (  id_profesor ,  id_cargo ) COMMENT  ''
;