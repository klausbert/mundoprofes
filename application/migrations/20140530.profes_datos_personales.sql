CREATE TABLE IF NOT EXISTS profes_datos_personales (
	id int(10) unsigned NOT NULL AUTO_INCREMENT,
	id_perfil int(10) unsigned NULL,
	RUT varchar(12) NOT NULL,
	nombres varchar(99) NOT NULL,
	apellidos varchar(99) NOT NULL,
	sexo enum('F', 'M') NOT NULL,
	estado_civil enum('casado', 'soltero', 'separado', 'viudo') NOT NULL,
	fecha_nacimiento DATETIME NOT NULL,
	id_nacion int(10) unsigned NOT NULL,
	domicilio varchar(99) NOT NULL,
	id_comuna int(10) unsigned NOT NULL,
	telefono_fijo int(10) unsigned NULL,
	telefono_movil int(10) unsigned NULL,
	email varchar(99) NOT NULL,
	email_alt varchar(99) NULL,
	tiene_empleo enum('NO', 'SI') NOT NULL,
	PRIMARY KEY (id),
	KEY id_comuna (id_comuna),
	UNIQUE KEY email (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
;