ALTER TABLE profesor 
  ADD UNIQUE(RUT)
;
ALTER TABLE profesor 
  ADD cv VARCHAR(99) NULL DEFAULT NULL 
, ADD cv_postgrado enum('NO','SI') NULL DEFAULT NULL 
, ADD cv_experiencia ENUM('0 a 1','2 a 5','6 a 10','11 a 15','16 a 20','m�s de 20') NULL DEFAULT NULL COMMENT 'A�os' 
, ADD cv_comentarios TEXT NULL DEFAULT NULL 
, ADD INDEX (cv_experiencia) 
;
