ALTER TABLE titulos DROP id
;
ALTER TABLE titulos CHANGE uid id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
;
CREATE OR REPLACE VIEW titulos_profesionales AS
SELECT id, nivel, tipo, titulo AS carrera FROM titulos WHERE nivel = 'profesional'
;
CREATE OR REPLACE VIEW titulos_postgrado AS
SELECT id, nivel, tipo, titulo AS postgrado FROM titulos WHERE nivel = 'postgrado'
;
