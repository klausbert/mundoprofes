CREATE TABLE IF NOT EXISTS _referencias (
  id int(10) unsigned NOT NULL,
  id_original int(10) unsigned NOT NULL,
  categoria varchar(20) NOT NULL,
  referencia varchar(99) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE _referencias
  ADD PRIMARY KEY (id), ADD KEY id_original (id_original,categoria);

ALTER TABLE _referencias
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT;

/*	INSERTs
*/
/*	asignaturas
*/
INSERT INTO _referencias
SELECT NULL, id, 'asignaturas', asignatura, NULL, 1
FROM asignaturas
;
/*	cargos
*/
INSERT INTO _referencias
SELECT NULL, id, 'cargos', cargo, NULL, 1
FROM cargos WHERE tipo = 'Colegio'
;
INSERT INTO _referencias
SELECT NULL, id, CONCAT('cargos_', tipo), cargo, NULL, 1
FROM cargos WHERE tipo <> 'Colegio'
;
/*	niveles
*/
INSERT INTO _referencias
SELECT NULL, id, 'niveles', nivel, NULL, 1
FROM niveles
;
/*	regiones
*/
INSERT INTO _referencias
SELECT NULL, id, 'regiones', region, NULL, 1
FROM regiones
;
/*	replace Tables with VIEWS
*/
/*	asignaturas
*/
DROP TABLE asignaturas
;
CREATE OR REPLACE VIEW asignaturas AS
SELECT IF(id_original = 0, id, id_original) AS id, referencia AS asignatura
FROM _referencias
WHERE categoria = 'asignaturas'
;
/*	cargos
*/
DROP TABLE cargos
;
CREATE OR REPLACE VIEW cargos AS
SELECT IF(id_original = 0, id, id_original) AS id, referencia AS cargo
FROM _referencias
WHERE categoria = 'cargos'
;
/*	niveles
*/
DROP TABLE niveles
;
CREATE OR REPLACE VIEW niveles AS
SELECT IF(id_original = 0, id, id_original) AS id, referencia AS nivel
FROM _referencias
WHERE categoria = 'niveles'
;
/*	regiones
*/
DROP TABLE regiones
;
CREATE OR REPLACE VIEW regiones AS
SELECT IF(id_original = 0, id, id_original) AS id, referencia AS region
FROM _referencias
WHERE categoria = 'regiones'
;
