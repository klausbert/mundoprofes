CREATE TABLE avisos_referencias (
  id int(10) UNSIGNED NOT NULL,
  id_aviso int(10) UNSIGNED NOT NULL,
  id_referencia int(10) UNSIGNED NOT NULL,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
ALTER TABLE avisos_referencias
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY uq_aviso_referencia (id_aviso, id_referencia)
;
ALTER TABLE avisos_referencias
  CHANGE id id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT
;


INSERT INTO avisos_referencias (id_aviso, id_referencia)
	SELECT id_aviso, id_asignatura
	FROM avisos_asignaturas
UNION ALL
	SELECT id_aviso, id_cargo
	FROM avisos_experiencias
UNION ALL
	SELECT id_aviso, id_nivel
	FROM avisos_niveles
;


DROP TABLE avisos_asignaturas;
CREATE VIEW avisos_asignaturas AS
SELECT id, id_aviso, id_referencia AS id_asignatura
FROM avisos_referencias
WHERE id_referencia IN (
	SELECT id FROM _referencias WHERE categoria = 'asignaturas'
);
DROP TABLE avisos_experiencias;
CREATE VIEW avisos_experiencias AS
SELECT id, id_aviso, id_referencia AS id_cargo
FROM avisos_referencias
WHERE id_referencia IN (
	SELECT id FROM _referencias WHERE categoria = 'cargos'
);
DROP TABLE avisos_niveles;
CREATE VIEW avisos_niveles AS
SELECT id, id_aviso, id_referencia AS id_nivel
FROM avisos_referencias
WHERE id_referencia IN (
	SELECT id FROM _referencias WHERE categoria = 'niveles'
);
