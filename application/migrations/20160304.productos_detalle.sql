SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS productos;
CREATE TABLE productos (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO productos (id, nombre, descripcion) VALUES
(1, 'MundoProfes Basic', 'La forma m�s f�cil, efectiva y econ�mica de publicar tu oferta laboral docente.'),
(2, 'MundoProfes Essentials', 'Gestiona de manera aut�noma, �gil y econ�mica campa�as de reclutamiento de profesores con una herramienta 100% web'),
(3, 'MundoProfes Premium', 'Mediante un proceso muy r�pido, consigue tus mejores candidatos para entrevistar.'),
(4, 'Proceso de Selecci�n full', 'Entendemos tu colegio. Toda una organizaci�n exclusivamente pensada para conseguir los mejores profesores para tu colegio.');

DROP TABLE IF EXISTS productos_detalle;
CREATE TABLE productos_detalle (
  `id` int(10) UNSIGNED NOT NULL,
  `id_producto` int(10) UNSIGNED NOT NULL,
  `orderBy` tinyint(3) UNSIGNED NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `token` varchar(10) DEFAULT NULL,
  `precio` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO productos_detalle (id, id_producto, orderBy, nombre, token, precio) VALUES
(1, 1, 1, 'Una Publicaci�n $25.000.- pesos', '1nxfvta', 25000),
(2, 1, 2, 'Dos Publicaciones $40.000.- pesos', '1nxfvta', 40000),
(3, 1, 3, 'Pack de tres Publicaciones $60.000.- pesos', '1nxfvta', 60000),
(4, 2, 1, 'Una Campa�a $60.000.- pesos', '21ggwa5', 60000),
(5, 2, 2, 'Dos Campa�as $100.000.- pesos', '21ggwa5', 100000),
(6, 2, 3, 'Pack de tres Campa�as $130.000.- pesos', '21ggwa5', 130000),
(7, 3, 1, 'Un Reclutamiento �$150.000.- pesos', 'ff1ottl', 150000),
(8, 3, 2, 'Dos Reclutamientos �$270.000.- pesos', 'ff1ottl', 270000),
(9, 3, 3, 'Pack de tres Reclutamientos $380.000.- pesos', 'ff1ottl', 380000);


ALTER TABLE productos
  ADD PRIMARY KEY (`id`);

ALTER TABLE productos_detalle
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_productos` (`id_producto`);


ALTER TABLE productos
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE productos_detalle
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

ALTER TABLE productos_detalle
  ADD CONSTRAINT `productos_detalle_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES productos (`id`);
