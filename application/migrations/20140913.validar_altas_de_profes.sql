ALTER TABLE colegios_v2 ADD validar TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT  'Alta de Profe';
UPDATE colegios_v2 SET validar = NULL;

ALTER TABLE instituciones ADD validar TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT  'Alta de Profe';
UPDATE instituciones SET validar = NULL;

ALTER TABLE titulos_postgrado ADD validar TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT  'Alta de Profe';
UPDATE titulos_postgrado SET validar = NULL;

ALTER TABLE titulos_profesionales ADD validar TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT  'Alta de Profe';
UPDATE titulos_profesionales SET validar = NULL;


