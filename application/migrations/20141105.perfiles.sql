--
-- Table structure for table perfiles
--

CREATE TABLE IF NOT EXISTS perfiles (
id int(10) unsigned NOT NULL,
  perfil varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table perfiles
--
ALTER TABLE perfiles
 ADD PRIMARY KEY (id), ADD UNIQUE KEY perfil (perfil);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table perfiles
--
ALTER TABLE perfiles
MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT
;
INSERT INTO perfiles (perfil) VALUES 
  ('Profesor junior básica')
, ('Profesor senior básica')
, ('Profesor junior media')
, ('Profesor senior media')
, ('Jefe UTP junior')
, ('Jefe UTP senior')
, ('Director junior')
, ('Director senior')
, ('Profesor Química junior')
, ('Profesor Química senior')
, ('Profesor Clases particulares')
, ('Profesor Reemplazo')
, ('Profesor de Religión')
, ('Profesor de Matemática')
, ('Profesor de Lenguaje')
, ('Profesor de Historia')
, ('Profesor de Filosofia')
, ('Profesor')
;
--
-- Table structure for table `profesor_cv_perfiles`
--

CREATE TABLE IF NOT EXISTS `profesor_cv_perfiles` (
`id` int(10) unsigned NOT NULL,
  `id_profesor` int(10) unsigned NOT NULL,
  `id_perfil` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profesor_cv_perfiles`
--
ALTER TABLE `profesor_cv_perfiles`
 ADD PRIMARY KEY (`id`), ADD KEY `id_profesor` (`id_profesor`,`id_perfil`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profesor_cv_perfiles`
--
ALTER TABLE `profesor_cv_perfiles`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
