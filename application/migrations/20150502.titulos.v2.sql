--
-- Table structure for table 'titulos'
--

CREATE TABLE IF NOT EXISTS titulos (
  uid int(10) unsigned NOT NULL,
  id int(10) unsigned NOT NULL,
  nivel enum('profesional','postgrado') DEFAULT NULL,
  tipo enum('T�cnico','Profesional','Menci�n','Magister','Doctorado') DEFAULT NULL,
  titulo varchar(99) NOT NULL DEFAULT '',
  validar timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Indexes for table titulos
--
ALTER TABLE titulos
 ADD PRIMARY KEY (uid);

--
-- AUTO_INCREMENT for table titulos
--
ALTER TABLE titulos
MODIFY uid int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

--
-- Move existing data into new table
--
INSERT INTO titulos (id, nivel, tipo, titulo)
	SELECT id, 'profesional', tipo, carrera
	FROM titulos_profesionales
UNION ALL
	SELECT id, 'postgrado', tipo, postgrado
	FROM titulos_postgrado
;

/*	Related (updateable) views
*/
RENAME TABLE titulos_profesionales TO titulos_profesionales_v1;

CREATE VIEW titulos_profesionales AS
SELECT id, nivel, tipo, titulo AS carrera FROM titulos WHERE nivel = 'profesional'
;

RENAME TABLE titulos_postgrado TO titulos_postgrado_v1;

CREATE VIEW titulos_postgrado AS
SELECT id, nivel, tipo, titulo AS postgrado FROM titulos WHERE nivel = 'postgrado'
;

--
-- Activate new [u]id 
--
UPDATE profes_formacion_postgrado P
JOIN titulos T ON P.id_titulo_postgrado = T.id AND T.nivel = 'postgrado'
SET P.id_titulo_postgrado = T.uid
;
CREATE OR REPLACE VIEW titulos_profesionales AS
SELECT uid AS id, nivel, tipo, titulo AS carrera FROM titulos WHERE nivel = 'profesional'
;
CREATE OR REPLACE VIEW titulos_postgrado AS
SELECT uid AS id, nivel, tipo, titulo AS postgrado FROM titulos WHERE nivel = 'postgrado'
;

