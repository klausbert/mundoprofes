CREATE TABLE avisos__referencias (
  id int(10) unsigned NOT NULL,
  id_aviso int(10) unsigned NOT NULL,
  categoria varchar(20) NOT NULL,
  id_referencia int(10) unsigned NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
;
ALTER TABLE avisos__referencias
  ADD PRIMARY KEY (id),
  ADD KEY id_aviso (id_aviso,categoria),
  ADD UNIQUE KEY referencia (categoria,id_referencia)
;
ALTER TABLE avisos__referencias
  MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1
;
-- 
-- copy existing data
-- 
INSERT INTO avisos__referencias
	SELECT NULL, id_aviso, 'asignaturas', id_asignatura, NULL, 1
	FROM avisos_asignaturas
UNION ALL 
	SELECT NULL, id_aviso, 'experiencias', id_cargo, NULL, 1
	FROM avisos_experiencias
UNION ALL 
	SELECT NULL, id_aviso, 'niveles', id_nivel, NULL, 1
	FROM avisos_niveles
UNION ALL 
	SELECT NULL, id_aviso, 'profesores', id_profesor, NULL, 1
	FROM avisos_profesores
UNION ALL 
	SELECT NULL, id_aviso, 'titulos', id_titulo, NULL, 1
	FROM avisos_titulos
;
