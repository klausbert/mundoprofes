CREATE TABLE IF NOT EXISTS profesor_intereses_comunas (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  id_profesor int(10) unsigned NOT NULL,
  id_comuna int(10) unsigned NOT NULL,
  PRIMARY KEY (id),
  KEY id_profesor (id_profesor),
  KEY id_comuna (id_comuna)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 
;
ALTER TABLE profesor_intereses_comunas 
DROP INDEX  id_profesor ,
ADD UNIQUE  id_profesor (  id_profesor ,  id_comuna ) COMMENT  ''
;