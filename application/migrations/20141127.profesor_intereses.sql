ALTER TABLE profesor_intereses 
  CHANGE referencia referencia ENUM('regiones', 'comunas', 'asignaturas', 'asig_clases_part', 'niveles', 'cargos', 'especificaciones_ate') 
  CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
;
