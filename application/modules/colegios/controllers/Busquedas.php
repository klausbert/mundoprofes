<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busquedas extends REST_Controller_Colegios
{
	public function __construct()
	{
		parent::__construct('busquedas', 'GET,POST,PUT,DELETE');
	}
	protected function index_get()
	{
		$ids_colegios = $this->session->userdata('ids_colegios');
		
		if (isset($this->data['id']))	// UNA búsqueda
		{
			if ($this->data['id'])	// exists
			{
				$data['item'] = $this->db->where("id_colegio IN ($ids_colegios)")->where($this->data)->get($this->model)->row_array();
				$data['item']['titulos'] = $this->db->select('T.*')
				->from('busquedas_titulos B')->join('titulos T', 'B.id_titulo = T.id')
				->where("B.id_busqueda = {$data['item']['id']}")->get()->result_array();
			}
			else	// 0 ==> new
			if (isset($this->data['id_colegio']))
				$data['item'] = array('id' => 0, 'id_colegio' => $this->data['id_colegio'], 'busqueda' => 'Nueva Búsqueda', 'fecha' => date('Y-m-d'), 'estado' => 'solicitada');
			
			if (! $data['item'])
				$data['error'] = "Busqueda #{$this->data['id']} no encontrada"
			;
			else
			{
				foreach ($this->db->query("SHOW COLUMNS FROM {$this->model} WHERE type LIKE 'enum(%'")->result_array() as $row)
				{
					preg_match_all("/'(.*?)'/", $row['Type'], $enum_array);
					$data['options'][$row['Field']] = $enum_array[1];
				}
			}
		}
		else
		if (isset($this->data['id_colegio']))
		{
			$data['list'] = $this->db->where("id_colegio IN ($ids_colegios)")->where($this->data)->get($this->model)->result_array();
		}
		$this->response($data, isset($data['error']) ? 404 : 200);
		// $this->response(array('sql' => $this->db->last_query()));
	}
	protected function index_post()
	{
		if ($this->data) 
		{
			unset($this->data['id']);
			
			$this->db->set($this->cleanup($this->data))->insert($this->model);
			
			$this->data = array('id' => $this->db->insert_id());
			$this->index_get();
		}
	}
	protected function index_put()
	{
		if ($this->data) 
		{
			$where['id'] = $this->data['id']; unset($this->data['id']);
			
			$this->db->update($this->model, $this->cleanup($this->data), $where);
			$this->_titulos_put($where['id'], $this->data['titulos']);
			
			$this->data = array('id' => $where['id']);
			$this->index_get();
		}
	}
	private function _titulos_put($id_busqueda, $ids_titulos)
	{
		foreach ($ids_titulos as $item) 
		{
			$ids[] = $item['id'];
			$values[] = "($id_busqueda, {$item['id']})";
		}
		if (isset($values))
		{
			$this->db->query("INSERT IGNORE INTO busquedas_titulos (id_busqueda, id_titulo) VALUES ". implode(',', $values));
			$this->db->query("DELETE FROM busquedas_titulos WHERE id_busqueda = $id_busqueda AND id_titulo NOT IN (". implode(',', $ids) .")");
		}
	}
}
