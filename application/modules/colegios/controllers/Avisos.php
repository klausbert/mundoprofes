<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Avisos extends Base_Controller
{
	protected function index_post()
	{
		$this->status(405);
	}
	protected function index_put()
	{
		if ($this->put() and $result = $this->model->update($this->put()))
		{
			if ($result['estado']=='preparado')
			{
				//	$result['email'] = LIST OF RECIPIENT EMAILS (ADMIN)
				
				$this->load->module('mailing');
				$result['post'] = 'aviso preparado';	//	wp
				$result['mailing'] = $this->mailing->post($result);
			}
			$this->response($result);
		}
	}
	protected function index_delete()
	{
		$this->status(405);
	}
	public function download_get()	// copied from admin/cvs
	{
		$this->load->model('session_model');
		$this->user = $this->session_model->index();
		
		$auth_colegio = FALSE;
		foreach ($this->user['roles'] as $role)
		if ($role['tipo']=='colegios')
			$auth_colegio = TRUE
		;
		if ($this->user['tipo']=='colegios' or $auth_colegio)
		{
			$parts = $this->model->download($this->input->get('id'));

			// $file = BASEPATH .'../z/'. $parts['assets'] .'/'. $parts['cv'];
			$file = APPPATH .'../z/'. $parts['assets'] .'/'. $parts['cv'];
log_error('download file', $file);
			header("Pragma: public");
			header("Content-disposition: attachment; filename=".$parts['cv']);
			header("Content-type: ". finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $file ));
			header('Content-Transfer-Encoding: binary');
			ob_clean();
			flush();
			readfile($file);
		}
	}
	public function postulantes_get($id)
	{
		$this->response($this->model->postulantes($id));
	}
}
