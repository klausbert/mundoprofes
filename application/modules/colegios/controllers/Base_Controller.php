<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends REST_Controller_v3
{
	protected $user;
	
	public function __construct()
	{
		parent::__construct();
		
		if (! $this->session->userdata('id'))
			$this->abort_status = 401;
		else
		{
			$this->load->model('session_model');
			$this->user = $this->session_model->index();
			
			$this->_get_args    = array_merge($this->_get_args,    ['ids_colegios' => @$this->user['ids_colegios']]);
			$this->_post_args   = array_merge($this->_post_args,   ['ids_colegios' => @$this->user['ids_colegios']]);
			$this->_put_args    = array_merge($this->_put_args,    ['ids_colegios' => @$this->user['ids_colegios']]);
			$this->_delete_args = array_merge($this->_delete_args, ['ids_colegios' => @$this->user['ids_colegios']]);
			
			$this->abort_status = 403;
			if ($this->user['tipo']=='colegios' || $this->user['tipo']=='admin')
				$this->abort_status = 0;
			else
			foreach ($this->user['roles'] as $role)
				if ($role['tipo']=='colegios')
					$this->abort_status = 0;
		}
		if ($this->abort_status)
			exit($this->abort_status);		
	}
	public function __destruct()
	{
		if ($this->abort_status)
			$this->response(NULL, $this->abort_status);
		
		parent::__destruct();
	}
}
