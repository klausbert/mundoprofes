<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Compras extends Base_Controller
{
	// public function __construct()
	// {
		// parent::__construct();
		
		// log_error('session', $this->session->all_userdata());
	// }
	protected function index_post()
	{
		log_error('posted values are', $this->post());
		
		if ($this->post() 
		and $result = $this->model->create($this->post()))
		{
			//	$result['email'] = LIST OF RECIPIENT EMAILS (ADMIN)
			
			$this->load->module('mailing');
			$result['post'] = 'compra';	//	wp
			$result['mailing'] = $this->mailing->post($result);
		}
		$this->response($result);
	}
	protected function index_delete()
	{
		$this->status(405);
	}
	/*
	**	etc
	*/
	protected function pagos_post()
	{
		$values = array_merge($this->_post_args, $_FILES['file']);
		
		$this->response($this->model->upload($values));
	}
}
