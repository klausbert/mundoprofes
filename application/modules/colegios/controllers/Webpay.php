<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webpay extends MX_Controller
{
	public function __construct()
	{
		$this->user = $this->load->model('session_model')->index();
	}
	public function send()
	{
		$pago = date("Ymdhis");
		
		$this->load->model('Compras_model', 'compras');
		$result = $this->compras->create([
			'id_colegio' => $this->input->get('id_colegio'),
			'id_producto' => $this->input->get('id_producto'),
			'estado' => 'pendiente',
			'pago_fecha' => date('Y-m-d'),
			'pago_forma' => 'webpay',
			'pago_id' => $pago
		]);
		// $this->_response($result);
		// return;
		$pago .= $result['id'];	//	por si entran dos en el mismo segundo
		$this->compras->update(['id' => $result['id'], 'pago_id' => $pago]);
		
		$item = $this->db->where('id', $this->input->get('id_producto'))->get('productos_detalle')->row_array();
		
		$item['TBK_ID_SESION']    = $pago;
		$item['TBK_ORDEN_COMPRA'] = $pago;
		/****************** CONFIGURACION *******************/
		//Archivos de datos para uso de pagina de cierre
		$myPath = APPPATH ."logs/tbk_$pago.log";
		$fic = fopen($myPath, "w+");
		$linea = $item['precio'] ."00;$pago";
		fwrite($fic, $linea);
		fclose($fic);
		
		log_error('Send', array_merge($_POST, [
			'session_id' => $this->session->userdata('session_id'),
			'ip_address' => $this->session->userdata('ip_address'),
			'sess_email' => $this->user['email'     ],
		]));
		
		$this->load->view('colegios/compras/webpay', $item);
	}
	public function done()
	{
		$this->_done_fail('verificado');
	}
	public function fail()
	{
		$this->_done_fail('rechazado');
	}
	private function _done_fail($status)
	{
		if (! in_array($status, ['verificado', 'rechazado']))
			return;
		
		log_error($status, array_merge($_POST, [
			'session_id' => $this->session->userdata('session_id'),
			'ip_address' => $this->session->userdata('ip_address'),
			'sess_email' => $this->user['email'     ],
		]));
		
		$pago = $this->input->post('TBK_ID_SESION');
		$item = $this->db->where('pago_id', $pago)->get('compras')->row_array();
		$item['estado'] = $status;
		
		$this->load->model('admin/compras_model', 'compras');
		$this->compras->update($item);	// acá es TARDE, hay que hacerlo en CHECK
		
		redirect("#/colegios/{$item['id_colegio']}/productos/{$item['id_producto']}/webpay/$status/$pago");	// cambiar done/fail por verif./recha.
		// redirect("https://www.mundoprofes.com/#/colegios/{$item['id_colegio']}/productos/{$item['id_producto']}/webpay/$status/$pago");	// cambiar done/fail por verif./recha.
	}
	public function data($pago = FALSE)
	{
		if (! $pago)
			return;

		// mejorar la seguridad acá:
		// $where = ['id_colegio' => $id_colegio, 'id_producto' => $, '' => $, ]
		// $item = $this->db->where('pago_id', $pago)->get('compras')->row_array();
		
		//	what TransBank generated from CGI
		$mac_file = BASEPATH ."../application/logs/mac_$pago.txt";
		if ($mac_text = file_get_contents($mac_file))
		{
			log_error('mac text', explode('&', $mac_text));
			$item = [];
			foreach (explode('&', $mac_text) as $qs)
			if (strpos($qs, '='))
			{
				list($key, $value) = explode('=', $qs);
				$item[$key] = $value;				
			}
			// fix date 
			$item['TBK_FECHA_TRANSACCION'] = substr($pago, 0, 4) . substr('0'. $item['TBK_FECHA_TRANSACCION'], -4);
			
			$this->_response($item);
		}
	}
    private function _response($data = NULL, $status = 200)
	{
		try 
		{
			$json_encode_numeric = json_encode($data, JSON_NUMERIC_CHECK);
			$preg_replace_string = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/u', '', $json_encode_numeric);	// Phil's Response treats numbers as strings!!
		}
		catch (Exception $exception)
		{
			$json_encode_numeric = json_encode(array('error' => $exception));
			$preg_replace_string = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/u', '', $json_encode_numeric);
		}
		$this->output
			->set_status_header($status)
			->set_content_type('application/json')
			->set_output($preg_replace_string);
	}
}
