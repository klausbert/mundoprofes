<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datos_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('colegios_v2');
	}
	public function index()
	{
		return FALSE;
	}
	public function show($where)
	{
		extract($where);
		
		if (isset($id))
		{
			$this->load->model('session_model');
			$this->user = $this->session_model->index();
			
			foreach ($this->user['roles'] as $role)
			{
				if ($role['tipo']=='colegios' and $role['id']==$id)
				{
					$data = $this->db->get_where($this->table, "id = $id")->row_array();
					$data['email_admin'] = $this->user['email'];
					
					return $data;
				}
			}
		}
	}
	public function update($values)
	{
		if ($values)
		{
			$where['id'] = $values['id']; unset($values['id']);
			
			$this->db->update($this->table, $this->cleanup($values), $where);
			
			return $this->show($where);
		}
	}
}
