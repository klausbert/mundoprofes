<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datos_admin_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('profesor');
	}
	public function index()
	{
		$role['id'] = $this->session->userdata('id');
		
		$data = $this->db->get_where($this->table, "id = {$role['id']}")->row_array();
		return $data;
	}
	public function show($where)
	{
		return $this->index();
	}
	public function update($values)
	{
		if ($values)
		{
			$where['id'] = $values['id']; unset($values['id']);
			
			$this->db->update($this->table, $this->cleanup($values), $where);
			
			return $this->show($where);
		}
	}
}
