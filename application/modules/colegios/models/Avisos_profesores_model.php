<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos_profesores_model extends MY_Model
{
	public function show($where)
	{
		if (extract($where) and isset($id_aviso, $ids_colegios))
		{
			$head = $this->db->query("
				SELECT A.busqueda
				FROM avisos A
				WHERE A.id = $id_aviso
			")->row_array();
			
			$list = $this->db->query("
				SELECT X.*
				, CONCAT(P.apellidos, ' ', P.apellido_m, ', ', P.nombres) AS postulante
				, P.telefono_movil , C.comuna, '' AS profesion
				, P.cv_experiencia AS experiencia, P.cv
				FROM profesor P
				JOIN avisos_profesores X	ON P.id = X.id_profesor
				JOIN avisos A				ON X.id_aviso = A.id AND X.id_aviso = $id_aviso AND A.id_colegio IN ($ids_colegios)
				JOIN comunas_v2 C			ON P.id_comuna = C.id
			")->result_array();
			$query = $this->db->last_query();
			foreach ($list as &$item)
			{
					// SELECT MIN(profesion) AS profesiones
				$item['profesion'] = $this->db->query("
					SELECT GROUP_CONCAT(profesion SEPARATOR '/') AS profesiones
					FROM profesor_intereses X
					JOIN profesiones F ON X.id_referencia = F.id AND X.id_profesor = {$item['id_profesor']}
				")->row()->profesiones;
			}
			
			return ['head' => $head, 'list' => $list, 'enum' => $this->enum_options(), 'query' => $query, 'where' => $where];
		}
		else
			return ['error' => 'Missing parameters'];
	}
	public function update($set)
	{
		extract($set);
		unset($set['id'], $set['ids_colegios']);
		
		$this->db->update($this->table, $set, "id = $id");
		// $this->db->update($this->table, $set, "id = $id AND id_colegio IN ($ids_colegios)");
		
		return ['update' => $this->db->affected_rows()];
	}
}
