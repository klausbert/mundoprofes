<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos_model extends MY_Model
{
	public function index()
	{
		return [];
	}
	private function _index($where)
	{
		if (extract($where) and isset($ids_colegios, $id_colegio))	// see Show below
			return $this->db->query("
				SELECT A.*
				, DATEDIFF(IFNULL(vigencia, DATE_ADD(CURDATE(), INTERVAL D.duracion DAY)), CURDATE()) AS dias_v
				,(SELECT COUNT(*) FROM avisos_profesores WHERE id_aviso = A.id) AS postulantes
				,(SELECT MIN(region) FROM regiones WHERE id = A.id_region) AS region
				FROM avisos A
                JOIN compras C ON A.id_compra = C.id JOIN productos_detalle D ON C.id_producto = D.id
				WHERE A.id_colegio IN ($ids_colegios) AND A.id_colegio = $id_colegio
				ORDER BY id DESC
			")->result_array();
	}
	public function show($where)	// ojo que ahora (2016-01-10) leo avisos segun las compras
	{
		if (extract($where) and ! isset($id))
			return $this->_index($where);
		
		if ($id > 0)
		{
			if (isset($fields))
				$select = "A.id, $fields";
			else
				$select = "A.*, DATE_ADD(fecha, INTERVAL D.duracion DAY) AS vigencia";
			
			$data = $this->db->query("
				SELECT $select
				FROM avisos A
                JOIN compras C ON A.id_compra = C.id JOIN productos_detalle D ON C.id_producto = D.id
				WHERE A.id_colegio IN ($ids_colegios) AND A.id = $id
			")->row_array();
		}
		else	// si se creó desde Compras, no entra nunca acá
		{
			$data['id']         = 0;
			$data['id_colegio'] = $id_colegio;
			$data['busqueda']   = 'Nuevo Aviso';
			$data['estado']     = '';
			$data['fecha']      = date('Y-m-d');
			$data['vigencia']   = date('Y-m-d');
			$data['contrato_desde'] = date('Y-m-d');
			$data['contrato_hasta'] = date('Y-m-d');
		}
		$data['titulos']      = $this->db->where("id IN (SELECT id_titulo     FROM avisos_titulos      WHERE id_aviso = $id)", NULL, FALSE)->get('titulos')->result_array();
		$data['experiencias'] = $this->db->where("id IN (SELECT id_cargo      FROM avisos_experiencias WHERE id_aviso = $id)", NULL, FALSE)->get('cargos')->result_array();
		$data['asignaturas']  = $this->db->where("id IN (SELECT id_asignatura FROM avisos_asignaturas  WHERE id_aviso = $id)", NULL, FALSE)->get('asignaturas')->result_array();
		$data['niveles']      = $this->db->where("id IN (SELECT id_nivel      FROM avisos_niveles      WHERE id_aviso = $id)", NULL, FALSE)->get('niveles')->result_array();
		
		// tags-input
		$this->load->model('general/funciones_model', 'funciones');
		$data['funciones']    = $this->funciones->show( array('id' => @$data['id_funcion']));
		
		$data['id_colegio'] = 0 + @$data['id_colegio'];
		$data['colegio'] = $this->db->where("id = {$data['id_colegio']}")->select('nombre')->get('colegios_v2')->row_array();
		
		// $data['postulantes'] = $this->postulantes($id);
		
		$data['options'] = $this->enum_options();
		//	override
		$data['options']['estado'] = ['disponible', 'preparado', 'rechazado'];
		
		$data['email'] = $this->db->query("
			SELECT GROUP_CONCAT(email SEPARATOR ', ') AS email
			FROM (
				SELECT email FROM usuarios WHERE tipo = 'admin'
			UNION
				SELECT email FROM usuarios U 
				JOIN usuarios_roles X ON U.id = X.id_usuario 
				JOIN usuarios_roles_colegios Y ON X.id = Y.id_role AND Y.id_colegio = {$data['id_colegio']}
			) G 
		")->row()->email;
		
		return $data;
	}
	public function download($id)	// copied from admin/cvs_model
	{
		$this->load->model('session_model');
		$ids_colegios = $this->session_model->index()['ids_colegios'];	// patch
		
		$result = $this->db->query("
			SELECT U.assets, P.cv
			FROM usuarios U 
			JOIN profesor P ON U.id = P.id
			JOIN avisos_profesores X ON P.id = X.id_profesor
			JOIN avisos A ON X.id_aviso = A.id
			WHERE U.id = $id AND A.id_colegio IN ($ids_colegios)
		")->row_array();
		// echo_pre($this->db->last_query());
		return $result;
	}
	public function postulantes($id)
	{
		$list = $this->db->query("
			SELECT P.*, X.fecha
			FROM profesor P
			JOIN avisos_profesores X ON P.id = X.id_profesor AND X.id_aviso = $id
			ORDER BY X.fecha
		")->result_array();
		
		foreach ($list as &$item)
		{
			$item['profesiones'] = $this->db->query("
				SELECT GROUP_CONCAT(profesion SEPARATOR ', ') AS profesiones
				FROM profesor_intereses X
				JOIN profesiones F ON X.id_referencia = F.id AND X.id_profesor = {$item['id']}
			")->row()->profesiones;
			
			$item['titulos'] = $this->db->query("
				SELECT nivel, titulo
				FROM titulos T
				JOIN profes_formacion_profesional L ON T.id = L.id_titulo_profesional AND L.id_profesor = {$item['id']}
			")->result_array();
			$item['titulos'] += $this->db->query("
				SELECT nivel, titulo
				FROM titulos T
				JOIN profes_formacion_postgrado L ON T.id = L.id_titulo_postgrado AND L.id_profesor = {$item['id']}
			")->result_array();
		}
		return $list;
	}
	public function update($values)
	{
		if (isset($values, $values['id'])
		and in_array($values['estado'], ['disponible', 'preparado', 'rechazado'])) 
		{
			if ($values['fecha'] < date('Y-m-d'))
				$values['fecha'] = date('Y-m-d');
		
			$fecha = date_create($values['fecha']);
			$duracion = $this->db->query("
				SELECT D.duracion
				FROM avisos A
                JOIN compras C ON A.id_compra = C.id JOIN productos_detalle D ON C.id_producto = D.id
				WHERE A.id = {$values['id']}
			")->row_array()['duracion'];
			date_add($fecha, date_interval_create_from_date_string("$duracion days"));
			$values['vigencia'] = date_format($fecha, 'Y-m-d');
			
			// funcion nueva
			if (isset($values['funciones'][0]) and ! isset($values['funciones'][0]['id']))	// add to reference table
			{
				$this->load->model('general/funciones_model', 'funciones');
				$result = $this->funciones->create($values['funciones'][0]);
				$values['id_funcion'] = $result['id'];
			}
			$where['id'] = $values['id']; unset($values['id']);
			$this->db->update($this->table, $this->cleanup($values), $where);
			
			$this->_junction_put( $where['id'], $values['titulos'],      'avisos_titulos',      'id_titulo');
			$this->_junction_put2($where['id'], $values['experiencias'], 'avisos_experiencias', 'id_cargo', 'cargos');
			$this->_junction_put2($where['id'], $values['asignaturas'],  'avisos_asignaturas',  'id_asignatura');
			$this->_junction_put2($where['id'], $values['niveles'],      'avisos_niveles',      'id_nivel');
			
			$where['ids_colegios'] = $values['ids_colegios'];	// patch
			return $this->show($where);
		}
		return ['error' => 'Avisos.update failed', 'values' => $values];
	}
	private function _junction_put($id_aviso, $ids_other, $junction_table, $other_id, $table = FALSE)
	{
		$field = str_replace("id_", "", $other_id);
		if (! $table)
			$table = str_replace("avisos_", "", $junction_table);
		
		foreach ($ids_other as $item) 
		{
			if (! isset($item['id']))	// add to reference table
			{
				if ($this->db->set($field, $item[$field])->insert($table))
					$item['id'] = $this->db->insert_id();
				else
					$this->log_error($table, $this->db->last_query());
			}
			$ids[] = $item['id'];
			$values[] = "($id_aviso, {$item['id']})";
		}
		if (isset($values))
		{
			$this->db->query("INSERT IGNORE INTO $junction_table (id_aviso, $other_id) VALUES ". implode(',', $values));
			$this->db->query("DELETE FROM $junction_table WHERE id_aviso = $id_aviso AND $other_id NOT IN (". implode(',', $ids) .")");
		}
	}
	private function _junction_put2($id_aviso, $ids_other, $junction_table, $other_id)
	{
		$model = str_replace("avisos_", "", $junction_table) ."_model";
		
		foreach ($ids_other as $item) 
		{
			if (! isset($item['id']))
			{
				$this->load->model("general/$model");
				$item = $this->$model->create($item);
			}
			$ids[] = $item['id'];
			$values[] = "($id_aviso, {$item['id']})";
		}
		if (isset($values))
		{
			$this->db->query("INSERT IGNORE INTO $junction_table (id_aviso, $other_id) VALUES ". implode(',', $values));
			$this->db->query("DELETE FROM $junction_table WHERE id_aviso = $id_aviso AND $other_id NOT IN (". implode(',', $ids) .")");
		}
	}
}
