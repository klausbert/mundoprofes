<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Colegios_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('colegios_v2');
	}
	public function index()
	{
		return FALSE;
	}
	public function show($where)
	{
		if (extract($where) and isset($like))
		{
			$this->db->like($like, $where[$like]);
			unset($where['like'], $where[$like]);
		}
		$this->db->where($this->cleanup($where));
		
		return $this->db->select('id, nombre, dependencia, IF(id_usuario IS NULL, 0, 1) AS sostenedor', FALSE)->order_by('nombre')->get($this->table)->result_array();
	}
}
