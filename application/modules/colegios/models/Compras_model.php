<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compras_model extends MY_Model
{
	public function index()
	{
		return FALSE;
	}
	public function show($where)
	{
		$this->load->model('session_model');
		$roles = $this->session_model->index()['roles'];

		extract($where);
		
		if (isset($id_colegio))
		foreach ($roles as $role)
		if ($role['tipo']=='colegios' and $role['id']==$id_colegio)
		{
			$this->db->where("id_colegio = $id_colegio");
			if (isset($id))
				$this->db->where("id = $id");
			
			$list = $this->db->order_by('id', 'DESC')->get_where($this->table, "id_colegio = $id_colegio")->result_array();
			foreach($list as &$item)
			{
				$item['avisos'] = $this->db->get_where('avisos', "id_compra = {$item['id']}")->result_array();
				$item['producto'] = $this->db->query("
					SELECT D.*, P.nombre
					FROM productos_detalle D
					JOIN productos P ON D.id_producto = P.id 
					WHERE D.id = {$item['id_producto']}
				")->row_array();
			}
			
			if (isset($id))
			{
				$list[0]['email'] = $this->db->query("
					SELECT GROUP_CONCAT(email SEPARATOR ', ') AS email
					FROM (
						SELECT email FROM usuarios WHERE tipo = 'admin'
					UNION
						SELECT email FROM usuarios U 
						JOIN usuarios_roles X ON U.id = X.id_usuario 
						JOIN usuarios_roles_colegios Y ON X.id = Y.id_role AND Y.id_colegio = {$item['id_colegio']}
					) G 
				")->row()->email;
				
				return $list[0];	// one
			}
			else
				return $list;		// all
		}
	}
	public function create($values)
	{
		$values['estado'] = 'pendiente';		// forzar
		unset($values['id']);
		
		$this->db->insert($this->table, $this->cleanup($values));
		$values['id'] = $this->db->insert_id();
		
		return $this->show($values);
	}
	//
	// UPLOAD assets (vile copy) deberia ser genérico para cualquier Usuario
	//
	public function upload($values)
	{
		// extract($values);
		// $this->log_error("(not an error)", $values);
		
		if ($size > 2000000)
			return array('error'=>"El archivo pesa más de 2 MB ($size)")
		;
		$data = $this->db->where('id', $id)->get('usuarios')->row_array();
		if (! isset($data['assets']) or $data['assets']=='')
		{
			$data['assets'] = md5($data['creado'] ." ". strrev($id));
			$this->db->set('assets', $data['assets'])->where('id', $id)->update('usuarios');
		}
		$assets_folder = "../z.{$_SERVER['CI_ENV']}/". $data['assets'];
		is_dir($assets_folder) or mkdir($assets_folder, 0777, TRUE);
		move_uploaded_file($tmp_name, "$assets_folder/$name");
		
		// // $this->db->set('cv', $name)->where('id', $id)->update('profesor');	//	reemplaza y ya
		
		// return $values;
		$this->load->model('admin/cvs_model', 'cvs');
		
		return $this->cvs->upload($values, $set_cv = FALSE);
	}
}
