<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios_roles');
	}
	public function index()
	{
		if ($id_usuario = $this->session->userdata('id'))
		{
			//	1. mis colegios (y mi rol en cada uno)
			return $this->db->query("
				SELECT Y.id_colegio, X.role
				, (SELECT apellidos FROM profesor WHERE id = Y.created_by) AS creador
				FROM usuarios_roles X
				JOIN usuarios_roles_colegios Y ON X.id = Y.id_role
				WHERE X.id_usuario = $id_usuario
				ORDER BY Y.id_colegio
			")->result_array();
		}
		return [];
	}
	public function show($where)
	{
		$id_root = $this->session->userdata('id');
		if (! $id_root) $id_root = 0;
		
		if (extract($where) and isset($id_colegio))
		{
			//	1. busco mi colegio
			$list = [];
			if ($id_usuario = $this->session->userdata('id'))
			{
				$list = $this->db->query("
					SELECT Y.id_colegio, X.role
					, (SELECT apellidos FROM profesor WHERE id = Y.created_by) AS creador
					FROM usuarios_roles X
					JOIN usuarios_roles_colegios Y ON X.id = Y.id_role
					WHERE X.id_usuario = $id_usuario AND Y.id_colegio = $id_colegio
				UNION
					SELECT id, 'sostenedor' AS role, '(self)' AS creador
					FROM colegios_v2
					WHERE id_usuario = $id_usuario AND id = $id_colegio
				")->result_array();
				// die("ID_USUARIO $id_usuario ID_COLEGIO $id_colegio ".print_r($list, TRUE).$this->db->last_query());
			}
			if (count($list) > 0 and $item = $list[0])
			{
				//	2. traer para este colegio todos los roles INFERIORES
				$roles = ['supervisor' => 0, 'vendedor' => 1, 'sostenedor' => 2, 'director' => 3, 'gestor' => 4];
				$when = '';
				foreach ($roles as $key => $value)
					$when .= "WHEN '$key' THEN $value ";
				
				return $this->db->query("
					SELECT * FROM (
						SELECT X.id, U.email, P.nombres, P.apellidos, P.RUT, X.role
						, CASE X.role $when END AS role_value
						, IF(X.id_usuario = $id_root, 1, 0) AS propio
						FROM usuarios_roles X
						JOIN usuarios_roles_colegios Y ON X.id = Y.id_role
						JOIN usuarios U ON X.id_usuario = U.id
						JOIN profesor P ON X.id_usuario = P.id
						WHERE Y.id_colegio = $id_colegio
					) Z
					WHERE role_value >= {$roles[$item['role']]}
					ORDER BY role, apellidos
				")->result_array();
			}
			return [];
		}
	}
	public function create($values)
	{
		$this->load->model('session_model');
		$roles = $this->session_model->index()['roles'];

		extract($values);
		// log_error('Colegios y Roles autorizados', $this->session->userdata('roles'));
		// $id vs $ids_colegios
		foreach ($roles as $myRole)
		if ($myRole['id']==$id /*and $myRole['role']=='sostenedor'*/)
		{
			$exists['email'] = $this->db->where('email', $email)->get('usuarios')->row_array();
			if (count($exists['email'])==0)
				return ['error' => 'El email no está en la base de datos']
			;
			$exists['RUT']   = $this->db->where('RUT',     $RUT)->get('profesor')->row_array();
			if (count($exists['RUT'])==0)
				return ['error' => 'El RUT no está en la base de datos']
			;
			if ($exists['email']['id']!==$exists['RUT']['id'])
				return ['error' => 'El email más el RUT no corresponden al mismo usuario']
			;
			// ok veamos si está en u_r, si no lo agregamos
			$where = ['id_usuario' => $exists['email']['id'], 'role' => $role];
			$id_role = $this->db->select_min('id')->where($where)->get($this->table)->row_array()['id'];
			if (! $id_role)
			{
				$this->db->set('created_by', $this->session->userdata('id'));
				$this->db->set($where);
				$this->db->insert($this->table);
				$id_role = $this->db->insert_id();
			}
			// y ahora usuarios_roles_colegios
			$where = ['id_role' => $id_role, 'id_colegio' => $id];
			$id_roleC = $this->db->select_min('id')->where($where)->get($this->table .'_colegios')->row_array()['id'];
			if (! $id_roleC)
			{
				$this->db->set('created_by', $this->session->userdata('id'));
				$this->db->set($where);
				$this->db->insert($this->table .'_colegios');
				$id_roleC = $this->db->insert_id();
			}
			
			
		}
	}
	public function update($values)
	{
		return ['update' => $values];
	}
	public function delete($values)
	{
		$this->db->where('id', $values['id'])->delete('usuarios_roles');
		return $this->db->affected_rows();
	}
}
