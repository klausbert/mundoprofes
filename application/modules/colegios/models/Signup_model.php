<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup_model extends MY_Model
{
	public function create($values)
	{
		extract($values);
		
		if (! isset($rbd, $rut)) return;
		
		$usuario = $this->db->get_where('profesor', "RUT = '$rut'")->row_array();
		$colegio = $this->db->get_where('colegios_v2', "id = $rbd")->row_array();
		
		$this->load->module('mailing');
		// extras
		$data = $values;
		$data['error'] = '';
		
		if (! count($usuario))	// el usuario no existe; lo creamos
		{
			$data['request'] = sha1($email . date('D m y h i s'));
			$data['tipo'] = 'colegio';
			$this->db->insert('usuarios', $this->cleanup($data, 'usuarios'));
			$data['id'] = $this->db->insert_id();
			$data['RUT'] = $rut;
			$this->db->insert('profesor', $this->cleanup($data, 'profesor'));
			
			$data['view'] = 'mailing/colegios_signup';
			$data['usuario'] = $this->mailing->post($data);
			
			$usuario = $this->db->get_where('profesor', "RUT = '$rut'")->row_array();
		}
		$data['email'] = $usuario['email'];
		
		if (count($colegio) and isset($colegio['id_usuario']))
		{
			$data['error'] = ($colegio['id_usuario']==$usuario['id']) 
			? "El RBD $rbd ya estaba asignado a este administrador"
			: "El RBD $rbd tiene otro usuario asignado";
		}
		else
		{
			if (! count($colegio))	// el colegio no existe; lo creamos
			{
				$values['id'] = $rbd;
				$values['nombre'] = $nombre;
				// $values['id_usuario'] = $usuario['id'];
				$this->db->insert('colegios_v2', $values);
				
				$colegio = $this->db->get_where('colegios_v2', "id = $rbd")->row_array();
			}
			if (! isset($colegio['id_usuario']))
			{
				$this->db->where('id', $rbd)->set('id_usuario', $usuario['id'])->update('colegios_v2');
				//	con el id_usuario de arriba, mandarle mail al usuario
				$data['view'] = 'mailing/colegios_assoc';	// avisarle al usuario
				$data['colegio'] = $this->mailing->post($data);
			}
			$data['status'] = 'complete';
		}
		return($data);
	}
}
