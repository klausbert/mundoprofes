<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios');
	}
	public function verify($profile, $provider = 'facebook')
	{
		// adapted from session_model
		extract($profile);
		
		$data = $this->db->select("id, email, tipo, $provider")->where('email', $email)->get($this->table)->row_array();
		
		if (! count($data)) 
		{
			$this->log_error("Inscripción via $provider", $profile);
			
			$values['email'] = $email;
			$values['nombres'] = $first_name;
			$values['apellidos'] = $last_name;
			$values[$provider] = $id;
			
			$this->load->model('profesor/signup_model', 'signup');
			if ($data = $this->signup->create($values))
				$data['just_created'] = TRUE
			;
		} else 
		if (! isset($data[$provider]))	// ya existia con otro proveedor: completo registro
			$this->db->set($provider, $id)->where('id', $data['id'])->update($this->table)
		;
		if (isset($data['error']))
			return $data
		;
		if (isset($data['bloqueado']))	// devuelvo poca información x las dudas
			return ['error' => $data]
		;
		else
		{
			$this->session->set_userdata('id', $data['id']);
			$this->load->model('session_model');
			$result = $this->session_model->index();
			$result['just_created'] = @$data['just_created'];
			return $result;
		} 
	}
}
