<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Linkedin extends REST_Controller_v3
{
	public function __construct()
	{
		parent::__construct('Auth_model');
	}
	protected function index_get()
	{
		$this->status(405);
	}
	protected function index_post()
	{
		$accessTokenUrl = 'https://www.linkedin.com/oauth/v2/accessToken';
		$peopleApiUrl = 'https://api.linkedin.com/v2/me';
		$emailApiUrl = 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))';
		
		$params['grant_type'] = 'authorization_code';
		$params['code'] = $this->post('code');
		$params['redirect_uri'] = $this->post('redirectUri');
		$params['client_id'] = $this->post('clientId');
		// $params['client_secret'] = 'QSGXk5wIQEUG20ob';
		$params['client_secret'] = 'wV0y5N9ggYmn3EAA';
		
		$access = $this->getJSON($accessTokenUrl, $params);
		$header = ["Authorization: Bearer {$access['access_token']}"];

		if (isset($access['error']))
			$this->response($access);
		else
		{			
			$this->log_error('Access was OK, getting Profile', $access);
			$profile1 = $this->getJSON2($peopleApiUrl, $header);
			$profile2 = $this->getJSON2($emailApiUrl, $header);
			
			if (isset($profile1['error']))
				$this->response($profile1);
			else 
			if (isset($profile2['error']))
				$this->response($profile2);
			else 
			{
				// 1. ver si está en las tablas (o dar de alta)
				$profile['email']      = $profile2['elements'][0]['handle~']['emailAddress'];
				$profile['first_name'] = $profile1['localizedFirstName'];
				$profile['last_name']  = $profile1['localizedLastName'];
				$this->log_error('Profile is', $profile);
				
				$result = $this->model->verify($profile, 'linkedin');
				if (isset($result['just_created']))
				{
					$this->load->module('mailing');
					$data['email'] = $profile['email'];
					$data['view'] = 'mailing/linkedin_signup';
					$result['just_created'] = $this->mailing->post($data);
				}
				// 2. y cómo sigo ahora que estoy autenticado?
				if (! isset($result['error']))
				{
					$this->response($result);					
				}
			}
		}
	}
	protected function index_put()
	{
		$this->status(405);
	}
	protected function index_delete()
	{
		$this->status(405);
	}
}