<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Google extends REST_Controller_v3
{
	public function __construct()
	{
		parent::__construct('Auth_model');
	}
	protected function index_get()
	{
		$this->status(405);
	}
	protected function index_post()
	{
		$accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
		// $peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
		$peopleApiUrl = 'https://openidconnect.googleapis.com/v1/userinfo';
		
		$params['code'] = $this->post('code');
		$params['client_id'] = $this->post('clientId');
		$params['client_secret'] = 'cVL9sbyrPFHnKVEUUeV-nEc1';
		$params['redirect_uri'] = $this->post('redirectUri');
		$params['grant_type'] = 'authorization_code';
		
		$access = $this->postJSON($accessTokenUrl, $params);
		
		if (isset($access['error']))
		{
			$this->log_error('Access failed with params', $params);
			$this->response($access);
		}
		else
		{			
			$this->log_error('Access was ok, getting Profile', $access);
			$profile = $this->getJSON($peopleApiUrl, array(), 'Authorization: Bearer '. $access['access_token']);	// ATTN !!
			log_error('New People API', ['peopleApiUrl' => $peopleApiUrl, 'profile' => $profile]);
			if (isset($profile['error']))
				$this->response($profile);
			else 
			{
				$this->log_error('Profile:', $profile);
				// 1. ver si está en las tablas (o dar de alta)
				$profile['id']         = $profile['sub'];
				$profile['first_name'] = $profile['given_name'];
				$profile['last_name']  = $profile['family_name'];

				$result = $this->model->verify($profile, 'google');
				if (isset($result['just_created']))
				{
					$this->load->module('mailing');
					$data['email'] = $profile['email'];
					$data['view'] = 'mailing/google_signup';
					$result['just_created'] = $this->mailing->post($data);
				}
				// 2. y cómo sigo ahora que estoy autenticado?
				if (! isset($result['error']))
				{
					$this->response($result);
				}
			}
		}
	}
	protected function index_put()
	{
		$this->status(405);
	}
	protected function index_delete()
	{
		$this->status(405);
	}
}