<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook extends REST_Controller_v3
{
	public function __construct()
	{
		parent::__construct('Auth_model');
	}
	protected function index_get()
	{
		$this->status(405);
	}
	protected function index_post()
	{
		$accessTokenUrl = 'https://graph.facebook.com/v3.3/oauth/access_token';
		$graphApiUrl = 'https://graph.facebook.com/v3.3/me';
		
		$params['code'] = $this->post('code');
		$params['client_id'] = $this->post('clientId');
		$params['client_secret'] = '3c8e784812d7481cdd288648c291b597';
		$params['redirect_uri'] = $this->post('redirectUri');
		
		$access = $this->getJSON($accessTokenUrl, $params);
		
		if (isset($access['error']))
		{
			$this->log_error('Access failed with params', $params);
			$this->response($access);
		}
		else
		{
			$access['fields'] = 'id,email,first_name,last_name,link,name';
			
			// https://developers.facebook.com/docs/graph-api/securing-requests
			$access['appsecret_proof'] = hash_hmac('sha256', $access['access_token'], $params['client_secret']); 
			
			$this->log_error('Access was ok, getting Profile', $access);
			$profile = $this->getJSON($graphApiUrl, $access);
			if (isset($profile['error']))
				$this->response($profile['error']);
			else
			{				
				$profile['picture'] = 'https://graph.facebook.com/v3.3/'. $profile['id'] .'/picture?type=large';
				// 1. ver si está en las tablas (o dar de alta)
				$result = $this->model->verify($profile);
				if (isset($result['just_created']))
				{
					$this->load->module('mailing');
					$data['email'] = $profile['email'];
					$data['view'] = 'mailing/facebook_signup';
					$result['just_created'] = $this->mailing->post($data);
				}
				// 2. y cómo sigo ahora que estoy autenticado?
				if (! isset($result['error']))
				{
					$this->response($result);					
				}
			}
		}
	}
	protected function index_put()
	{
		$this->status(405);
	}
	protected function index_delete()
	{
		$this->status(405);
	}
}