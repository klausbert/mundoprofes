<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form_express_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('profesor/alta_express_model', 'alta_express');
		$this->load->model('profesor/signup_model', 'signup');
	}
	public function hello()
	{
		return ['general' => 'Newman'];
	}
	public function index()
	{
		return $this->show(['id' => $this->session->userdata('id')]);
	}
	public function show($where)
	{
		return $this->alta_express->show($where);
	}
	public function create($values)
	{	
		$result = $this->signup->create($values);
		
		foreach (['id', 'temp_password'] as $key)
			$values[$key] = $result[$key];
		
		$this->alta_express->update($values);
		
		return $result;
	}
	public function upload($values)
	{
		return $this->alta_express->create($values);
	}
}
