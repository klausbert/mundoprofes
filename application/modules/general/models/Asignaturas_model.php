<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Referencias_model.php');

class Asignaturas_model extends Referencias_model
{
	public function __construct()
	{
		parent::__construct('asignaturas', 'asignatura');
	}
}
