<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profesiones_model extends MY_Model
{
	public function show($values)
	{
		if (isset($values['profesion'])) 
			$this->db->like('profesion', $values['profesion']);
		else 
			$this->db->where($values);
		
		return $this->db->get($this->table)->result_array();
	}
}
