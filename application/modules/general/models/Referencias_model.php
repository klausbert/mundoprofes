<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referencias_model extends MY_Model
{
	protected $categoria  = '';
	protected $referencia = '';
	
	public function __construct($cat, $ref)
	{
		parent::__construct('_referencias');
		
		$this->categoria  = $cat;
		$this->referencia = $ref;
	}
	public function index()
	{
		// $this->db->select("id, referencia AS $this->referencia", FALSE)
		// 	->where('categoria', $this->categoria)->order_by('referencia');
		$this->db->select("MIN(id) AS id, referencia AS $this->referencia", FALSE)
			->where('categoria', $this->categoria)
			->group_by('referencia')->order_by('referencia');
		
		return $this->db->get($this->table)->result_array();
	}
	public function show($where)
	{
		if (isset($where[$this->referencia])) 
			$this->db->like('referencia', $where[$this->referencia]);
		else if (isset($where)) 
			$this->db->where($where);
		
		return $this->index();
	}
	public function create($values)
	{
		$set['categoria'] = $this->categoria;
		$set['referencia'] = $values[$this->referencia];
		$set['created_by'] =  $this->session->userdata('id');
		return parent::create($set);
	}
}
