<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('productos');
	}
	public function index()
	{
		$result = parent::index();
		foreach ($result as &$item)
			$item['detl'] = $this->db->get_where('productos_detalle', "id_producto = {$item['id']}")->result_array()
		;
		return $result;
	}
	public function show($where)
	{
		return $this->db->get_where('productos_detalle', "id = {$where['id']}")->row_array();
	}
}
