<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wp_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('wp_posts');
	}
	public function index()
	{
		return FALSE;
	}
	public function show($where)
	{
		$this->db->select('post_title AS title, post_content AS content', FALSE);
		
		if (extract($where) and isset($name)) 
		return parent::show(['post_name' => $name])[0];
	}
}
