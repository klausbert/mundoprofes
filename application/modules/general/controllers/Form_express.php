<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form_express extends REST_Controller_v3
{
	protected function hello_get()
	{
		$this->response($this->model->hello());
	}
	protected function index_get()
	{
		$this->_get_args['id'] = 0;
		
		parent::index_get();
	}
	protected function cv_post()
	{
		//	taken from profesor/alta_express
		if (isset($_FILES['file']))
			$this->_post_args = array_merge($this->_post_args, $_FILES['file']);
		
		$item = $this->model->upload($this->post());
	}
	protected function index_post()
	{
		//	taken from profesor/signup
		if ($this->post()) 
		{
			$status = 403;
				$item = $this->model->create($this->post());	// parece llamar a profesor/Alta_express...
			
			if (! isset($item['error']))	// Send email, requesting confirmation (passwords)
			{
				$status = 201;
				if (isset($item['request']))
				{
					$item['post'] = 'signup';
					$item['temp'] = $this->session->userdata('temp_password');
					$this->load->module('mailing');
					$item['result'] = $this->mailing->post($item + ['debug' => print_r($item, TRUE)]);
				}
			}
			else
				$this->log_error('Signup error', $item)	// what's causing the empty records?
			;
			$this->response($item, $status);
		}
	}
	protected function index_put()
	{
		$this->status(405);
	}
	protected function index_delete()
	{
		$this->status(405);
	}
}
