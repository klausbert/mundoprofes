<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos extends REST_Controller_v3
{
	public function __construct()
	{
		parent::__construct('profesor/avisos_model');
	}
	protected function index_put()
	{
		$this->status(405);
	}
	protected function index_post()
	{
		$this->status(405);
	}
	protected function index_delete()
	{
		$this->status(405);
	}
	//
	//
	//
	protected function rss_get()
	{
		$this->output
		->set_status_header(200)
		->set_content_type('application/rss+xml');
		
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo '<rss version="2.0">';
		echo '<channel>';
		echo '<title>Avisos MundoProfes</title>';
		echo '<description>Ofertas de Empleo</description>';
		echo '<link>'. base_url('/#/avisos') .'</link>';
		echo '<lastBuildDate>'. date(DATE_RSS) .'</lastBuildDate>';
		echo '<pubDate>'. date(DATE_RSS) .'</pubDate>';
		echo '<ttl>1800</ttl>';
		// iterar aqui
		$data = $this->model->index();
		foreach ($data['list'] as $item)
		{
			echo '<item>';
			echo "<title>{$item['busqueda']}</title>";
			echo "<description>{$item['descripcion']}</description>";
			echo '<link>'. base_url('/#/aviso/'.$item['id']) .'</link>';
			echo '<guid isPermaLink="false">'. base_url('/#/aviso/'.$item['id']) .'</guid>';
			echo '<pubDate>'. date(DATE_RSS) .'</pubDate>';	// $item['fecha']
			echo '</item>';
		}
		// fin
		echo '</channel>';
		echo '</rss>';
	}
}
