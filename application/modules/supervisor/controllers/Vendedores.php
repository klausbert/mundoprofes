<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Vendedores extends Base_Controller
{
	protected function index_post()
	{
		if ($this->post() and $result = $this->model->create($this->post())) 
			$this->response($result, isset($result['error']) ? 400 : 201)
		;
	}
	protected function index_delete()
	{
		if ($this->delete() and $result = $this->model->delete($this->delete())) 
			$this->response($result, isset($result['error']) ? 404 : 200);
		else
			$this->response(['params' => $this->delete(), 'result' => $result]);
	}
}
