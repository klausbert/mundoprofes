<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Colegios_model extends MY_Model
{
	public $my_ids;
	
	public function __construct()
	{
		parent::__construct('usuarios_roles_colegios');
		
		if (! $this->my_ids()) exit('No eres supervisor!');
	}
	private function my_ids()
	{
		if (! $this->my_ids) 
		$this->my_ids = $this->db->query("
			SELECT GROUP_CONCAT(Y.id_colegio SEPARATOR ', ') AS ids
			FROM usuarios_roles_colegios Y 
			JOIN usuarios_roles X ON Y.id_role = X.id
			WHERE X.id_usuario = {$this->session->userdata('id')} AND role = 'supervisor'
		")->row()->ids;
		
		return $this->my_ids;
	}
	public function index($where = FALSE)
	{
		if (! $this->my_ids()) return ['error' => 'No eres supervisor!'];
		
		$list = $this->db->query("
			SELECT C.id, C.nombre, C.comuna, C.dependencia, Y.id_role
			FROM colegios C
			LEFT JOIN (
				usuarios_roles_colegios Y
				JOIN usuarios_roles X ON X.id = Y.id_role AND X.role IN ('vendedor')
			) ON Y.id_colegio = C.id
			WHERE C.id IN ($this->my_ids)
		")->result_array();
		
		return $list;
	}
	public function show($where)
	{
		return FALSE;
	}
	public function create($values)
	{
		if (extract($values) and isset($id_role, $id_colegio)
		and ! $this->db->where($values)->count_all_results($this->table))
		{
			$this->db->insert($this->table, $values);
			$values['id'] = $this->db->insert_id();
			
			log_error('Inserted rows: '. $this->db->affected_rows(), $this->db->last_query());
			
			return $values;
		}
	}
	public function update($values)
	{
		return ['update' => $values];
	}
	public function delete($values)
	{
		if (extract($values) and isset($id_role, $id_colegio) 
		and in_array($id_colegio, explode(',', $this->my_ids())))
			$this->db->delete($this->table, $values);
	}
}
