<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends MY_Model
{
	public function index()
	{
		return false;
	}
	public function show($where)
	{
		//	filter out all guys who are already selling (id_usuario + role:'vendedor' in db)
		$ids = $this->db->select("GROUP_CONCAT(id_usuario SEPARATOR ',') AS ids", FALSE)->where('role', 'vendedor')->get('usuarios_roles')->row()->ids;
		
		if (extract($where) and isset($like))
		{
			$this->db->like($like, $where[$like]);
			unset($where['like'], $where[$like]);
		}
		$this->db->where($this->cleanup($where));
		$this->db->where("id NOT IN ($ids)");
		
		$list = parent::index();
		foreach ($list as &$item)
		{
			unset($item['password']);
			$item += $this->db->select('RUT, nombres, apellidos')->get_where('profesor', "id = {$item['id']}")->row_array();
		}
		return $list;
	}
}
