SELECT U.id, email, IF (password IS NULL, NULL, 'si') AS `password`, facebook, google, linkedin, tipo, creado, bloqueado, RUT, nombres, apellidos, apellido_m, sexo, estado_civil, fecha_nacimiento, domicilio, (SELECT comuna FROM comunas_v2 WHERE id = id_comuna) AS comuna, telefono_fijo, telefono_movil, email_alt, tiene_empleo, skype, disponibilidad, disponibilidad_desde, disponibilidad_hasta, zonas_geograficas, valor_hora_desde, sueldo_mensual_desde, cantidad_horas, clases_particulares, clases_particulares_disponibilidad, reemplazos, reemplazos_disponibilidad, trabajaria_en_ATE, cv, cv_postgrado, cv_experiencia, updated_at
FROM usuarios U
JOIN profesor P ON U.id = P.id
WHERE email > '' AND tipo = 'profesor'
