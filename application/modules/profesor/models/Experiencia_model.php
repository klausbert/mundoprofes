<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Experiencia_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('profesor');
	}
	public function index()
	{
		return $this->enum_options()['cv_experiencia'];
	}
	public function show($where)
	{
		return $this->enum_options()['cv_experiencia'];
	}
}
