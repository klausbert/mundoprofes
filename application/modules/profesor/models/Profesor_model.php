<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profesor_model extends MY_Model
{
	public function index()
	{
		return FALSE;
	}
	public function show($where)
	{
		// if (isset($where['RUT']))
			// return parent::show($where);	// colegios/signup.js
		
		if (isset($where['email']))
			return array_intersect_key(	//	TODO: fix EMPTY case
				$this->db->where('email', $where['email'])->get('usuarios')->row_array(), 
				['id' => '', 'tipo' => '']
			);
			
		if (isset($where['rut']))
			return array_intersect_key(
				$this->db->where('rut', $where['rut'])->get('profesor')->row_array(), 
				['id' => '', 'email' => '']
			);
	}
}
