<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Formacion_profesional_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	
		$this->table = 'profes_formacion_profesional';
	}
	public function index()
	{
		return FALSE;	//	deny FULL DUMP for CHILD table
	}
	public function show($where)
	{
		$list = $this->db->select("
			T.*
			, (SELECT tipo        FROM instituciones WHERE id = T.id_tipo_institucion) tipo
			, (SELECT institucion FROM instituciones WHERE id = T.id_tipo_institucion) institucion
			, (SELECT carrera     FROM titulos_profesionales WHERE id = T.id_titulo_profesional) carrera
		", FALSE)
		->order_by('anno')
		->get_where("{$this->table} T", $where)->result_array();
		
		// $this->log_error('SQL', $this->db->last_query());
		
		return $list;
	}
	public function create($post)
	{
		//	typeahead-editable="true"
		$this->load->model('instituciones_model', 'instituciones');
		if (! isset($post['id_tipo_institucion'])
		/* or ($test = $this->instituciones->show( array('id'=> $post['id_tipo_institucion']))
		and $test[0]['institucion'] <> $post['institucion'])*/ ) 
		{
			$make = $this->instituciones->create( array('tipo'=> $post['tipo'], 'institucion'=> $post['institucion']));
			$post['id_tipo_institucion'] = $make['id'];
		}
		
		$this->load->model('titulos_profesionales_model', 'titulos_profesionales');
		if (! isset($post['id_titulo_profesional'])
		/* or ($test = $this->titulos_profesionales->show( array('id'=> $post['id_titulo_profesional']))
		and $test[0]['carrera'] <> $post['carrera'])*/ ) 
		{
			$make = $this->titulos_profesionales->create( array('tipo'=> $post['tipo'], 'carrera'=> $post['carrera']));
			$post['id_titulo_profesional'] = $make['id'];
		}
		
		return parent::create( $this->cleanup($post));
	}
}
