<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alta_express_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('profesor');
	}
	public function hello()
	{
		return ['profesor' => 'Newman'];
	}
	public function index()
	{
		return $this->show(['id' => $this->session->userdata('id')]);
	}
	public function show($where)
	{
		if (extract($where) and isset($id) and $id > 0)
		{
			if ($data = $this->db->get_where($this->table, "id = $id")->row_array()
			and count($data)==0)
				$this->db->insert($this->table, ['id' => $id]);	// agrego Profesor a partir de Id no-validado (Usuario?)
			
			if ($data = $this->db->get_where($this->table, "id = $id")->row_array())
			{
				$data['email'] = $this->db->get_where('usuarios', "id = $id")->row()->email;
				
				$this->load->model('comunas_model', 'comunas');
				$data['comunas'] = $this->comunas->show( array('id' => $data['id_comuna']));
				
				$set = array('id_profesor' => $id, 'categoria' => 'profesiones');
				$data['profesiones'] = $this->db->where($set)->from('profesor_intereses X')->join('profesiones P', 'X.id_referencia = P.id')
				->get()->result_array();
				
				
				$data['formacion']['profesional'] = array();
				$this->load->model('profesor/formacion_profesional_model', 'profesional');
				$data['formacion']['profesional'] = $this->profesional->show( array('id_profesor' => $id));
				
				$data['formacion']['postgrado'] = array();
				$this->load->model('profesor/formacion_postgrado_model', 'postgrado');
				$data['formacion']['postgrado'] = $this->postgrado->show( array('id_profesor' => $id));
				
				$data['request'] = $this->db->select_max('request')->where('id', $id)->get('usuarios_a_confirmar')->row()->request;
			}
		}
		$data['options'] = $this->enum_options();			
		// $data['sql'] = $this->db->last_query();
		return $data;
	}
	public function create($values)	// POST for files, PUT for data
	{
		$this->load->model('admin/cvs_model', 'cvs');
		
		return $this->cvs->upload($values);
	}
	public function update($values)
	{
		extract($values);
		
		if (isset($RUT)
		and $dupe = $this->db->where("RUT = '$RUT' AND id <> $id")->get($this->table)->row_array())
		{
			$result['error'][] = ['item' => 'RUT', 'message' => "El RUT $RUT ya existe en nuestra base para otra cuenta de usuario"];
		}
		else
		{
			if (isset($email)) // cambió el email
				$this->db->set('email', $email)->where('id', $id)->update('usuarios');
			
			$values['id_comuna'] = @$comunas[0]['id'];
			$values += ['updated_at' => NULL, 'updated_by' => $this->session->userdata('id')];	// log
			
			//	profesiones
			// $result['profesiones_hmvc'] = Modules::run('profesor/profesiones/index_put', $values);	// requires PUBLIC function, not Protected
			
			$set = ['id_profesor' => $id, 'categoria' => 'profesiones'];
			
			$old = [];
			foreach ($this->db->where($set)->get('profesor_intereses')->result_array() as $item)
				$old[] = $item['id_referencia'];
							
			$new = [];
			if (isset($profesiones))
			foreach ($profesiones as $profesion) 
			{
				if (! isset($profesion['id'])
				and $this->db->set('referencia', $profesion['profesion'])->set('categoria', 'profesiones')->insert('_referencias'))
					$profesion['id'] = $this->db->insert_id();
					// es nueva...
				
				if (! isset($profesion['id']))
					continue;
				// else
				$new[] = $profesion['id'];
				if (! in_array($profesion['id'], $old))
					$this->db->set('id_referencia', $profesion['id'])->insert('profesor_intereses', $set);
			}
			if (count($new))
			$this->db->where_not_in('id_referencia', $new)->delete('profesor_intereses', $set);
			
			$result = parent::update($values);
		}
		return $result;
	}
}
