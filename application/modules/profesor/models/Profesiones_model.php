<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profesiones_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('profesor_intereses');
	}
	public function show($where)
	{
		$where['categoria'] = 'profesiones';
		return parent::show($where);
	}
	// public function create($values)
	// {
		// if (extract($values) and isset($profesion))
		// {
			// $this->db->insert('_referencias', ['referencia' => $profesion, 'categoria' => 'profesiones']);
			// // return ['values' => $values, 'query' => $this->db->last_query()];
			
			// $id = $this->db->insert_id();
			// return $this->db->get_where('profesiones', "id = $id")->row_array();
		// }
	// }
}
