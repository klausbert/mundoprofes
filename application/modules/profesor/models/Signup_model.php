<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios');
	}
	public function create($post)	// called also from: facebook_model
	{
		extract($post);
		
		if (! isset($email)) 
			return array('error' => 'El email no puede estar vacío')
		;
		if (! filter_var($email, FILTER_VALIDATE_EMAIL)) 
			return array('error' => "'$email' no es un email válido")
		;
		if ($user = $this->db->where('email', $email)->get($this->table)->row_array()) 
		{
			if ($user['precargado'])
				return $user;	// pretend it's new
			
			$post = $user;
		} 
		//	coming up:
		if (! isset($post['tipo'])) 
			$post['tipo'] = 'profesor';
		
		$post['request'] = sha1($email . date('Y-m-d h:i:s'));
		
		if (! isset($post['facebook'], $post['google'], $post['linkedin']))	// Alta Express v2 requires TEMP session
		{
			$temp_password = strrev(substr($post['request'].$post['request'], 17, 13));	// send via email - or simply forget
			$post['password'] = sha1($temp_password);
		}
		
		if (! isset($post['id']))
		{
			$this->db->insert($this->table, $this->cleanup($post));
			$post['id'] = $this->db->insert_id();
			
			if (isset($temp_password))
			{
				$this->load->model('Session_model', 'temp_session');
				$temp_session = $this->temp_session->create(['email' => $email, 'password' => $temp_password]);
				$temp_session['temp_password'] = $temp_password;
				log_error('TEMP Session', $temp_session);
			}
			if (! isset($post['facebook'], $post['google'], $post['linkedin'], $post['precargado']))
				$this->db->insert('usuarios_a_confirmar', $this->cleanup($post, 'usuarios_a_confirmar'))	// TBD: remover registro en Reset y quitar ésto
				// hasta aqui OK
			;
			if ($post['tipo']=='profesor') 
				$this->db->insert('profesor', $this->cleanup($post, 'profesor'))	// id, email, nombres, apellidos
			;
		}
		return $this->db->query("
			SELECT U.*, A.request, '$temp_password' AS temp_password
			FROM usuarios U
			LEFT JOIN usuarios_a_confirmar A ON U.id = A.id 
			WHERE U.id = {$post['id']}
		")->row_array() + ['signin' => TRUE, 'password' => ''];
	}
}
