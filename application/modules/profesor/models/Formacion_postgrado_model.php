<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Formacion_postgrado_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('profes_formacion_postgrado');
	}
	public function index()
	{
		return FALSE;	//	deny FULL DUMP for CHILD table
	}
	public function show($where)
	{
		return $this->db->select("
			T.*
			, (SELECT tipo        FROM instituciones WHERE id = T.id_tipo_institucion) tipo
			, (SELECT institucion FROM instituciones WHERE id = T.id_tipo_institucion) institucion
			, (SELECT postgrado   FROM titulos_postgrado WHERE id = T.id_titulo_postgrado) postgrado
		", FALSE)
		->order_by('anno')
		->get_where("{$this->table} T", $where)->result_array();
	}
	public function create($post)
	{
		//	typeahead-editable="true"
		$this->load->model('instituciones_model', 'instituciones');
		if (! isset($post['id_tipo_institucion'])
		/* or ($test = $this->instituciones->show( array('id'=> $post['id_tipo_institucion']))
		and $test[0]['institucion'] <> $post['institucion'])*/ ) 
		{
			$make = $this->instituciones->create( array('tipo'=> $post['tipo'], 'institucion'=> $post['institucion']));
			$post['id_tipo_institucion'] = $make['id'];
		}
		
		$this->load->model('titulos_postgrado_model', 'titulos_postgrado');
		if (! isset($post['id_titulo_postgrado'])
		/* or ($test = $this->titulos_postgrado->show( array('id'=> $post['id_titulo_postgrado']))
		and $test[0]['postgrado'] <> $post['postgrado'])*/ ) 
		{
			$make = $this->titulos_postgrado->create( array('tipo'=> $post['tipo'], 'postgrado'=> $post['postgrado']));
			$post['id_titulo_postgrado'] = $make['id'];
		}
		
		return parent::create( $this->cleanup($post));
	}
}
