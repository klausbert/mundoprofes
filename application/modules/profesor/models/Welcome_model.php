<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios_a_confirmar');
	}
	public function show($where)
	{
		if (extract($where) and  isset($request))
		{
			$id = $this->db->select_max('id')->where('request', $request)->get($this->table)->row()->id;
			log_error("Id from request $request", $id);
			if (isset($id) and $id)
			{
				$email = $this->db->select_max('email')->where('id', $id)->get('usuarios')->row()->email;
				log_error("Email from Id $id", $email);
				
				$post_id  = 146;
				// copied from Mailing
				$uri = site_url("general/wp?name=bienvenido");
				$ctx = stream_context_create(['http' => [
					'method' => 'GET',
					// 'header' => "Authorization: Basic ". base64_encode('mp:mirto130'),
					'ignore_errors' => TRUE,
				]]);
				$js = @file_get_contents($uri, FALSE, $ctx);
				$wp_view  = json_decode( preg_replace("/\\\u([0-9a-f]{4})/", "&#x\\1;", $js), TRUE);
				
				log_error("WP_view from Email $email", $wp_view);
				
				// if ($wp_view['status']!=='ok')
					// return ['error' => $wp_view['status']];
				
				// $wp_view['post']['content'] = html_entity_decode($wp_view['post']['content']);
				// $wp_view['post']['content'] = str_replace('{{email}}', $email, $wp_view['post']['content']);				
				$wp_view['post']['content'] = html_entity_decode($wp_view['content']);
				$wp_view['post']['content'] = str_replace('{{email}}', $email, $wp_view['content']);
				
				return $wp_view;
			}
		}
	}
}
