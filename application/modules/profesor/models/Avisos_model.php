<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos_model extends MY_Model
{
	public function index()
	{
		$where['id_profesor'] = 0;
		return $this->show($where);
	}
	public function show($where)
	{
		extract($where);
		
		if (! isset($id_profesor))
			$id_profesor = 0
		;
		if (! isset($id))
		{
			$data['list'] = $this->db->query("
				SELECT *
				, DATEDIFF(IFNULL(vigencia, DATE_ADD(CURDATE(), INTERVAL 15 DAY)), CURDATE()) AS dias_v
				,(SELECT COUNT(*) FROM avisos_profesores WHERE id_aviso = avisos.id AND id_profesor = $id_profesor) AS postulantes
				,(SELECT MIN(region) FROM regiones    WHERE id = avisos.id_region) AS region
				,(SELECT MIN(comuna) FROM comunas_v2  WHERE id = avisos.id_comuna) AS comuna
				, IF(confidencial = 1, '(confidencial)', (
					SELECT nombre FROM colegios_v2 WHERE id = avisos.id_colegio
				)) AS institucion
				FROM avisos
				WHERE IFNULL(vigencia, CURDATE()) >= CURDATE() AND estado = 'verificado'
			")->result_array();
		}
		else
		if ($id > 0)
		{
			$data['item'] = $this->_get_one($id, $id_profesor);	// UN aviso
			$data['options'] = $this->enum_options();
		}
		else
			//	falta poder ver todos los avisos donde postulé !!
		{
			$data['error'] = $where;
		}
		return $data;
	}
	private function _get_one($id, $id_profesor)
	{		
		$data = $this->db->where("id = $id")->get('avisos')->row_array();	
		$data['titulos']      = $this->db->where("id IN (SELECT id_titulo     FROM avisos_titulos      WHERE id_aviso = $id)", NULL, FALSE)->get('titulos')->result_array();
		$data['experiencias'] = $this->db->where("id IN (SELECT id_cargo      FROM avisos_experiencias WHERE id_aviso = $id)", NULL, FALSE)->get('cargos')->result_array();
		$data['asignaturas']  = $this->db->where("id IN (SELECT id_asignatura FROM avisos_asignaturas  WHERE id_aviso = $id)", NULL, FALSE)->get('asignaturas')->result_array();
		$data['niveles']      = $this->db->where("id IN (SELECT id_nivel      FROM avisos_niveles      WHERE id_aviso = $id)", NULL, FALSE)->get('niveles')->result_array();
		$data['comunas']      = $this->db->where("id", $data['id_comuna'])->get('comunas_v2')->result_array();
		$data['funciones']    = $this->db->where("id", $data['id_funcion'])->get('cargos')->result_array();
				
		$data['profesor'] = $this->db->query("
			SELECT P.cv, IFNULL(A.id, 0) AS postulado
			FROM profesor P
			LEFT JOIN avisos_profesores A ON P.id = A.id_profesor AND A.id_aviso = $id
			WHERE P.id = $id_profesor
		")->row_array();
		//	TODO: fix for empty case
		return $data;
	}
	public function create($values)
	{
		extract($values);
		if (isset($id_aviso, $id_profesor))
			$this->db->insert('avisos_profesores', $values)
		;
		return array('id'=> $this->db->insert_id());
	}
	public function update($values)
	{
		return FALSE;
	}
	public function delete($values)
	{
		extract($values);
		if (isset($id_aviso, $id_profesor))
			$this->db->delete('avisos_profesores', $values)
		;
		return array('success'=> $this->db->affected_rows());
	}
}
