<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Alta_express extends Base_Controller
{
	protected function index_post()
	{
		$this->_post_args = array_merge($this->_post_args, $_FILES['file']);
		parent::index_post();
	}
	protected function index_put()
	{
		$item = $this->put();
		parent::index_put();
		
		if (isset($item['request']))
		{
			$item['post'] = 'signup';
			$this->load->module('mailing');
			$item['result'] = $this->mailing->post($item);
		}
		return $item;
	}
}
