<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends REST_Controller_v3
{
	protected function index_get()
	{
		$this->status(405);
	}
	protected function index_post()
	{
		if ($this->post()) 
		{
			$status = 403;
			$data = $this->model->create($this->post());
			
			if (! isset($data['error']))	// Send email, requesting confirmation (passwords)
			{
				$status = 201;
			}
			else
				$this->log_error('Signup error', $data)	// what's causing the empty records?
			;
			$this->response($data, $status);
		}
	}
	protected function index_put()
	{
		$this->status(405);
	}
	protected function index_delete()
	{
		$this->status(405);
	}
}
