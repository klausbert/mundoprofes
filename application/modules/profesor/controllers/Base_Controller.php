<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends REST_Controller_v3
{
	protected $user;
	
	public function __construct($model = FALSE)
	{
		parent::__construct($model);
		
		if (! $this->session->userdata('id'))
			$this->abort_status = 401;
		else
		{
			$this->load->model('session_model');
			$this->user = $this->session_model->index();
			
			$this->abort_status = 403;
			if ($this->user['tipo']=='admin' or $this->user['tipo']=='profesor')
				$this->abort_status = 0;
			else
			foreach ($this->user['roles'] as $role)
			if ($role['role']=='profesor')
				$this->abort_status = 0;
		}
		if ($this->abort_status)
			exit($this->abort_status);
	}
	public function __destruct()
	{
		if ($this->abort_status)
			$this->response(NULL, $this->abort_status);
		
		parent::__destruct();
	}
	private function _identity($merge)
	{
		//	si es un Profesor, le fuerzo la ID para que no ponga otra.
		if ($this->user['tipo']=='profesor')
		{
			$identity = in_array(get_class($this), ['Alta_express']) ? 'id' : 'id_profesor';
			$merge[$identity] = $this->session->userdata('id');
		}
		//	si es ADMIN, uso la ID que venga.
		
		// if ($this->session->userdata('tipo')!=='admin' or ! isset($merge[$identity]))
			// $merge = array_merge($merge, [$identity => ((! $this->session->userdata('id'))? 0 : $this->session->userdata('id'))]);
		
		return $merge;
	}
	protected function index_get()
	{
		$this->_get_args = $this->_identity($this->_get_args);
		
		parent::index_get();
	}
	protected function index_post()
	{
		$this->_post_args = $this->_identity($this->_post_args);
	
		parent::index_post();
	}
	protected function index_put()
	{
		$this->_put_args = $this->_identity($this->_put_args);
	
		parent::index_put();
	}
	protected function index_delete()
	{
		$this->_delete_args = $this->_identity($this->_delete_args);
	
		parent::index_delete();
	}
}
