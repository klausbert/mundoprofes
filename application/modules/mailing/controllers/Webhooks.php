<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webhooks extends REST_Controller_v3
{
	public function index_get()		{ $this->status(200); }
	public function index_put()		{ $this->status(405); }
	public function index_delete()	{ $this->status(405); }
}
