<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mailing extends MX_Controller 
{
	public function hello()
	{
		$this->load->library('email');
		$this->config->load('emails', TRUE);
		
		extract($this->config->item('emails'));
		$subject = "Hello";
		$email_message = "Newman!";
		$this->email->to($email_bcc);
		$this->email->cc($email_cc);
		$this->email->from($email_from, $email_sender);	// had it backwards!
		$this->email->subject($subject);
		$this->email->message($email_message);
		
		$this->email->send();
		echo print_r($this->email->print_debugger(), TRUE);
	}
	public function post($data = [])
	{
		$this->load->model('session_model');
		if (! isset($data['email'])) 
			$data['email'] = $this->session_model->index()['email'];
		
		if (! isset($data['view']) and ! isset($data['post'])) 
			$data['error'] = 'missing view or post name';
		else
		{
			$this->load->library('email');
			$this->config->load('emails', TRUE);
			
			extract($this->config->item('emails'));
			
			if (isset($data['view']))
			{
				$subject = $email_sender;
				$email_message = $this->load->view($data['view'], $data, TRUE);
			}
			else 
			if (isset($data['post']))
			{
				$post_id  = $email_source[$data['post']];
				$post_url = site_url("wp/?json=get_post&post_id=$post_id");
				$wp_view  = self::getJSON($post_url);
					
				if ($wp_view['status']!=='ok')
				{
					// bail out, save error to log
					$wp_view['post_url'] = $post_url;
					log_message('error', print_r($wp_view, TRUE));
					$data['error'] = 'mailing source error; check the logs';
					return $data;
				}
				$subject = $wp_view['post']['title'];
				$email_message = html_entity_decode($wp_view['post']['content']);
				
				//	1. special case
				if (isset($data['request'])) 
					$email_message = str_replace('{{request}}', site_url('#/first/'.$data['request']), $email_message);	
				//	2. general case
				foreach ($data as $key => $value)
					$email_message = str_replace("{{" .$key. "}}",   $value, $email_message);
			}
			$this->email->to(($notify_to_author) ? $data['email'] : $email_bcc);
			$this->email->cc($email_cc);
			$this->email->bcc($email_bcc);
			$this->email->from($email_from, $email_sender);	// had it backwards!
			$this->email->subject($subject);
			$this->email->message($email_message);
			if (! $this->email->send()) 
			{
				$data['error'] = 'mailing not sent; check the logs';
			}
			log_message('error', ENVIRONMENT ."\t". $_SERVER['SERVER_NAME'] ."\t". $_SERVER['REQUEST_URI']);
			log_message('error', print_r($this->email->print_debugger(), TRUE));
		}
		if (isset($data['error']))
			log_message('error', print_r($data, TRUE));
		
		return (! isset($data['error']) or TRUE);
	}
	//
	//
	//
	private static function getJSON($uri, $qs = FALSE)
	{
		if ($qs) $uri = $uri .'?'. http_build_query($qs);
		$ctx = stream_context_create(['http' => [
			'method' => 'GET',
			'header' => "Authorization: Basic ". base64_encode('mp:mirto130'),
			'ignore_errors' => TRUE,
		]]);
		$js = @file_get_contents($uri, FALSE, $ctx);
		if ($js)
			return self::JSONtoArray($js);	
		else
			return ['status' => 'error', 'uri' => $uri];
	}
	private static function JSONtoArray($data)
	{
		return json_decode( preg_replace("/\\\u([0-9a-f]{4})/", "&#x\\1;", $data), TRUE);
	}
}
