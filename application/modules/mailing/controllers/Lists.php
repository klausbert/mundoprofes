<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lists extends REST_Controller_v3
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('HTTP_Model', 'HTTP');
	}
	/*
	**	LISTS
	*/
	public function index_get()
	{
		$this->config->load('emails', TRUE);
		extract($this->config->item('emails'));
		
		$encodedAuth = base64_encode('anystring:'. $mailChimp['apiKey']);

		$action = $mailChimp['url'].'lists?fields=lists';
		$curl = $this->HTTP->curl($action, FALSE, ["Authorization: Basic $encodedAuth"], 'GET');
		
		$this->response(json_decode($curl['response'], TRUE)['lists'], $curl['status']);
	}
	public function index_post()	{ $this->status(405); }
	public function index_put()		{ $this->status(405); }
	public function index_delete()	{ $this->status(405); }
	/*
	**	LISTS/{list_id}/MEMBERS
	*/
	// public function members_get()
	// {
		// if (extract($this->get()) and isset($filter)
		// and $data = $this->model->members($filter) and count($data))
		// {
			// $this->config->load('emails', TRUE);			
			// extract($this->config->item('emails'));
			
			// $encodedAuth = base64_encode('anystring:'. $mailChimp['apiKey']);
			// $basicAuth = 
				// "Content-type: application/json\r\n".
				// "Authorization: Basic $encodedAuth"
			// ;
			
			// $result = [];
			// foreach ($data as $index => $cv)
			// {
				// $code = 'X';
				// if (($cv['rut'] + $cv['cv'])>=0 and ($cv['password'] + $cv['facebook'] + $cv['google'] + $cv['linkedin'])==0) $code = 'R';
				// //	($cv['rut'] + $cv['cv'])>=0 con passwords en 0 no deberia darse nunca
				// else
				// if (($cv['rut'] + $cv['cv'])==0 and ($cv['password'] + $cv['facebook'] + $cv['google'] + $cv['linkedin'])> 0
				 // or ($cv['rut'] + $cv['cv'])< 2 and ($cv['password'] + $cv['facebook'] + $cv['google'] + $cv['linkedin'])==0) $code = 'A';
				// else
				// if (($cv['rut'] + $cv['cv'])==2) $code = 'V';
				
				// if ($code!=='X')
				// {
					// $action = $mailChimp['url'].'lists/'. $mailChimp['listas'][$code] .'/members';
					
					// $body["email_address"] = $cv['email'];
					// $body["merge_fields"] = ["FNAME" => $cv['nombres'], "LNAME" => $cv['apellidos']];
					// $body["status"] = "subscribed";
					
					// $curl = $this->HTTP->curl($action, json_encode($body), ['Content-Type: application/json', "Authorization: Basic $encodedAuth"], 'POST');
					
					// $result[$index]['response'] = $curl['response'];
					// $result[$index]['status'] = $curl['status'];
				// }
			// }
			// $this->response($result);
		// }
	// }
	public function members_post()
	{
		if (extract($this->post()) and isset($listId, $email, $fName, $lName))
		{
			$this->config->load('emails', TRUE);			
			extract($this->config->item('emails'));
			
			$encodedAuth = base64_encode('anystring:'. $mailChimp['apiKey']);
			$basicAuth = 
				"Content-type: application/json\r\n".
				"Authorization: Basic $encodedAuth"
			;
			
			$action = $mailChimp['url']."lists/$listId/members";
			
			$body["email_address"] = $email;
			$body["merge_fields"] = ["FNAME" => $fName, "LNAME" => $lName];
			$body["status"] = "subscribed";
			
			$curl = $this->HTTP->curl($action, json_encode($body), ['Content-Type: application/json', "Authorization: Basic $encodedAuth"], 'POST');
			
			if ($curl['status']==200)
				$this->db->update('usuarios', ['mailing_list' => $listId], ['email' => $email]);
			
			$this->response(json_decode($curl['response'], TRUE), $curl['status']);
		}
	}
	public function members_delete()
	{
		$this->log_error('_delete_args', print_r($this->_delete_args, TRUE));
		
		if (extract($this->delete()) and isset($listId, $memberId))
		{
			$this->config->load('emails', TRUE);		
			extract($this->config->item('emails'));
			
			$action = $mailChimp['url']."lists/$listId/members/$memberId";
			$encodedAuth = base64_encode('anystring:'. $mailChimp['apiKey']);
			
			$curl = $this->HTTP->curl($action, FALSE, ["Authorization: Basic $encodedAuth"], 'DELETE');
			
			$this->response(json_decode($curl['response'], TRUE), $curl['status']);
		}
	}
}
