<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webhooks_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios');
	}
	public function create($values = FALSE)
	{
		$this->log_error('create', $values);
		/*
		2016-04-17 10:50:21 --> Webhooks_model - create 
		Array
		(
			[type] => unsubscribe
			[fired_at] => 2016-04-17 13:50:21
			[data] => Array
				(
					[action] => unsub
					[reason] => manual
					[id] => fa543d2c38
					[email] => klaus.pieslinger@gmail.com
					[email_type] => html
					[ip_opt] => 181.44.4.145
					[ip_signup] => 181.44.4.145
					[web_id] => 131722145
					[campaign_id] => 3ff5ad2f52
					[merges] => Array
						(
							[EMAIL] => klaus.pieslinger@gmail.com
							[FNAME] => Klaus
							[LNAME] => Pieslinger
						)

					[list_id] => 4cf56a53bb
				)
		)
		*/
		if (extract($values) and $type=='unsubscribe')
		{
			foreach ($data as $item)
			{
				if (extract($item) and $action=='unsub')
				{
					$this->db->update($this->table, ['mailing_list' => $list_id, 'mailing_unsub' => $fired_at], ['email' => $email]);
					$this->log_error('unsub', $email);
				}
			}
		}
		return $type;
	}
}
