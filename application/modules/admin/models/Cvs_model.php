<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cvs_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('profesor');
	}
	public function index($and_filter = '', $limit = '')
	{
		return $this->db->query("
			SELECT U.*
			, CONCAT_WS(' ', P.apellidos, P.apellido_m) AS apellidos
			, IFNULL(P.nombres, '') AS nombres
			, CONCAT_WS(', ', CONCAT_WS(' ', P.apellidos, P.apellido_m), P.nombres) AS nombre
			,(U.password IS NOT NULL) AS password
			,(U.facebook IS NOT NULL) AS facebook
			,(U.google   IS NOT NULL) AS google
			,(U.linkedin IS NOT NULL) AS linkedin
			,(P.RUT      IS NOT NULL) AS rut
			,(P.cv       IS NOT NULL) AS cv
			,(SELECT COUNT(*) FROM profes_formacion_profesional WHERE P.id = id_profesor) AS FP
			,(SELECT COUNT(*) FROM profes_formacion_postgrado   WHERE P.id = id_profesor) AS FO
			,(SELECT COUNT(*) FROM profesor_intereses           WHERE P.id = id_profesor AND categoria = 'etiquetas')   AS CP
			,(SELECT COUNT(*) FROM profesor_intereses           WHERE P.id = id_profesor AND categoria = 'profesiones') AS PR
			,(U.bloqueado IS NOT NULL OR U.mailing_unsub IS NOT NULL) AS bloqueado
			FROM usuarios U 
			LEFT JOIN profesor P ON P.id = U.id
			WHERE U.tipo = 'profesor' $and_filter
			ORDER BY U.creado DESC, P.apellidos, P.apellido_m, P.nombres
			$limit
		")->result_array();
	}
	public function show($where)
	{
		if ($data = $this->db->get_where('profesor', $where)->row_array())
		{
			$data['assets'] = $this->db->where('id', $data['id'])->get('usuarios')->row()->assets;
			
			$this->load->model('comunas_model', 'comunas');
			$data['comunas'] = $this->comunas->show( array('id' => $data['id_comuna']));
			
			$data['etiquetas'] = $this->db->query("
				SELECT T.id, etiqueta 
				FROM profesor_intereses 
				JOIN etiquetas T ON id_referencia = T.id 
				WHERE id_profesor = {$data['id']}
			")->result_array();
			$data['profesiones'] = $this->db->query("
				SELECT T.id, T.profesion
				FROM profesor_intereses 
				JOIN profesiones T ON id_referencia = T.id 
				WHERE id_profesor = {$data['id']}
			")->result_array();
			
			$this->load->model('profesor/formacion_profesional_model', 'profesional');
			$data['formacion']['profesional'] = $this->profesional->show( array('id_profesor' => $data['id']));
			
			$this->load->model('profesor/formacion_postgrado_model', 'postgrado');
			$data['formacion']['postgrado'] = $this->postgrado->show( array('id_profesor' => $data['id']));
			
			return $data;
		}
	}
	public function create($values)
	{
		extract($values);
		
		if ($result = $this->db->get_where($this->table, "RUT = $RUT")->row_array())
			$result['error'] = 'El RUT ya existe en la base de datos';
		else
		if ($result = $this->db->get_where('usuarios', "email = '$email'")->row_array())
			$result['error'] = 'El Email ya existe en la base de datos';
		else
		{
			$this->load->model('profesor/signup_model', 'signup');
			$result = $this->signup->create($values);
		}
		return $result;
	}
	public function update($values)
	{
		extract($values);
		
		$values['id_comuna'] = @$comunas[0]['id'];
		
		$this->load->model('profesor/formacion_profesional_model', 'profesional');
		foreach ($formacion['profesional'] as $profesional)
		{
			$profesional['id_profesor'] = $id;
			$this->profesional->create($profesional);
		}
		$this->load->model('profesor/formacion_postgrado_model', 'postgrado');
		foreach ($formacion['postgrado'] as $postgrado)
		{
			$postgrado['id_profesor'] = $id;
			$this->postgrado->create($postgrado);
		}
		if (isset($etiquetas))
			$this->_update_referencias('etiquetas', $etiquetas, 'etiqueta', $id);
		
		if (isset($profesiones)) 
			$this->_update_referencias('profesiones', $profesiones, 'profesion', $id);

		if (isset($email)) // cambió el email
			$this->db->set('email', $email)->where('id', $id)->update('usuarios');
		
		$values['updated_at'] = NULL;	// log
		$values['updated_by'] = $this->session->userdata('id');	// log
		
		parent::update($values);
		
		return $this->show(array('id' => $id));
	}
	private function _update_referencias($categoria, $list, $referencia, $id_profesor)
	{
		$this->db->where('id_profesor', $id_profesor)->where('categoria', $categoria)->delete('profesor_intereses');
		
		foreach ($list as $item)
		{
			if (! isset($item['id']))
			{
				$this->db->insert('_referencias', ['categoria' => $categoria, 'referencia' => $item[$referencia]]);
				$profesion['id'] = $this->db->insert_id();
			}
			$item['categoria'] = $categoria; 
			$item['id_referencia'] = $item['id']; 
			$item['id_profesor'] = $id_profesor;
			unset($item['id'], $item[$referencia]);
			$this->db->insert('profesor_intereses', $item);
		}
	}
	/*
	**	OTHER METHODS
	*/
	public function download($id)
	{
		return $this->db->query("
			SELECT U.assets, P.cv
			FROM usuarios U JOIN profesor P ON U.id = P.id
			WHERE U.id = $id
		")->row_array();
	}
	public function filter($what, $query)
	{
		$and_filter = '';
		$no_pw = 'U.password IS NULL AND U.facebook IS NULL AND U.google IS NULL AND U.linkedin IS NULL';
		$no_cv = 'P.cv IS NULL AND P.RUT IS NULL';
		
		if ($what=='pw')
			$and_filter = "AND $no_pw";
		else
		if ($what=='cv')
			$and_filter = "AND $no_cv AND NOT ($no_pw)";
		else
		if ($what=='ok')
			$and_filter = "AND NOT ($no_cv) AND NOT ($no_pw)";
		else
		if ($what=='fb')
			$and_filter = 'AND U.facebook IS NOT NULL';
		else
		if ($what=='gg')
			$and_filter = 'AND U.google IS NOT NULL';
		else
		if ($what=='in')
			$and_filter = 'AND U.linkedin IS NOT NULL';
		else
		if ($what=='comunas' and isset($query['ids']) and $query['ids'] > '')
			$and_filter = "AND U.id IN (
				SELECT P.id 
				FROM profesor P
				WHERE P.id_comuna IN ({$query['ids']})
			)";
		else
		if ($what=='etiquetas' and isset($query['ids']) and $query['ids'] > '')
			$and_filter = "AND U.id IN (
				SELECT X.id_profesor 
				FROM profesor_intereses X
				WHERE X.categoria = '$what' AND X.id_referencia IN ({$query['ids']})
			)";
		else
		if ($what=='experiencia' and isset($query['ids']) and $query['ids'] > '')
			$and_filter = "AND U.id IN (
				SELECT P.id 
				FROM profesor P
				WHERE P.cv_experiencia = '{$query['ids']}'
			)";
		else
		if ($what=='profesiones' and isset($query['ids']) and $query['ids'] > '')
			$and_filter = "AND U.id IN (
				SELECT X.id_profesor 
				FROM profesor_intereses X
				WHERE X.categoria = '$what' AND X.id_referencia IN ({$query['ids']})
			)";
		else
		if ($what > '')
			$and_filter = "AND (
					U.email LIKE '%$what%'
				OR	CONCAT(P.apellidos, ' ', P.apellido_m, ', ', P.nombres) LIKE '%$what%')
			";
		$limit =(isset($query['limit'])) ? "LIMIT ". str_replace('-', ', ', $query['limit']) : "";
		
		return $this->index($and_filter, $limit);
	}
	//
	// UPLOAD assets
	//
	public function upload($values, $set_cv = TRUE)
	{
		extract($values);
		$this->log_error("(not an error)", $values);
		
		if ($size > 2000000)
			return array('error'=>"El archivo pesa más de 2 MB ($size)")
		;
		// if (! in_array($type, array('application/pdf', 'text/pdf', 'application/docx')))
			// return array('error'=>"El tipo de archivo no está permitido ($type)")
		// ;
		$data = $this->db->where('id', $id)->get('usuarios')->row_array();
		if (! isset($data['assets']) or $data['assets']=='')
		{
			$data['assets'] = md5($data['creado'] ." ". strrev($id));
			$this->db->set('assets', $data['assets'])->where('id', $id)->update('usuarios');
		}
		$assets_folder = "z/". $data['assets'];
		is_dir($assets_folder) or mkdir($assets_folder, 0777, TRUE);
		move_uploaded_file($tmp_name, "$assets_folder/$name");
		
		if ($set_cv)
			$this->db->set('cv', $name)->where('id', $id)->update('profesor');	//	reemplaza y ya
		
		return $values;
	}
}
