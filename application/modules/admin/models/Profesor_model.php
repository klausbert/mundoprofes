<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profesor_model extends MY_Model
{
	public function show($where)
	{
		$this->load->model('profesor/datos_personales_model', 'datos_personales');
		$this->load->model('profesor/experiencia_colegios_model', 'experiencia_colegios');
		$this->load->model('profesor/experiencia_otras_model', 'experiencia_otras');
		$this->load->model('profesor/formacion_escolar_model', 'formacion_escolar');
		$this->load->model('profesor/formacion_postgrado_model', 'formacion_postgrado');
		$this->load->model('profesor/formacion_profesional_model', 'formacion_profesional');
		$this->load->model('profesor/intereses_model', 'intereses');
		
		$data['datos_personales'] = $this->datos_personales->show($where);
		$data['intereses'] = $this->intereses->show($where);
		
		$where['id_profesor'] = $where['id']; unset($where['id']);
		
		$data['experiencia_colegios'] = $this->experiencia_colegios->show($where);
		$data['experiencia_otras'] = $this->experiencia_otras->show($where);
		$data['formacion_escolar'] = $this->formacion_escolar->show($where);
		$data['formacion_postgrado'] = $this->formacion_postgrado->show($where);
		$data['formacion_profesional'] = $this->formacion_profesional->show($where);
		return $data;
	}
}
