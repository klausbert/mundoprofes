<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos_model extends MY_Model
{
	public function index()
	{
		$data['list'] = $this->db->query("
			SELECT *
			, IF(vigencia < CURDATE(), 'vencido', estado) AS estado
			, DATEDIFF(IFNULL(vigencia, DATE_ADD(CURDATE(), INTERVAL 15 DAY)), CURDATE()) AS dias_v
			,(SELECT COUNT(*) FROM avisos_profesores WHERE id_aviso = avisos.id) AS postulantes
			,(SELECT MIN(region) FROM regiones    WHERE id = avisos.id_region) AS region
			,(SELECT MIN(comuna) FROM comunas_v2  WHERE id = avisos.id_comuna) AS comuna
			,(
				SELECT nombre FROM colegios_v2 WHERE id = avisos.id_colegio
			) AS institucion
			FROM avisos
			ORDER BY fecha DESC
		")->result_array();
		return $data;
	}
	public function show($where)
	{
		extract($where);
		
		if (isset($id) and $id > 0 and $item = $this->db->where("id = $id")->get('avisos')->row_array())
		{
			$item['titulos']      = $this->db->where("id IN (SELECT id_titulo     FROM avisos_titulos      WHERE id_aviso = $id)", NULL, FALSE)->get('titulos')->result_array();
			$item['experiencias'] = $this->db->where("id IN (SELECT id_cargo      FROM avisos_experiencias WHERE id_aviso = $id)", NULL, FALSE)->get('cargos')->result_array();
			$item['asignaturas']  = $this->db->where("id IN (SELECT id_asignatura FROM avisos_asignaturas  WHERE id_aviso = $id)", NULL, FALSE)->get('asignaturas')->result_array();
			$item['niveles']      = $this->db->where("id IN (SELECT id_nivel      FROM avisos_niveles      WHERE id_aviso = $id)", NULL, FALSE)->get('niveles')->result_array();
			if (isset($data['id_comuna']))  $item['comunas']   = $this->db->where("id", $data['id_comuna'])->get('comunas_v2')->result_array();
			if (isset($data['id_funcion'])) $item['funciones'] = $this->db->where("id", $data['id_funcion'])->get('cargos')->result_array();
			
			$item['colegio'] = $this->db->select('B.*')
				->from('colegios_v2 B')->join('colegios_v2b T', 'B.id = T.RBD')
				->where("B.id = {$item['id_colegio']}")->get()->row_array();
			
			$email = $this->db->query("
				SELECT GROUP_CONCAT(email SEPARATOR ', ') AS email
				FROM (
					SELECT U.email FROM usuarios U JOIN colegios_v2 B ON U.id = B.id
				UNION
					SELECT email FROM usuarios U 
					JOIN usuarios_roles X ON U.id = X.id_usuario 
					JOIN usuarios_roles_colegios Y ON X.id = Y.id_role AND Y.id_colegio = {$item['id_colegio']}
				) G 
			")->row()->email;
			
			return ['item' => $item, 'email' => $email, 'options' => $this->enum_options()];
		}
	}
	public function create($values)
	{
		return FALSE;	//	se hace desde COMPRAS
	}
	public function update($values)
	{
		if (extract($values) and isset($id, $estado)) 
		{
			$where['id'] = $id;
			
			if (isset($vigencia)) 
				$set['vigencia'] = $vigencia;
			$set['estado'] = $estado;
			
			$this->db->update($this->table, $set, $where);
			
			return $this->show($where)['item'];
		}
	}
	public function delete($values)
	{
		return FALSE;
	}
}
