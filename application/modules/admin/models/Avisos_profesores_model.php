<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos_profesores_model extends MY_Model
{
	public function index()
	{
		$grid = $this->db->query("
			SELECT A.id AS id_aviso, A.busqueda, A.estado, A.fecha, C.nombre AS colegio
			FROM avisos_profesores X
			JOIN avisos A				ON X.id_aviso = A.id 
			JOIN colegios_v2 C			ON A.id_colegio = C.id
			WHERE A.estado NOT IN ('rechazado', 'vencido')
			GROUP BY A.id HAVING COUNT(*) > 0
			ORDER BY A.fecha DESC, A.id
		")->result_array();
		
		foreach ($grid as &$item)
			$item['postulantes'] = $this->db->query("
				SELECT X.situacion, COUNT(*) AS cantidad
				FROM avisos_profesores X
				WHERE X.id_aviso = {$item['id_aviso']}
				GROUP BY X.situacion
				ORDER BY X.situacion
			")->result_array();
			
		return ['list' => $grid, 'enum' => $this->enum_options()];
	}
	public function show($where)
	{
		$enum = $this->enum_options();
		if (extract($where) and ! isset($id_aviso, $situacion))
			return ['error' => 'Parámetros faltantes'];
		
		if (! in_array($situacion, $enum['situacion']))
			return ['error' => 'Parámetro inválido; valores aceptados: '.implode('/', $enum['situacion'])];
		
		$head = $this->db->query("
			SELECT A.busqueda, C.nombre AS colegio, '$situacion' AS situacion
			FROM avisos A
			JOIN colegios_v2 C ON A.id_colegio = C.id
			WHERE A.id = $id_aviso
		")->row_array();
		
		$list = $this->db->query("
			SELECT P.*, X.situacion
			, P.id AS id_profesor, X.id
			FROM profesor P
			JOIN avisos_profesores X ON P.id = X.id_profesor
			JOIN avisos A ON X.id_aviso = A.id
			JOIN colegios_v2 C ON A.id_colegio = C.id
			WHERE X.id_aviso = $id_aviso 
			  AND X.situacion = '$situacion'
		")->result_array();
		
		foreach ($list as &$item)
			$item['profesiones'] = $this->db->query("
				SELECT GROUP_CONCAT(profesion SEPARATOR ', ') AS profesiones
				FROM profesor_intereses X
				JOIN profesiones F ON X.id_referencia = F.id AND X.id_profesor = {$item['id_profesor']}
			")->row()->profesiones;
			
		// {
			// $item['titulos'] = $this->db->query("
				// SELECT nivel, titulo
				// FROM titulos T
				// JOIN profes_formacion_profesional L ON T.id = L.id_titulo_profesional AND L.id_profesor = {$item['id']}
			// ")->result_array();
			// $item['titulos'] += $this->db->query("
				// SELECT nivel, titulo
				// FROM titulos T
				// JOIN profes_formacion_postgrado L ON T.id = L.id_titulo_postgrado AND L.id_profesor = {$item['id']}
			// ")->result_array();
		// }
		return ['list' => $list, 'head' => $head, 'enum' => $this->enum_options('avisos_profesores')];
	}
	public function create($values)
	{
		return FALSE;
	}
	public function update($values)
	{
		log_error('AP::update', $values);
		return parent::update($values);
	}
	public function delete($values)
	{
		return FALSE;
	}
}
