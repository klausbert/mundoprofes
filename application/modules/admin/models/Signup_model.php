<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup_model extends MY_Model
{
	public function index()
	{
		return $this->db->query("
			SELECT *
			FROM usuarios U
			JOIN usuarios_a_confirmar A ON U.id = A.id 
		")->result_array();
	}
	public function update($values)
	{
		extract($values);
		
		if (isset($id)) 
		{
			$set['recordatorio'] = date('Y-m-d');
			$where['id'] = $id;
			
			$this->db->update('usuarios_a_confirmar', $set, $where);
			
			return $this->db->affected_rows();
		}
	}
}
