<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends MY_Model
{
	public function index()
	{
		return false;
	}
	public function show($where)
	{
		if (extract($where) and isset($like))
		{
			$this->db->like($like, $where[$like]);
			unset($where['like'], $where[$like]);
		}
		$this->db->where($this->cleanup($where));
		
		$list = parent::index();
		foreach ($list as &$item)
		{
			unset($item['password']);
			$item += $this->db->select('RUT, nombres, apellidos')->get_where('profesor', "id = {$item['id']}")->row_array();
		}
		return $list;
	}
}
