<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles_model extends MY_Model
{
	public function index($where = FALSE)
	{
		$id_root = $this->session->userdata('id');
		if (! $id_root) $id_root = 0;
		
		$where = "X.role IN ('supervisor', 'vendedor')";
		return $this->db->query("
			SELECT X.id, U.email, P.nombres, P.apellidos, P.RUT, X.role
			, COUNT(Y.id_colegio) AS colegios
			, (SELECT apellidos FROM usuarios WHERE id = X.created_by) AS creador
			, IF(X.created_by = $id_root, 1, 0) AS directo
			FROM usuarios_roles X LEFT
			JOIN usuarios_roles_colegios Y ON X.id = Y.id_role
			JOIN usuarios U ON X.id_usuario = U.id
			JOIN profesor P ON X.id_usuario = P.id
			WHERE $where
			GROUP BY X.id
		")->result_array();
	}
	public function show($where)
	{
		$id_root = $this->session->userdata('id');
		if (! $id_root) $id_root = 0;
		
		if (extract($where) and isset($id_role))
		{
			$where = "X.id = $id_role";
			$result = $this->db->query("
				SELECT X.id, U.email, P.nombres, P.apellidos, P.RUT, X.role
				, GROUP_CONCAT(Y.id_colegio SEPARATOR ', ') AS ids_colegios
				, (SELECT apellidos FROM usuarios WHERE id = X.created_by) AS creador
				, IF(X.created_by = $id_root, 1, 0) AS directo
				FROM usuarios_roles X LEFT
				JOIN usuarios_roles_colegios Y ON X.id = Y.id_role
				JOIN usuarios U ON X.id_usuario = U.id
				JOIN profesor P ON X.id_usuario = P.id
				WHERE $where
				GROUP BY X.id
			")->row_array();
			
			if (isset($result['ids_colegios']))
				$result['colegios'] = $this->db->query("
					SELECT * FROM colegios_v2 WHERE id IN ({$result['ids_colegios']})
				")->result_array();
			
			return $result;
		}
	}
	public function create($values)
	{
		extract($values);
		if (isset($email, $role))
		{
			$exists['email'] = $this->db->where('email', $email)->get('usuarios')->row_array();
			if (count($exists['email'])==0)
				return ['error' => 'El email no está en la base de datos']
			;
			if (! in_array($role, ['vendedor', 'supervisor']))
				return ['error' => "El rol '$role' no está permitido"];
			;
			$this->db->set('id_usuario', $exists['email']['id'])->set('role', $role);
			$this->db->set('created_by', $this->session->userdata('id'));
			$this->db->insert('usuarios_roles');
			$id_role = $this->db->insert_id();
			
			log_error('Inserted rows: '. $this->db->affected_rows(), $this->db->last_query());
			
			return $this->show(['id_role' => $id_role]);			
		}
		else if (isset($id_role, $id_colegio))
		{
			$this->db->set('id_role', $id_role)->set('id_colegio', $id_colegio);
			$this->db->insert('usuarios_roles_colegios');
			$id = $this->db->insert_id();
			
			log_error('Inserted rows: '. $this->db->affected_rows(), $this->db->last_query());
			
			return $this->show(['id_role' => $id_role]);			
		}
	}
	public function update($values)
	{
		return ['update' => $values];
	}
	public function delete($values)
	{
		if (extract($values) and isset($id_colegio, $id_role))
		{
			$this->db->where('id_colegio', $id_colegio)->where('id_role', $id_role)->delete('usuarios_roles_colegios');
			return ['deleted' => $this->db->affected_rows()];
		}
		else
		if (isset($id))
		{
			$this->db->where('id', $id)->delete('usuarios_roles');
			return ['deleted' => $this->db->affected_rows()];
		}
		else 
			return ['error' => $values];
	}
}
