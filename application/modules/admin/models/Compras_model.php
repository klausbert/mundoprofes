<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Compras_model extends MY_Model
{
	public function index()
	{
		$list = $this->db->order_by('id', 'DESC')->get_where($this->table, "estado = 'pendiente'")->result_array();
		foreach($list as &$item)
		{
			$item['colegio']  = $this->db->get_where('colegios',  "id = {$item['id_colegio']}")->row_array();
			$item['producto'] = $this->db->get_where('productos_detalle', "id = {$item['id_producto']}")->row_array();
		}
		return $list;		// all pending
	}
	public function show($where)
	{
		extract($where);
		
		if (isset($id))
		{
			$item = $this->db->get_where($this->table, "id = $id")->row_array();
			$item['colegio']  = $this->db->get_where('colegios',  "id = {$item['id_colegio']}")->row_array();
			$item['producto'] = $this->db->get_where('productos_detalle', "id = {$item['id_producto']}")->row_array();
			
			return $item;
		}
	}
	public function update($values)
	{
		if (extract($values) and ! isset($id))
			return;
		
		$item = parent::update($values);
		
		$item['email'] = $this->db->query("
			SELECT GROUP_CONCAT(email SEPARATOR ', ') AS email
			FROM (
				SELECT U.email FROM usuarios U 
				JOIN colegios_v2 B ON U.id = B.id_usuario AND B.id = {$item['id_colegio']}
			UNION
				SELECT email FROM usuarios U 
				JOIN usuarios_roles X ON U.id = X.id_usuario 
				JOIN usuarios_roles_colegios Y ON X.id = Y.id_role AND Y.id_colegio = {$item['id_colegio']}
			) G 
		")->row()->email;
		
		if ($item['estado']=='verificado')
		{
			$avisos = $item['producto']['avisos'] - $this->db->where('id_compra', $id)->count_all_results('avisos');
			
			while ($avisos > 0)
			{
				$this->db->query("
					INSERT INTO avisos (id_colegio, id_compra, estado, fecha)
					SELECT id_colegio, id AS id_compra, 'disponible', NOW() 
					FROM compras
					WHERE id = $id
				");
				$item['id_aviso'][] = $this->db->insert_id();
				$avisos--;
			}
		}
		return $item;
	}
}
