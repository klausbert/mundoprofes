<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busquedas_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('busquedas');
	}
	protected function index_get($where)
	{
		if (isset($where['id']))
			$this->show($where);
		else
			$this->index();
	}
	public function index()
	{
		$data['list'] = $this->db->get($this->table)->result_array();
		return $data;
	}
	public function show($where)
	{
		$data = array();
		if (isset($where['id']))
		{
			if ($data['item'] = $this->db->where($where)->get($this->table)->row_array())
			{
				$data['item']['titulos'] = $this->db->select('T.*')
				->from('busquedas_titulos B')->join('titulos T', 'B.id_titulo = T.id')
				->where("B.id_busqueda = {$data['item']['id']}")->get()->result_array();
				//	ENUMs
				foreach ($this->db->query("SHOW COLUMNS FROM {$this->table} WHERE type LIKE 'enum(%'")->result_array() as $row)
				{
					preg_match_all("/'(.*?)'/", $row['Type'], $enum_array);
					$data['options'][$row['Field']] = $enum_array[1];
				}
				//	already selected
				$data['prospects'] = $this->_prospects($where['id']);
				
				//	results
				$data['sets'] = $this->_sets($data['item']);
			}
			else
			{
				$data['error'] = "Busqueda #{$where['id']} no encontrada";
			}
		}
		return $data;
	}
	private function _prospects($id)
	{
		return $this->db->select('P.id, nombres, apellidos, apellido_m')->where('id_busqueda', $id)
		->from('profesor P')->join('busquedas_prospectos B', 'P.id = B.id_profesor')->get()->result_array();
	}
	private function _sets($where)
	{
		extract($where);
		$data = array();
		if (isset($titulos))
			foreach ($titulos as $titulo)
				$data['titulos'][$titulo['id']] = $this->db->query("
					SELECT P.id, nombres, apellidos, apellido_m
					FROM profes_formacion_profesional F
					JOIN profesor P ON F.id_profesor = P.id
					WHERE id_titulo_profesional = {$titulo['id']}
				UNION ALL
					SELECT P.id, nombres, apellidos, apellido_m
					FROM profes_formacion_postgrado F
					JOIN profesor P ON F.id_profesor = P.id
					WHERE id_titulo_postgrado = {$titulo['id']}
				")->result_array();
		if (isset($cv_experiencia))
			$data['cv_experiencia'] = $this->db->select('P.id, nombres, apellidos, apellido_m')
			->from('profesor P')->where('cv_experiencia', $cv_experiencia)->get()->result_array();
		if (isset($tamanio))
			$data['tamanio'] = $this->db->distinct()->select('P.id, nombres, apellidos, apellido_m')
			->from('colegios_v2b C')->join('profes_experiencia_colegios E', 'C.RBD = E.id_colegio')
			->join('profesor P', 'E.id_profesor = P.id')->where('tamanio', $tamanio)->get()->result_array();
		if (isset($dependencia))
			$data['dependencia'] = $this->db->distinct()->select('P.id, nombres, apellidos, apellido_m')
			->from('colegios_v2 C')->join('profes_experiencia_colegios E', 'C.id = E.id_colegio')
			->join('profesor P', 'E.id_profesor = P.id')->where('dependencia', $dependencia)->get()->result_array();
		if (isset($laico_religioso))
			$data['laico_religioso'] = $this->db->distinct()->select('P.id, nombres, apellidos, apellido_m')
			->from('colegios_v2b C')->join('profes_experiencia_colegios E', 'C.RBD = E.id_colegio')
			->join('profesor P', 'E.id_profesor = P.id')->where('laico_religioso', $laico_religioso)->get()->result_array();
		if (isset($tipo))
			$data['tipo'] = $this->db->distinct()->select('P.id, nombres, apellidos, apellido_m')
			->from('colegios_v2 C')->join('profes_experiencia_colegios E', 'C.id = E.id_colegio')
			->join('profesor P', 'E.id_profesor = P.id')->where('tipo', $tipo)->get()->result_array();
		// if (isset($where['sueldo_mensual_hasta']))
		// {
			// foreach ($this->db->distinct()->select('id')->where('sueldo_mensual_desde <= '. $where['sueldo_mensual_hasta'])->get('profesor')->result_array() as $item)
				// $data['sueldo_mensual_hasta'][] = 0 + $item['id'];
		// }
		return $data;
	}
	public function create($where)
	{
		extract($where);
		if (isset($id, $ids_prospectos))
		{
			$this->db->query("
				INSERT IGNORE INTO busquedas_prospectos (id_busqueda, id_profesor)
				SELECT $id AS id_busqueda, id AS id_profesor
				FROM profesor WHERE id IN ($ids_prospectos)
			");
			$data['prospects'] = $this->_prospects($id);
			return $data;
		}
	}
	public function update($where)
	{
		extract($where);
		if (isset($id, $estado))	// cambio de estado
		{
			$this->db->where('id', $id)->set('estado', $estado)->update('busquedas');
			return array('updated' => $this->db->affected_rows());
		}
	}
	public function delete($where)
	{
		extract($where);
		if (isset($id, $ids_prospectos))
		{
			$this->db->where('id_busqueda', $id)->where("id_profesor IN ($ids_prospectos)")->delete('busquedas_prospectos');
			$data['prospects'] = $this->_prospects($id);
			return $data;
		}
	}
}
