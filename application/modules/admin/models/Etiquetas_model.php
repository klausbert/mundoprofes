<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Etiquetas_model extends MY_Model
{
	public function index()
	{
		$result = parent::index();
		
		foreach ($result as &$item)
		{
			$item['cvs'] = $this->db->query("
				SELECT COUNT(*) AS cvs
				FROM profesor_intereses
				WHERE id_referencia = {$item['id']} AND categoria = 'etiquetas'
			")->row()->cvs;
		}
		return $result;
	}
	public function show($values)
	{
		if (isset($values['etiqueta'])) 
			$this->db->like('etiqueta', $values['etiqueta']);
		else 
			$this->db->where($values);
		
		return $this->db->get($this->table)->result_array();
	}
}
