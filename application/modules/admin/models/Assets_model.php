<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assets_model extends MY_Model
{
	public function __construct()
	{
		$this->load->helper('directory');
	}
	public function index()
	{
		return FALSE;
	}
	public function show($where)
	{
		/*	directory_map returns a "malformed" array - not suitable for json encoding
		*/
		$data = $this->_recurseTree( directory_map($where['dir']));
		return $data;
	}
	public function create($where)
	{
		$data = array();
		extract($where);
		foreach ($this->show($where) as $node)
		{
			extract($node);
			
			$check = $this->db->where('email', "$file-cv@mundoprofes.cl")->select('id')->get('usuarios')->row_array();
			if (! isset($check['id'])) {
				$this->db->set('email', "$file-cv@mundoprofes.cl")->insert('usuarios');
				$id = $this->db->insert_id();
				$this->db->set('email', "$file-cv@mundoprofes.cl")->set('cv', "$file")->set('id', $id)->insert('profesor');
			} else {
				$id = $check['id'];
			}
			$assets_folder = "assetz/profesores/$id";
			is_dir($assets_folder) or mkdir($assets_folder, 0777, TRUE);
			
			// echo "rename('$dir$path$file', '$assets_folder/$file') \r\n";
			// echo "file_put_contents($assets_folder/$file.log, '$path') \r\n";
			// rename("$dir$path$file", utf8_decode("$assets_folder/$file"));
			rename(utf8_decode("$dir$path$file"), utf8_decode("$assets_folder/$file"));
			file_put_contents(utf8_decode("$assets_folder/$file.log"), "$path");
			$data[] = utf8_decode("$assets_folder/$file");
		} 
		return $data;
	}
	private function _recurseTree($directory_map, $path = '')
	{
		$data = array();
		// echo_pre($directory_map);
		foreach ($directory_map as $index => $value)
		{
			if (gettype($index)=='integer')
			{
				$data[] = array_map('utf8_encode', array('path' => $path .'/', 'file' => $value));	// ?
				// echo "\r\n", print_r($value, TRUE);
			} 
			else 
			{
				// $data[] = array('folder' => $index, 'contents' => $this->_recurseTree($value));
				$data = array_merge($data, $this->_recurseTree($value, $path .'/'. $index));
			}
		}
		return $data;
	}
}
