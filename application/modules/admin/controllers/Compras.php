<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Compras extends Base_Controller
{
	protected function index_post()
	{
		$this->status(405);
	}
	protected function index_put()
	{
		if ($this->put() and $result = $this->model->update($this->put()))
		{
			if ($result['estado']=='verificado')
			{
				//	$result['email'] = LIST OF RECIPIENT EMAILS (Colegio)
				
				$this->load->module('mailing');
				$result['post'] = 'compra verificada';	//	wp
				$result['mailing'] = $this->mailing->post($result);
			}
			$this->response($result);
		}
	}
	protected function index_delete()
	{
		$this->status(405);
	}
}
