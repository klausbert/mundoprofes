<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Avisos_profesores extends Base_Controller
{
	public function index_get()
	{
		if (! extract($this->get()) or ! isset($id_aviso, $situacion))
			$this->response($this->model->index());
		else
			$this->response($this->model->show(['id_aviso' => $id_aviso, 'situacion' => urldecode($situacion)]));
	}
}
