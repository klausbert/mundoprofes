<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Signup extends Base_Controller
{
	protected function index_put()
	{
		$this->load->module('mailing');
		
		foreach ($this->model->index() as $data)	// todos los no confirmados
		{
			$data['post'] = 'remind';
			if ($this->mailing->post($data))
			{
				$this->model->update($data);
			}
		}
		$this->response(array('remind' => TRUE));
	}
}
