<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Cvs extends Base_Controller
{
	public function filter_get($what)
	{
		$status = 200;
		if ($range = $this->input->get_request_header('Range', TRUE)
		and $range = explode('=', $range)
		and $range[0]=='cvs')
		{
			$limit = explode('-', $range[1]);
			$this->_get_args = array_merge($this->_get_args, array('limit' => implode(',', $limit)));
		}
		$result = $this->model->filter($what, $this->get());
		
		if ($range > '' and count($result) >= $limit[1] - $limit[0])
		{
			$this->output->set_header('Accept-Ranges: '. $range[0]);
			$this->output->set_header('Content-Range: '. implode(' ', $range) .'/*');
			$status = 206;	
		}
		$this->response($result, $status);
	}
	public function upload_post()
	{
		$values = array_merge($this->_post_args, $_FILES['file']);
		
		$this->response($this->model->upload($values));
	}
	public function download_get()
	{
		$this->load->model('session_model');
		$tipo = $this->session_model->index()['tipo'];

		if ($this->session->userdata('id') and $tipo=='admin')
		{
			$parts = $this->model->download($this->input->get('id'));

			$file = BASEPATH .'../z/'. $parts['assets'] .'/'. $parts['cv'];

			header("Pragma: public");
			header("Content-disposition: attachment; filename=".$parts['cv']);
			header("Content-type: ". finfo_file( finfo_open( FILEINFO_MIME_TYPE ), $file ));
			header('Content-Transfer-Encoding: binary');
			ob_clean();
			flush();
			readfile($file);
		}
	}
}
