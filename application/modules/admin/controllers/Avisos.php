<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(realpath(dirname(__FILE__)).'/Base_Controller.php');

class Avisos extends Base_Controller
{
	protected function index_put()
	{
		if ($this->put() and $result = $this->model->update($this->put()))
		{
			if ($result['estado']=='verificado' or $result['estado']=='rechazado')
			{
				//	$result['email'] = LIST OF RECIPIENT EMAILS (Colegio)
				
				$this->load->module('mailing');
				$result['post'] = "aviso {$result['estado']}";	//	wp
				$result['mailing'] = $this->mailing->post($result);
			}
			$this->response($result);
		}
	}
}
