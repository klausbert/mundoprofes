<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends REST_Controller_v3
{
	protected $user;
	
	public function __construct($model = FALSE)
	{
		parent::__construct($model);
		
		if (! $this->session->userdata('id'))
			$this->abort_status = 401;
		else
		{
			$this->load->model('session_model');
			$this->user = $this->session_model->index();
			
			$this->abort_status = 403;
			if ($this->user['tipo']=='admin')
				$this->abort_status = 0;
		}
		if ($this->abort_status)
			exit($this->abort_status);
	}
	public function __destruct()
	{
		if ($this->abort_status)
			$this->response(NULL, $this->abort_status);
		
		parent::__destruct();
	}
}
