<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfiles_model extends MY_Model
{
	public function show($get)
	{
		if (isset($get['perfil']))
			return $this->db->like($get)->get($this->table)->result_array();
		else
			return $this->db->where($get)->get($this->table)->result_array();
		
	}
}
