<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Titulos_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('titulos');
	}
	public function show($where)
	{
		extract($where);
		
		if (isset($nivel))
			return $this->db->where("nivel = '$nivel'")->get($this->table)->result_array()
		;
		if (isset($titulo))
			return $this->db->where("titulo LIKE '%$titulo%'")->get($this->table)->result_array()
		;
	}
}
