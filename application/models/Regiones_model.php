<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regiones_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('regiones');
	}
	public function show($get)
	{
		if (isset($get['region'])) {
			return $this->db->like($get)->get($this->table)->result_array();
		} else {
			return $this->db->where($get)->get($this->table)->result_array();
		}
	}
}
