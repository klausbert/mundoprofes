<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios');
	}
	public function index()
	{
		// log_error('Session retrieval', $this->session->all_userdata());
		if ($this->session->userdata('id')
		and $data = $this->db->select('id, email, tipo')
			->where('id', $this->session->userdata('id'))
			->where('bloqueado IS NULL')->get($this->table)->row_array()) 
		{
			$data['roles'] = $this->db->query("
				SELECT id, 'profesor' AS tipo, 'Profesor' AS nombre, 'profesor' AS role
				FROM profesor
				WHERE id = {$data['id']}
			UNION
				SELECT id, 'colegios' AS tipo, nombre, 'sostenedor' AS role
				FROM colegios_v2
				WHERE id_usuario = {$data['id']}
			UNION
				SELECT C.id, 'colegios' AS tipo, C.nombre, X.role
				FROM usuarios_roles X LEFT
				JOIN usuarios_roles_colegios Y ON X.id = Y.id_role
				JOIN colegios_v2 C ON Y.id_colegio = C.id
				WHERE X.id_usuario = {$data['id']}
			ORDER BY 1
			")->result_array();
			
			$id_colegio = [];
			foreach ($data['roles'] as &$role)
				if ($role['tipo']=='colegios')
					$id_colegio[] = $role['id']
			;
			$data['ids_colegios'] = implode(',', $id_colegio);
			
			$data['signin'] = TRUE;
			$data['temp_password'] = $this->session->userdata('temp_password');
			return $data;
		}
		else
		{
			$data['signin'] = FALSE;
			$data['SQL'] = $this->db->last_query();
			return $data;			
		}
	}
	public function show($get)
	{
		return $this->index();
	}
	public function create($post)
	{
		$data = [];
		
		;
		if (extract($post) and !isset($email))
			return $data;
		
		$this->db->select('id, email, tipo')
			->from('usuarios')
			->where('email', $email)->where('bloqueado IS NULL');
		
		if (isset($request))	//	1.
			$data = $this->db->where("id IN (SELECT id FROM usuarios_a_confirmar WHERE id = usuarios.id AND request = '$request')")
				->get()->row_array();
		else
		if (isset($password))	//	2.
			$data = $this->db->where('password', sha1($password))->get()->row_array();

		if (isset($data) and count($data))
		{
			$this->session->set_userdata('id', $data['id']);
			if (isset($temp_password))
				$this->session->set_userdata('temp_password', $temp_password);
			
			return $this->index();
		} 
		else 
		{
			$data['error'] = $this->db->last_query();
			$data['signin'] = FALSE;
			return $data;
		}
	}
	public function delete($where)
	{
		$this->session->unset_userdata('id');
		
		$data['deleted'] = $this->session->sess_destroy();

		return $data;
	}
}
