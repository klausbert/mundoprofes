<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios');
	}
	public function update($data)
	{
		extract($data);
		
		if (isset($email) 
		and $user = $this->db->get_where($this->table, "email = '$email'")->row_array())
		{
			if ($pending = $this->db->where('id', $user['id'])->get('usuarios_a_confirmar')->row_array())
				$hash = $pending['request'];
			else
			{
				$hash = SHA1($email .' '. date('Y-m-d H:i:s'));
				$this->db->set('id', $user['id'])->set('request', $hash)->insert('usuarios_a_confirmar');
			}
			// send mail
			$data['hash'] = $hash;
			$data['email'] = $email;
			$data['status'] = 'mailing';
		} else {
			$data['status'] = 'not found';
		}
		return $data;
	}
}
