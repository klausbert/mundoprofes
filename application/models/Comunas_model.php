<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comunas_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('comunas_v2');
	}
	public function show($values)
	{
		if (isset($values['comuna'])) 
		{
			$this->db->like('comuna', $values['comuna']);
			
			if (isset($values['id_region'])) $this->db->where('id_region', $values['id_region']);
		} 
		else $this->db->where($values);
		
		return $this->db->get($this->table)->result_array();
	}
}
