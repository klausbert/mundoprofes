<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Confirm_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios');
	}
	public function index()
	{
		return FALSE;	//	deny FULL DUMP
	}
	public function show($data)
	{
		// aprovecho parra borrar los viejos
		$this->db->query("
			DELETE FROM usuarios_a_confirmar
			WHERE DATE_ADD(creado, INTERVAL 1 MONTH) < NOW()
		");
		
		return $this->db->query("
			SELECT U.*
			FROM usuarios U 
			JOIN usuarios_a_confirmar A ON U.id = A.id 
			WHERE A.request = '{$data['reset']}'
		")->row_array();
	}
}
