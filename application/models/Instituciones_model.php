<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instituciones_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('instituciones');
	}
	public function show($get)
	{
		return $this->db->select('id, institucion')->where($get)->order_by('institucion')->get($this->table)->result_array();
	}
}
