<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HTTP_Model extends CI_Model
{
	public $fhandle;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->fhandle = fopen(APPPATH .'logs/log-'. date('Y-m-d') .'.php', 'at');
	}
	/*
	**	file_get_contents
	*/
	public function get($uri, $params = FALSE, $header = 'Accept:application/json')
	{
		if ($params) $uri .= '?'. http_build_query($params);
		
		$context = stream_context_create( array('http' => array(
			'method' => 'GET',
			'header' => $header,
			'ignore_errors' => TRUE,
		)));
		
		return $this->call($uri, $context);
	}
	public function post($uri, $content, $header = 'Content-type:application/json')
	{
		$context = ['http' => [
			'method' => 'POST',
			'header' => $header,
			'content' => $content,
			'ignore_errors' => TRUE,
		]]; 
		
		$arrJSON = $this->call($uri, stream_context_create( $context ));
		
		fwrite($this->fhandle, "[POST]\t$uri\n");
		fwrite($this->fhandle, "Context:\n". json_encode($context)."\n");
		fwrite($this->fhandle, "Response:\n". json_encode($arrJSON) ."\n");
		
		return $arrJSON;
	}
	private function call($uri, $context)
	{
		$strJSON = file_get_contents($uri, FALSE, $context);
		$arrJSON = json_decode($strJSON, TRUE);
		
		if (isset($http_response_header) and ! preg_match("/^HTTP\/1\.[0,1] 200 OK$/", $http_response_header[0]))
			return array('error' => $http_response_header);
		else
			return $arrJSON;
	}
	/*
	**	CURL
	*/
	public function curl($action, $encodedBody, $headers, $method)
	{
		fwrite($this->fhandle, "CURL - ". date('Y-m-d h:i:s') ."\n");
		fwrite($this->fhandle, "[$method]\t$action\n");
		fwrite($this->fhandle, "Headers:\n". json_encode($headers)."\n");
		
		$ch = curl_init($action);
		fwrite($this->fhandle, "After curl_init(), before curl_setopt()");
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if ($method=='POST')
		curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedBody);
		
		// $verbose = fopen(APPPATH .'logs/log-'. date('Y-m-d').'.php', 'a');
		// curl_setopt($ch, CURLOPT_VERBOSE, true);
		// curl_setopt($ch, CURLOPT_STDERR, $verbose);
		
		fwrite($this->fhandle, "Before curl_exec()");
		$result['response'] = curl_exec($ch);
		fwrite($this->fhandle, "After curl_exec(), before curl_getinfo()");
		
		$result['status'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		fwrite($this->fhandle, "After curl_getinfo()");
		
		// fwrite($verbose, print_r(curl_getinfo($ch), TRUE));
		// fclose($verbose);
		curl_close($ch);

		fwrite($this->fhandle, "Response:\n". print_r($result['response'], TRUE) ."\n");
		fwrite($this->fhandle, "Status:\n".   print_r($result['status'],   TRUE) ."\n");
		
		return $result;
	}
}
