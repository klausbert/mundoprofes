<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reset_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct('usuarios');
	}
	public function update($data)
	{
		$password = SHA1($data['pass']);
		$this->db->query("
			UPDATE usuarios U 
			JOIN usuarios_a_confirmar A ON U.id = A.id 
			SET U.password = '$password'
			WHERE A.request = '{$data['reset']}'
		");
		if ($rows = $this->db->affected_rows())
			$this->db->query("
				DELETE FROM usuarios_a_confirmar
				WHERE request = '{$data['reset']}'
			");
		return $rows;
	}
}
