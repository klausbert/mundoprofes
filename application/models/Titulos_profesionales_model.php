<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Titulos_profesionales_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	
		$this->table = 'titulos_profesionales';
	}
	public function show($get)
	{
		if (isset($get['carrera'])) {
			return $this->db->like($get)->get($this->table)->result_array();
		} else {
			return $this->db->where($get)->get($this->table)->result_array();
		}
	}
	public function create($post)
	{
		$post['nivel'] = 'profesional';
		$result = parent::create($post);
		$uid = $this->db->insert_id();
		return $result;
	}
}
