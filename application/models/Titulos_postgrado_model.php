<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Titulos_postgrado_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	
		$this->table = 'titulos_postgrado';
	}
	public function create($post)
	{
		$post['nivel'] = 'postgrado';
		$result = parent::create($post);
		$uid = $this->db->insert_id();
		return $result;
	}
}
