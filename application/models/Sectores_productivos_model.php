<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sectores_productivos_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
	
		$this->table = 'sectores_productivos';
	}
}
