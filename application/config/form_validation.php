<?
$config['posts/save'] = array();

$config['posts/save'][] = set_rules('title', 'T�tulo', 'trim|required|max_length[100]|xss_clean');
$config['posts/save'][] = set_rules('content', 'Contenido', 'trim|required|xss_clean');

function set_rules()
{
	$keys = array('field', 'label', 'rules');
	return array_combine($keys, func_get_args());
}
