<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['digest_to_all'] = FALSE;
$config['notify_to_author'] = FALSE;
// $config['email_from'] = 'no-responder@mundoprofes.cl';
$config['email_from'] = 'admin@mundoprofes.com';
$config['email_cc']   = '';
$config['email_bcc']   = 'klaus.pieslinger+hello.newman@gmail.com';
$config['email_sender'] = 'Admin MundoProfes '. ENVIRONMENT;

if (ENVIRONMENT=='production')
{
	$config['notify_to_author'] = TRUE;
	$config['email_bcc']  = 'mundoprofes.com@gmail.com';
	$config['email_sender'] = 'Admin MundoProfes';
}
if (ENVIRONMENT=='prod2edo')
{
	$config['email_bcc']  = 'klaus@pieslinger.net, mundoprofes.com@gmail.com';
	$config['email_sender'] = 'MAILING MundoProfes SOLO emails al Admin';
}
if (ENVIRONMENT=='sales')
{
	$config['notify_to_author'] = TRUE;
	$config['email_bcc']  = 'klaus@pieslinger.net';
	$config['email_sender'] = 'Admin MundoProfes TEST';
}
$config['email_source']['signup'] = 63;
$config['email_source']['remind'] = 65;
$config['email_source']['compra'] = 77;
$config['email_source']['aviso preparado'] = 79;
$config['email_source']['aviso verificado'] = 81;
$config['email_source']['aviso rechazado'] = 83;
$config['email_source']['compra verificada'] = 85;

$config['mailChimp'] = [
	'apiKey' => 'ab132bc3a167ed65c2cf9448cb64bf35-us12',
	'listas' => ['V' => '5497eecc13', 'A' => '864efb21b9', 'R' => '2f563eb42b'],
	'url'    => 'https://us12.api.mailchimp.com/3.0/'
];
