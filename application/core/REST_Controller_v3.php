<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

class REST_Controller_v3 extends REST_Controller 
{
	public $data = array();
	
	public function __construct($model = FALSE)
	{
		parent::__construct();
		
		$this->load->model(($model ? $model : get_class($this).'_model'), 'model');
		
		//	hotfix for DELETE agruments
		// $this->log_error('_delete_args', print_r($this->_delete_args, TRUE));
		$hotfix_args = array_keys($this->_delete_args);
		if ($hotfix_args) $this->_delete_args = json_decode( $hotfix_args[0], TRUE);		
		// $this->log_error('_delete_args', print_r($this->_delete_args, TRUE));
	}
    public function response($data = NULL, $status = 200, $continue = FALSE)	// compatibility w/ Phil Sturgeon's method
	{
		try 
		{
			$json_encode_numeric = json_encode($data, JSON_NUMERIC_CHECK);
			$preg_replace_string = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/u', '', $json_encode_numeric);	// Phil's Response treats numbers as strings!!
		}
		catch (Exception $exception)
		{
			$json_encode_numeric = json_encode(array('error' => $exception));
			$preg_replace_string = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/u', '', $json_encode_numeric);
		}
		$this->output
			->set_status_header($status)
			->set_content_type('application/json')
			->set_output($preg_replace_string);
	}
	/*	
	**	Default methods
	*/
	protected function index_get()
	{
		if (! $this->get())
			$this->response( $this->model->index());
		else
			$this->response( $this->model->show($this->get()));
	}
	protected function id_get($id = FALSE)
	{
		$this->response( $this->model->show(array_merge($this->get(), array('id' => $id))));
	}
	protected function index_post()
	{
		if ($this->post() and $id = $this->model->create($this->post())) 
			$this->response($id, '201')
		;
	}
	protected function index_put()
	{
		if ($this->put()) 
			$this->response( $this->model->update($this->put()))
		;
	}
	protected function index_delete()
	{
		if ($this->delete()) 
			$this->response( $this->model->delete($this->delete()))
		;
	}
	/*	
	**	default status errors and log_error
	*/
	protected function status($status)
	{
		$error[405] = 'Method Not Allowed';
		
		$this->response( array('Error' => $error[$status]), $status);
	}
	protected function log_error($message, $data = FALSE)
	{
		$message = get_class($this) ." - $message ";

		log_message('ERROR', $message .($data ? "\n". print_r($data, TRUE) : ""));
		
		$result['error'] = $message;
		if ($data) $result['data'] = $data;
		
		return $result;
	}
	/*
	**	JSON methods (los usa AUTH; actualizar antes de sacar)
	*/
	protected function getJSON($uri, $params = array(), $header = 'Accept:application/json')
	{
		$context = stream_context_create( array('http' => array(
			'method' => 'GET',
			'header' => $header,
			'ignore_errors' => TRUE,
		)));
		return $this->callJSON($uri .'?'. http_build_query($params), $context);
	}
	protected function getJSON2($uri, $header)
	{
		$context = stream_context_create( array('http' => array(
			'method' => 'GET',
			'header' => $header,
			'ignore_errors' => TRUE,
		)));
		return $this->callJSON($uri, $context);
	}
	protected function postJSON($uri, $params)
	{
		$context = stream_context_create( array('http' => array(
			'method' => 'POST',
			'header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => http_build_query($params),
			// 'ignore_errors' => TRUE,
		))); 
		return $this->callJSON($uri, $context);
	}
	private function callJSON($uri, $context)
	{
		$strJSON = file_get_contents($uri, FALSE, $context);
		
		if (! preg_match("/^HTTP\/1\.[0,1] 200 OK$/", $http_response_header[0]))
			return array('error' => $http_response_header);
		else
			return json_decode($strJSON, TRUE);
	}
}
