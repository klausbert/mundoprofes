<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model
{
	public $table = 'DUAL';
	
	public function __construct($table = FALSE)
	{
		parent::__construct();
		
		$this->table =($table)? $table : strtolower(str_replace('_model', '', get_class($this)));
	}
	protected function cleanup($data, $table = FALSE)
	{
		if (! $table) $table = $this->table;
		return array_intersect_key($data, array_flip( $this->db->list_fields($table)));
	}
	protected function log_error($message, $data = FALSE)
	{
		$message = get_class($this) ." - $message ";

		log_message('ERROR', $message .($data ? "\n". print_r($data, TRUE) : ""));
		
		$result['error'] = $message;
		if ($data) $result['data'] = $data;
		
		return $result;
	}
	/*	Default methods called from REST_Controller_v3 / MY_Controller
	*/
	public function index()
	{
		return $this->db->get($this->table)->result_array();
	}
	public function show($where)
	{
		return $this->db->get_where($this->table, $this->cleanup($where))->result_array();
	}
	public function create($post)
	{
		if (! isset($post['id'])) 
		{
			$this->db->insert($this->table, $this->cleanup($post));
			
			$show = $this->show( array('id'=> $this->db->insert_id()));
			return $show[0];
		} 
		else 
		{
			return $this->update($post);
		}
	}
	public function update($put)
	{
		$where['id'] = $put['id']; unset($put['id']);
		
		$this->db->update($this->table, $this->cleanup($put), $where);
		
		return $this->show($where);
	}
	public function delete($where)
	{
		$this->db->delete($this->table, $where);
		
		return $this->db->affected_rows();
	}
	//
	protected function enum_options($table = FALSE)
	{
		if (! $table) $table = $this->table;
		
		foreach ($this->db->query("SHOW COLUMNS FROM {$table} WHERE type LIKE 'enum(%'")->result_array() as $row)
		{
			preg_match_all("/'(.*?)'/", $row['Type'], $enum_array);
			$data[$row['Field']] = $enum_array[1];
		}
		return $data;
	}
}
