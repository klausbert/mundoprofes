<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class REST_Model_v3 extends CI_Model
{
	public $table = FALSE;
	public $identity = FALSE;
	
	public function __construct($table, $identity = '')
	{
		parent::__construct();
		
		$this->table = $table;
		$this->identity = $identity;
	}
	public function get($where)
	{
		return $this->db->get_where($this->table, $where)->result_array();
	}
	public function post($set)
	{
		$this->db->set($set)->insert($this->table);
		return $this->db->insert_id();
	}
	public function put($set, $where = FALSE)
	{
		if (! $where)
		{
			$where[$this->identity] = $set[$this->identity]; 
			unset($set[$this->identity]);
		}
		$this->db->set($set)->where($where)->update($this->table);
		return $this->db->affected_rows();
	}
	public function delete($where = FALSE)
	{
		return ($where) ? $this->db->where($where)->delete($this->table) : FALSE;
	}
}
