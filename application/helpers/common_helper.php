<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('echo_pre'))
{
	function echo_pre($data, $die = TRUE)
	{
		echo '<pre>'; print_r($data); echo '</pre>'; 
		if ($die) die();
	}
}
if ( ! function_exists('excerpt'))
{
	function excerpt($content, $numberOfWords = 20) {
		$contentWords = substr_count($content, ' ') + 1;
		$words = explode(' ', $content, $numberOfWords + 1);
		if( $contentWords > $numberOfWords ) {
			$words[count($words) - 1] = '...';
		}
		return join(' ', $words);
	}
}
if ( ! function_exists('fix_html'))
{
	function fix_html(&$item) {
		$item = htmlentities($item);
	}
}
if ( ! function_exists('layout_view'))
{
	function layout_view($view, $data = NULL, $layout = 'layout')
	{
		$CI =& get_instance();
		
		$main = $CI->load->view($view, $data, TRUE);
		if ($CI->input->is_ajax_request())
		{
			echo $main;
		} else 
		{
			$menu = $CI->load->view('menu', $data, TRUE);
			echo $CI->load->view($layout, array('menu' => $menu, 'main' => $main), TRUE);
		}
	}
}
if ( ! function_exists('log_error'))
{
	function log_error($message, $data = FALSE)
	{
		$callers = debug_backtrace();
		$separator = (strpos($callers[0]['file'], '\\')==FALSE)? '/' : '\\';
		$path_name = array_reverse( explode($separator, $callers[0]['file']));
		$caller = str_replace('.php', '', $path_name[0]);
		
		$message = "$caller - $message ";
		
		log_message('ERROR', $message .($data ? "\n". json_encode($data, JSON_NUMERIC_CHECK) ."\n" : ""));
		
		$result['error'] = $message;
		if ($data) $result['data'] = $data;
		
		return $result;
	}
}
if ( ! function_exists('todays_date'))
{
	function todays_date()
	{
		return Date('Y-m-d', time()-(8 * 60 * 60));	// 'Y-m-d H:i:s'
	}
}
